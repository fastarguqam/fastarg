@echo on

call  FastARG 500_1000_Margarita(test1).json
call  FastARG 500_1000_Margarita(test2).json
call  FastARG 500_1000_Margarita(test3).json
call  FastARG 500_2000_Margarita(test1).json
call  FastARG 500_2000_Margarita(test2).json
call  FastARG 500_2000_Margarita(test3).json
call  FastARG 500_5000_Margarita(test1).json
call  FastARG 500_5000_Margarita(test2).json
call  FastARG 500_5000_Margarita(test3).json
call  FastARG 500_10000_Margarita(test1).json
call  FastARG 500_10000_Margarita(test2).json
call  FastARG 500_10000_Margarita(test3).json

call  FastARG 1000_1000_Margarita(test1).json
call  FastARG 1000_1000_Margarita(test2).json
call  FastARG 1000_1000_Margarita(test3).json
call  FastARG 1000_2000_Margarita(test1).json
call  FastARG 1000_2000_Margarita(test2).json
call  FastARG 1000_2000_Margarita(test3).json
call  FastARG 1000_5000_Margarita(test1).json
call  FastARG 1000_5000_Margarita(test2).json
call  FastARG 1000_5000_Margarita(test3).json
call  FastARG 1000_10000_Margarita(test1).json
call  FastARG 1000_10000_Margarita(test2).json
call  FastARG 1000_10000_Margarita(test3).json

call  FastARG 2000_1000_Margarita(test1).json
call  FastARG 2000_1000_Margarita(test2).json
call  FastARG 2000_1000_Margarita(test3).json
call  FastARG 2000_2000_Margarita(test1).json
call  FastARG 2000_2000_Margarita(test2).json
call  FastARG 2000_2000_Margarita(test3).json
call  FastARG 2000_5000_Margarita(test1).json
call  FastARG 2000_5000_Margarita(test2).json
call  FastARG 2000_5000_Margarita(test3).json
call  FastARG 2000_10000_Margarita(test1).json
call  FastARG 2000_10000_Margarita(test2).json
call  FastARG 2000_10000_Margarita(test3).json
pause
