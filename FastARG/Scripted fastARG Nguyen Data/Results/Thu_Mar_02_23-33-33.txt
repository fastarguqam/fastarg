FastARG parameters are : 
 -The data file is                     : 500_5000_Nguyen(test3).dat
 -The specified number of sequences is : 500
 -The specified size of sequences is   : 5000
 -The algorithm is                     : ARG4WG
 -The coalescence mode is              : HAVE_ANCESTRAL_MATERIAL_IN_COMMON
 -The recombination mode is            : HAVE_ANCESTRAL_MATERIAL_ONLY
 -The random seed flag is              : false
 -The seed is                          : 1
 -The Rplot settings                   : false


             ***  ARG4GW ARG summary *** 
Basic attributes.
 - Length of sequences   : 5000
 - Number of sequences   : 500
 - Total number of nodes : 43929
 - Total of lives nodes  : 1
 - Sum of mutant loci    : 0

Quantity of each type of event in the current state of the ARG.
 - Mutation event      : 3252
 - Coalescence event   : 13725
 - Recombination event : 13226

Quantity of possible events for each type.
 - Possible mutation event        : 0
 - Possible coalescence event     : 0
 - Number of position to be muted : 0

Constructed in 245 seconds.
