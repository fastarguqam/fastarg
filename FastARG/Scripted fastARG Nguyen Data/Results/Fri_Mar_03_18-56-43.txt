FastARG parameters are : 
 -The data file is                     : 500_2000_Nguyen(test1).dat
 -The specified number of sequences is : 500
 -The specified size of sequences is   : 2000
 -The algorithm is                     : ARG4WG
 -The coalescence mode is              : HAVE_ANCESTRAL_MATERIAL_IN_COMMON
 -The recombination mode is            : HAVE_ANCESTRAL_MATERIAL_ONLY
 -The random seed flag is              : false
 -The seed is                          : 1
 -The Rplot settings                   : false


             ***  ARG4GW ARG summary *** 
Basic attributes.
 - Length of sequences   : 2000
 - Number of sequences   : 500
 - Total number of nodes : 25097
 - Total of lives nodes  : 1
 - Sum of mutant loci    : 1

Quantity of each type of event in the current state of the ARG.
 - Mutation event      : 1469
 - Coalescence event   : 8042
 - Recombination event : 7543

Quantity of possible events for each type.
 - Possible mutation event        : 1
 - Possible coalescence event     : 0
 - Number of position to be muted : 1

Constructed in 50 seconds.
