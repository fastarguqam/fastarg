FastARG parameters are : 
 -The data file is                     : 500_5000_Nguyen(test1).dat
 -The specified number of sequences is : 500
 -The specified size of sequences is   : 5000
 -The algorithm is                     : Manhattan
 -The coalescence mode is              : HAVE_ANCESTRAL_MATERIAL_IN_COMMON
 -The recombination mode is            : HAVE_ANCESTRAL_MATERIAL_ONLY
 -The random seed flag is              : false
 -The seed is                          : 0
 -The Rplot settings                   : false


             ***  Manhattan ARG summary *** 
Basic attributes.
 - Length of sequences   : 5000
 - Number of sequences   : 500
 - Total number of nodes : 58970
 - Total of lives nodes  : 1
 - Sum of mutant loci    : 0

Quantity of each type of event in the current state of the ARG.
 - Mutation event      : 3566
 - Coalescence event   : 18634
 - Recombination event : 18135

Quantity of possible events for each type.
 - Possible mutation event        : 0
 - Possible coalescence event     : 0
 - Number of position to be muted : 0

Manhattan specific attributes :
 - The seed is                        : 0
Constructed in 187 seconds.
