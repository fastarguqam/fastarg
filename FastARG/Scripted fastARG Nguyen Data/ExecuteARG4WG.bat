@echo on
call  FastARG 500_1000_ARG4WG(test1).json
call  FastARG 500_1000_ARG4WG(test2).json
call  FastARG 500_1000_ARG4WG(test3).json
call  FastARG 500_2000_ARG4WG(test1).json
call  FastARG 500_2000_ARG4WG(test2).json
call  FastARG 500_2000_ARG4WG(test3).json
call  FastARG 500_5000_ARG4WG(test1).json
call  FastARG 500_5000_ARG4WG(test2).json
call  FastARG 500_5000_ARG4WG(test3).json
call  FastARG 500_10000_ARG4WG(test1).json
call  FastARG 500_10000_ARG4WG(test2).json
call  FastARG 500_10000_ARG4WG(test3).json

call  FastARG 1000_1000_ARG4WG(test1).json
call  FastARG 1000_1000_ARG4WG(test2).json
call  FastARG 1000_1000_ARG4WG(test3).json
call  FastARG 1000_2000_ARG4WG(test1).json
call  FastARG 1000_2000_ARG4WG(test2).json
call  FastARG 1000_2000_ARG4WG(test3).json
call  FastARG 1000_5000_ARG4WG(test1).json
call  FastARG 1000_5000_ARG4WG(test2).json
call  FastARG 1000_5000_ARG4WG(test3).json
call  FastARG 1000_10000_ARG4WG(test1).json
call  FastARG 1000_10000_ARG4WG(test2).json
call  FastARG 1000_10000_ARG4WG(test3).json

call  FastARG 2000_1000_ARG4WG(test1).json
call  FastARG 2000_1000_ARG4WG(test2).json
call  FastARG 2000_1000_ARG4WG(test3).json
call  FastARG 2000_2000_ARG4WG(test1).json
call  FastARG 2000_2000_ARG4WG(test2).json
call  FastARG 2000_2000_ARG4WG(test3).json
call  FastARG 2000_5000_ARG4WG(test1).json
call  FastARG 2000_5000_ARG4WG(test2).json
call  FastARG 2000_5000_ARG4WG(test3).json
call  FastARG 2000_10000_ARG4WG(test1).json
call  FastARG 2000_10000_ARG4WG(test2).json
call  FastARG 2000_10000_ARG4WG(test3).json


pause
