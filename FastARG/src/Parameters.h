/*
 * Parameters.h
 *
 *  Created on: 2016-09-07
 *      Author: �ric Marcotte
 */

#ifndef PARAMETERS_H_
#define PARAMETERS_H_


#include <string>
#include <vector>

#include "json.hpp"
#include "Enums.h"

using std::string;
using std::vector;
using std::ostream;

class Parameters {
public:
	// Members
	bool randomSeed;
	bool Rplot;
	size_t sequencesSize;
	size_t numberOfSequences;
	size_t seed;
	string dataFile;

	vector<bool> phenotypes;
	vector<size_t> locusCoordinates;

	algorithms algorithmChoice = algorithms::UNSPECIFIED;
	//string algorithmName;

	coalescenceMode coalescenceModeChoice= coalescenceMode::UNSPECIFIED;
	string coalescenceModeName;

	recombinationMode recombinationModeChoice = recombinationMode::UNSPECIFIED;
	string recombinationModeName;

	// Margarita specific.
	double MaximumRecombinationRate;

	// TODO implement these
	double mutationRate;
	double recombinationRate;
	double coalescenceRate;

	// Functions
	Parameters();
	Parameters( string FilePath );
	static bool verifyIfParametersAreValid(const Parameters & params);
	friend ostream & operator << ( ostream &, const Parameters &);
	virtual ~Parameters();
};

#endif /* PARAMETERS_H_ */

