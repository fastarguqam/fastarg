/*
 * ARG4WG.h
 *
 *  Created on: 2017-02-21
 *      Author: �ric Marcotte
 */

#ifndef ARG4WG_H_
#define ARG4WG_H_

#include "ARG.h"

class ARG4WG: public ARG {
public:
	vector<vector<std::pair<size_t,size_t>>> leftRightSharedIndexMatrixForLiveNodes;

	ARG4WG(Parameters params, vector<ARGnode> Leaves, const vector<bool> &phenotypes,const  vector<size_t> & distances);
	virtual void generateACoalescentEvent(size_t firstSourceNode, size_t secondSourceNode) override;
	virtual void generateARecombinationEvent(size_t indexOfTheRecombination, size_t sourceNode) override;
	virtual void generetaAMutationEvent(size_t positionOfTheMutation , size_t sourceNode) override;
	void printARGSummary();
	void generateAnARG4WGRecombinationEvent();
	std::vector<std::tuple<bool,size_t,size_t,size_t>> findLongestSharedEnds(void); // tuple< LEFT RIGHT bool, index of end, source node1, source node 2>


	static std::pair<size_t,size_t> getleftAndRightSharedIndex (const SnipSequence &, const SnipSequence & );
	virtual void run(void) override;
	virtual ~ARG4WG() = default;
};

#endif /* ARG4WG_H_ */
