/*
 * ARGraph.cpp
 *
 *  Created on: 2015-09-05
 *      Author: �ric Marcotte
 *
 */

#include <limits>
#include <chrono>
#include <sstream>
#include <cassert>
#include <fstream>
#include <boost/math/distributions/chi_squared.hpp>

#include "ARGnode.h"
#include "ARG.h"

typedef std::chrono::high_resolution_clock Clock;

using std::cout;
using std::cerr;
using std::pow;
using std::vector;
using std::numeric_limits;
using std::uniform_int_distribution;
using std::to_string;
using std::invalid_argument;

int _DEBUG_ = 0;
int _HISTORY_ = 0;
#define HISTORY( x )  if (_HISTORY_ == 1) { x }
//#define DEBUG( x )  if (_DEBUG_ > 0) { x }
//#define DEBUG1( x ) if (_DEBUG_ > 1) { x }

//#define TEST_FOR_COMPLETE_NON_ANCESTRAL_SEQUENCES
//#define EXECUTIONTRACE
//#define VALIDATION

/*********************************************************************************************/
/**                     ANCESTRAL RECOMBINATION GRAPH CONSTRUCTOR                           **/
/*********************************************************************************************/
ARG::ARG( Parameters params, vector<ARGnode> leaves, const vector<bool> & phenotypes, const  vector<size_t> & coordinates){
	size_t  numberOfSequences  = params.numberOfSequences;
	size_t lengthOfsequences = params.sequencesSize;
	this->maxGenerationSize = params.numberOfSequences;

	// Check if sizes are coherent.
	if ( leaves.size() != numberOfSequences){
		throw std::logic_error( string(__FILE__) + ':' + std::to_string(__LINE__) + ": leaves.size() != numberOfSequences" );
	}
	if( leaves.size() %2 != 0 ){
		throw std::logic_error( string(__FILE__) + ':' + std::to_string(__LINE__) + ": leaves.size() %2 != 0" );
	}
	if ( leaves.size()/2 != phenotypes.size()){
		throw std::logic_error( string(__FILE__) + ':' + std::to_string(__LINE__) + ": leaves.size()/2 != phenotypes.size()" );
	}
	if( coordinates.size() != lengthOfsequences ){
		throw std::logic_error( string(__FILE__) + ':' + std::to_string(__LINE__) + ": coordinates.size() != lengthOfsequences" );
	}

	// Sets basics parameters.
	SnipSequence::coalescenceModeFlag =  params.coalescenceModeChoice;
	this->recombinationModeFlag = params.recombinationModeChoice;
	this->lenghtOfSequences = lengthOfsequences;
	this->numberOfSequences = numberOfSequences;

	// Initialization of events counters.
	this->totalNumberOfCoalescenceEvent = 0;
	this->totalNumberOfMutationEvent = 0;
	this->totalNumberOfRecombinationEvent = 0;

	// Assigns functions parameters to the ARG object.
	this->ARGnodes = leaves;
	this->ARGnodes.reserve(10*leaves.size());
	this->SnipsCoordinates = coordinates;
	this->phenotypes = phenotypes;

	// At the beginning all leaves are lives nodes.
	this->positionOfAliveNodes = vector<size_t>(this->ARGnodes.size());
	for (size_t i = 0; i < this->ARGnodes.size(); ++i){
		this->positionOfAliveNodes.at(i) = i;
	}

	// Sets the seeds and the random number generator with the clock.
	// If you sets the seed with zero you *should* get the same RNG.
	this->seed = params.seed;
	if (params.randomSeed){
		this->seed = params.seed * Clock::now().time_since_epoch().count();
	}else{
		this->seed = params.seed;
	}
	this->eng = mt19937(this->seed);

	// Populates the vector that contains the number of mutants locus for each position.
	this->numberOfMutantSNPsByPosition.resize(params.sequencesSize);
	for (const auto &aLeaf : leaves){
		for (size_t i = 0 ; i < params.sequencesSize; ++i){
			if (SnipSequence::isPositionMutant(i, aLeaf.sequence)){
				++this->numberOfMutantSNPsByPosition.at(i);
			}
		}
	}
	// Computes counters for all the mutants position and the number of locus that can revert to an non-mutant state.
	this->numberOfMutationPossible = 0;
	this->numberOfPositionToBeMuted = 0;
	this->isAMutationEventPossible = false;
	for (const auto locus : this->numberOfMutantSNPsByPosition){
		if (locus == 1){
			this->isAMutationEventPossible = true;
			++this->numberOfMutationPossible;
			++this->numberOfPositionToBeMuted;
		}else if (locus > 1){
			++this->numberOfPositionToBeMuted;
		}
	}

	// Computes all the pairs that can coalesce.
	for ( size_t i = 0 ; i < this->positionOfAliveNodes.size() ; ++i){
		for ( size_t j = i+1 ; j < this->positionOfAliveNodes.size() ; ++j){
			if ( this->ARGnodes.at(this->positionOfAliveNodes.at(i)).sequence ==  this->ARGnodes.at(this->positionOfAliveNodes.at(j)).sequence ){
				this->possibleCoalescence.insert(std::pair<size_t,size_t>{i,j});
			}
		}
	}
	this->numberOfCoalescencePossible = this->possibleCoalescence.size();
}

/*********************************************************************************************/
/**                          COALESCENT EVENT GENERATOR                                     **/
/*********************************************************************************************/
void ARG::generateACoalescentEvent(size_t firstSourceNode, size_t secondSourceNode) {
#ifdef VALIDATION
	if (firstSourceNode == secondSourceNode){
		throw std::logic_error( string(__FILE__) + ':' + std::to_string(__LINE__) + " : The node sources can't be the same in a coalescent event.");
	}
	if (secondSourceNode >= this->ARGnodes.size()){
		throw std::logic_error( string(__FILE__) + ':' + std::to_string(__LINE__) + ": Source node doesn't exist.");
	}
	if (A != B){
		throw std::logic_error( string(__FILE__) + ':' + std::to_string(__LINE__) + " : Those sequences can't coalesce.");
	}

	if ( indexOfFirstSourceInAliveNode == this->positionOfAliveNodes.size() ){ // If true, then we didn't find the source nodes in the lives nodes.
		throw std::logic_error ( string(__FILE__) + ':' + std::to_string(__LINE__) + ": Position " + std::to_string(firstSourceNode+1) + " is not an alive node.");
	}
	if ( indexOfSecondSourceInAliveNode == this->positionOfAliveNodes.size() ){ // Idem.
		throw std::logic_error ( string(__FILE__) + ':' + std::to_string(__LINE__) + "Position " + std::to_string(secondSourceNode+1) + " is not an alive node.");
	}
#endif

	// In this case we need to make the unions of the two nodes.
	vector<vector<uint16_t>> resultVectorForLeafReach;
	vector<uint16_t> resultVectorForLeafReachForALocus;
	vector<uint16_t> leafReachForFirstNodeForALocus, leafReachForSecondNodeForALocus;

	for (size_t i = 0 ; i < this->lenghtOfSequences  ; ++i){
		leafReachForFirstNodeForALocus = getLeafReachabilityforALocus(i,firstSourceNode);
		leafReachForSecondNodeForALocus = getLeafReachabilityforALocus(i,secondSourceNode);

		std::set_union(leafReachForFirstNodeForALocus.begin(), leafReachForFirstNodeForALocus.end(),
				leafReachForSecondNodeForALocus.begin(), leafReachForSecondNodeForALocus.end(),
				std::back_inserter(resultVectorForLeafReachForALocus));
		resultVectorForLeafReach.push_back(resultVectorForLeafReachForALocus);
		resultVectorForLeafReachForALocus.clear();
	}

	// Creates the new node.
	SnipSequence sequence = this->ARGnodes.at(firstSourceNode).sequence + this->ARGnodes.at(secondSourceNode).sequence;
	ARGnode aCoalescenceNode(ARGNodeType::COALESCENSENODE, sequence, firstSourceNode, secondSourceNode, resultVectorForLeafReach );

	// Add the new coalescence node at the end of all the nodes.
	this->ARGnodes.push_back(aCoalescenceNode);

	// Adjust counters of coalescent event.
	++this->totalNumberOfCoalescenceEvent;

	// Populates the vector that contains the number of mutant locus for each position.
	for (size_t i = 0 ; i < this->lenghtOfSequences; ++i){
		// we decrement by 1 if BOTH locus were mutant.
		if ( SnipSequence::isPositionMutant( i ,this->ARGnodes.at(firstSourceNode).sequence ) &&
				SnipSequence::isPositionMutant( i ,this->ARGnodes.at(secondSourceNode).sequence ) ){
			--this->numberOfMutantSNPsByPosition.at(i);
			// We check if we can mutate this locus after decrementing.
			if ( this->numberOfMutantSNPsByPosition.at(i) == 1 ){
				++this->numberOfMutationPossible;
				this->isAMutationEventPossible = true;
			}
		}
	}

	// Updates the lives node vector.
	size_t indexOfFirstSourceInAliveNode = this->positionOfAliveNodes.size(), indexOfSecondSourceInAliveNode = this->positionOfAliveNodes.size();
	for ( size_t i = 0 ; i < this->positionOfAliveNodes.size(); ++i){
		if (this->positionOfAliveNodes.at(i) == firstSourceNode){
			indexOfFirstSourceInAliveNode = i;
		}else if (this->positionOfAliveNodes.at(i) == secondSourceNode ){
			indexOfSecondSourceInAliveNode = i;
		}
		if (indexOfFirstSourceInAliveNode != this->positionOfAliveNodes.size() && indexOfSecondSourceInAliveNode != this->positionOfAliveNodes.size() ){
			break;
		}
	}
	if (indexOfFirstSourceInAliveNode > indexOfSecondSourceInAliveNode ){
		std::swap(indexOfFirstSourceInAliveNode,indexOfSecondSourceInAliveNode);
	}
	// We delete the second position in the alive vector and the coalescence matrix and change the first one.
	this->positionOfAliveNodes.erase(this->positionOfAliveNodes.begin()+indexOfSecondSourceInAliveNode );
	this->positionOfAliveNodes.at(indexOfFirstSourceInAliveNode) = this->ARGnodes.size()-1;

	// TODO change for element !
	// We delete the source nodes coalescence pairs.
	for (auto itr = this->possibleCoalescence.begin(); itr != this->possibleCoalescence.end(); ){
		if ( (*itr).first == firstSourceNode || (*itr).first == secondSourceNode || (*itr).second == firstSourceNode || (*itr).second == secondSourceNode ){
			itr = this->possibleCoalescence.erase(itr);
		}else{
			++itr;
		}
	}
	// We then add all the coalescence pairs for the new node.
	for ( size_t i = 0 ; i < this->positionOfAliveNodes.size() ; ++i){
		if (this->positionOfAliveNodes.at(i) != this->ARGnodes.size()-1){
			if (this->ARGnodes.back().sequence == this->ARGnodes.at(this->positionOfAliveNodes.at(i)).sequence){
				this->possibleCoalescence.insert(std::pair<size_t,size_t>{this->positionOfAliveNodes.at(i), this->ARGnodes.size()-1});
			}
		}
	}
	this->numberOfCoalescencePossible = this->possibleCoalescence.size();
	HISTORY(cout << "Coalescence between node " << to_string(firstSourceNode+1) << " and node " << to_string(secondSourceNode+1)<< std::endl;);
}

/*********************************************************************************************/
/**                   RANDOM COALESCENT EVENT GENERATOR                                     **/
/*********************************************************************************************/
bool ARG::generateARandomCoalescentEvent() {
	// Check there is at least one coalescence event possible.
	if ( this->numberOfCoalescencePossible == 0 ){
		return false;
	}
	// Gets a random possible coalescence envent.
	uniform_int_distribution<size_t> uniformDistribution(0, (this->numberOfCoalescencePossible)-1);
	size_t numberOfARandomCoalescence = uniformDistribution(eng);
	std::pair<size_t,size_t> winningPair= *std::next(this->possibleCoalescence.begin(), numberOfARandomCoalescence);
	generateACoalescentEvent( winningPair.first, winningPair.second );
	return true;
}

/*********************************************************************************************/
/**                           RECOMBINATION EVENT GENERATOR                                 **/
/*********************************************************************************************/
void ARG::generateARecombinationEvent(size_t positionOfTheRecombination, size_t sourceNode) {
	assert(positionOfTheRecombination > 0);
	assert(positionOfTheRecombination < ARGnodes.at(0).sequence.getSizeOfSequence());

	// We find and iterator on the node that will recombine.
	vector<size_t>::iterator it;
	it = find(this->positionOfAliveNodes.begin(), this->positionOfAliveNodes.end(), sourceNode  );

#ifdef VALIDATION
	if (it == this->positionOfAliveNodes.end() ){
		throw std::out_of_range (string(__FILE__) + ':' + std::to_string(__LINE__) + "Position " + std::to_string(sourceNode+1) + " is not a live node.\n" );
	}
#endif

	// Then we create an array of SnipSequence to receive the result of the recombination.
	try {
		std::pair<SnipSequence,SnipSequence> RecombinationResultSequences = SnipSequence::recombinationOnAposition(this->ARGnodes.at(*it).sequence, positionOfTheRecombination);
		ARGnode leftNode  (ARGNodeType::RECOMBINATIONODE, RecombinationResultSequences.first, sourceNode, positionOfTheRecombination, {});
		ARGnode rightNode (ARGNodeType::RECOMBINATIONODE, RecombinationResultSequences.second, sourceNode, positionOfTheRecombination, {});
		// Adds the left nodes in the ARG.
		this->ARGnodes.push_back(leftNode);
		// Replaces one alive node with the position of the left one.
		(*it) = this->ARGnodes.size()-1;
		// Adds the right one in the ARG.
		this->ARGnodes.push_back(rightNode);
	}catch(const std::exception & exception){
		throw std::runtime_error(string(__FILE__) + ':' + std::to_string(__LINE__) + " : An exception has been catch while creating the new recombination nodes.\n" + exception.what() );
	}

	// Adds the other alive node in the vector.
	this->positionOfAliveNodes.push_back(this->ARGnodes.size()-1);

	// Adds on event on the counter.
	++this->totalNumberOfRecombinationEvent;

	// We delete the souce nodes coalescence pairs.
	for (auto itr = this->possibleCoalescence.begin(); itr != this->possibleCoalescence.end(); ){
		( (*itr).first == sourceNode || (*itr).second == sourceNode ) ? ( itr = this->possibleCoalescence.erase(itr) ) : (++itr);
	}

	// We then add all the coalescence pairs for the new node.
	for ( size_t i = 0 ; i < this->positionOfAliveNodes.size() ; ++i){
		if (this->positionOfAliveNodes.at(i) != this->ARGnodes.size()-1  ){
			if (this->ARGnodes.back().sequence == this->ARGnodes.at(this->positionOfAliveNodes.at(i)).sequence){
				this->possibleCoalescence.insert(std::pair<size_t,size_t>{this->positionOfAliveNodes.at(i), this->ARGnodes.size()-1});
			}
		}
		if (this->positionOfAliveNodes.at(i) != this->ARGnodes.size()-2){
			if (this->ARGnodes.at(this->ARGnodes.size()-2).sequence == this->ARGnodes.at(this->positionOfAliveNodes.at(i)).sequence){
				this->possibleCoalescence.insert(std::pair<size_t,size_t>{this->positionOfAliveNodes.at(i), this->ARGnodes.size()-2});
			}
		}
	}

	this->numberOfCoalescencePossible = this->possibleCoalescence.size();
	// Mutation attributes should stay the same.

	if (this->maxGenerationSize < this->positionOfAliveNodes.size()){
		this->maxGenerationSize = this->positionOfAliveNodes.size();
	}

	HISTORY(cout << "Recombination on the node " << to_string(sourceNode+1) << " at the position " << to_string(positionOfTheRecombination) << std::endl;);
}

/*********************************************************************************************/
/**                  RANDOM RECOMBINATION EVENT GENERATOR                                  **/
/*********************************************************************************************/
bool ARG::generateARandomRecombinationEvent() {
	size_t randomNode,randomPosition;

	// First we select the type of recombination event.
	if (this->recombinationModeFlag == recombinationMode::NO_CONSTRAINT){

		// We get one random node number of all the alive nodes.
		uniform_int_distribution<size_t> uniformDistribution(0, (this->positionOfAliveNodes.size() -1));
		randomNode = this->positionOfAliveNodes.at(uniformDistribution(eng));

		// The we get a random position.
		uniform_int_distribution<size_t> uniformDistribution2(1, (this->lenghtOfSequences)-1);
		randomPosition = uniformDistribution2(eng);

	}else if (this->recombinationModeFlag == recombinationMode::HAVE_ANCESTRAL_MATERIAL_ONLY){

		// First we select a random node.
		uniform_int_distribution<size_t> uniformDistribution(0, (this->positionOfAliveNodes.size() -1));
		randomNode = this->positionOfAliveNodes.at(uniformDistribution(eng));


		// Then we find the first position that is an ancestral position.
		size_t firstAncestralPosition = 0;
		SnipSequence aRandomSequence = this->ARGnodes.at(randomNode).sequence;
		while (firstAncestralPosition < aRandomSequence.getSizeOfSequence() && !SnipSequence::isPositionAncestral(firstAncestralPosition, aRandomSequence) ){
			firstAncestralPosition++;
		}

		// And now we find the last ancestral position.
		size_t lastAncestralPosition = aRandomSequence.getSizeOfSequence()-1;
		while (lastAncestralPosition > 0 && !SnipSequence::isPositionAncestral(lastAncestralPosition, aRandomSequence)){
			lastAncestralPosition--;
		}

#ifdef TEST_FOR_COMPLETE_NON_ANCESTRAL_SEQUENCES
		// Issues a warning message only.
		if (lastAncestralPosition < firstAncestralPosition){
			cerr << " Complete non-ancestral sequence present in the ARG.\n";
		}
#endif
		// Then we check if we can recombine this node. If not we return false.
		if (lastAncestralPosition <= firstAncestralPosition){
			return false;
		}

		// If we can we select a uniform point between the last an the first ancestral position.
		uniform_int_distribution<size_t> uniformDistribution2(firstAncestralPosition+1,lastAncestralPosition);
		randomPosition = uniformDistribution2(eng);

	}else if (this->recombinationModeFlag == recombinationMode::UNSPECIFIED){
		throw invalid_argument ( string(__FILE__) +':'+ std::to_string(__LINE__)+ " Unspecified recombination flag!");

	}else{
		throw invalid_argument ( string(__FILE__) +':'+ std::to_string(__LINE__)+ "Unknown recombination flag!");
	}

	// And finally we do the event.
	try{
		generateARecombinationEvent( randomPosition , randomNode );
	}catch (const std::exception & exception ){
		throw std::runtime_error(string(__FILE__) + ':' + std::to_string(__LINE__) + ": Error during generation of a recombination event.\n" + exception.what());
	}
	return true;
}


/*********************************************************************************************/
/**                             MUTATION EVENT GENERATOR                                    **/
/*********************************************************************************************/
void ARG::generetaAMutationEvent(size_t positionOfTheMutation, size_t sourceNode) {

	// Gets an iterator on the lives nodes that has the corresponding node.
	vector<size_t>::iterator it;
	it = find(this->positionOfAliveNodes.begin(), this->positionOfAliveNodes.end(), sourceNode);

#ifdef VALIDATION
	if (it == this->positionOfAliveNodes.end()){
		throw std::out_of_range ( string(__FILE__) +':'+ std::to_string(__LINE__)+ std::to_string(sourceNode+1) + " is not a live node." );
	}
	if ( positionOfTheMutation >= this->lenghtOfSequences){
		throw std::out_of_range ( string(__FILE__) + ':' + std::to_string(__LINE__) + ": The position " + std::to_string(positionOfTheMutation+1) + " is invalid.");
	}
	if ( this->numberOfMutantSNPsByPosition.at(positionOfTheMutation) != 1){
		throw std::logic_error( string(__FILE__) + ':' + std::to_string(__LINE__) + ": The position " + std::to_string(positionOfTheMutation+1) + " cannot mutate.");
	}
#endif

	// Creates the new node.
	SnipSequence sequence = SnipSequence::mutateAPosition(this->ARGnodes.at(sourceNode).sequence,positionOfTheMutation);
	ARGnode aMutationNode(ARGNodeType::MUTATIONODE, sequence, sourceNode, positionOfTheMutation, {} );

	// Add the new mutated node at the end of the nodes.
	this->ARGnodes.push_back(aMutationNode);

	// Replaces the original source node in the alive vector by the new one.
	(*it) =  this->ARGnodes.size()-1;

	// Adjust counters.
	++this->totalNumberOfMutationEvent;
	--this->numberOfMutationPossible;
	--this->numberOfMutantSNPsByPosition.at(positionOfTheMutation);
	--this->numberOfPositionToBeMuted;

	if ( this->numberOfMutationPossible == 0 ){
		this->isAMutationEventPossible = false;
	}

	// We delete the source nodes coalescence pairs.
	for (auto itr = this->possibleCoalescence.begin(); itr != this->possibleCoalescence.end(); ){
		( (*itr).first == sourceNode || (*itr).second == sourceNode ) ? ( itr = this->possibleCoalescence.erase(itr) ) : (++itr);
	}
	// We then add all the coalescence pairs for the new node.
	for ( size_t i = 0 ; i < this->positionOfAliveNodes.size() ; ++i){
		if (this->positionOfAliveNodes.at(i) != this->ARGnodes.size()-1){
			if (this->ARGnodes.back().sequence == this->ARGnodes.at(this->positionOfAliveNodes.at(i)).sequence){
				this->possibleCoalescence.insert(std::pair<size_t,size_t>{this->positionOfAliveNodes.at(i), this->ARGnodes.size()-1});
			}
		}
	}
	this->numberOfCoalescencePossible = this->possibleCoalescence.size();
	HISTORY(cout << "Mutation on the node " << to_string(sourceNode+1) << " at the position " << to_string(positionOfTheMutation+1) << std::endl;);
}

/*********************************************************************************************/
/**                        RANDOM MUTATION EVENT GENERATOR                                  **/
/*********************************************************************************************/
bool ARG::generateARandomMutationEvent() {

	// Checks if a mutation is possible.
	if (!this->isAMutationEventPossible){ return false;	}

	// Chooses a random mutation between the one that are possible.
	uniform_int_distribution<size_t> uniformDistribution(1, (this->numberOfMutationPossible));
	size_t winningMutation = uniformDistribution(eng);

	// Then we need too find which position it is in the vector;
	size_t posibleMutationCounter = 0, mutationIndex = 0;
	while ( mutationIndex < this->numberOfMutantSNPsByPosition.size()){

		// We add one if the mutation is possible for this index.
		if (this->numberOfMutantSNPsByPosition.at(mutationIndex) == 1){
			++posibleMutationCounter;
		}

		// If we found the mutation.
		if (posibleMutationCounter == winningMutation){
			//	DEBUG( cout << "The index of the random mutation is : " << mutationIndex+1; );

			// Then we need to find which live nodes it is.
			for ( size_t liveNodeIndex = 0 ; liveNodeIndex < this->positionOfAliveNodes.size() ; ++liveNodeIndex){

				if ( SnipSequence::isPositionMutant(mutationIndex, this->ARGnodes.at(this->positionOfAliveNodes.at(liveNodeIndex)).sequence)){
					//		DEBUG(cout << " on the node : " << this->positionOfAliveNodes.at(liveNodeIndex)+1 << "\n";);
					// We generate the mutation
					try{
						generetaAMutationEvent(mutationIndex, this->positionOfAliveNodes.at(liveNodeIndex));
					}catch(const std::exception & exception){
						throw std::runtime_error(string(__FILE__) + ':' + std::to_string(__LINE__) + ": Exception during mutation event generation.\n" + exception.what());
					}

					return true;
				}
			}
			// We didn't find the alive node.
			throw std::runtime_error(string(__FILE__) + ':' + std::to_string(__LINE__) + ": Can't find the live node.");
		}
		++mutationIndex;
	}// End while
	throw std::runtime_error(string(__FILE__) + ':' + std::to_string(__LINE__) + ": Can't find the mutation.");
}




/*********************************************************************************************/
/**                                                           **/
/*********************************************************************************************/
vector<uint16_t> ARG::getLeafReachabilityforALocus(size_t theLocusPosition, size_t nodeIndex ) const {
	assert( theLocusPosition < this->ARGnodes.at(nodeIndex).sequence.getSizeOfSequence() );

	if (this->ARGnodes.at(nodeIndex).typeOfNode == ARGNodeType::LEAFSAMPLEMODE || this->ARGnodes.at(nodeIndex).typeOfNode == ARGNodeType::COALESCENSENODE ){
		return this->ARGnodes.at(nodeIndex).leafReachabilityForALocus.at(theLocusPosition);
	}else if(this->ARGnodes.at(nodeIndex).typeOfNode == ARGNodeType::MUTATIONODE){
		return getLeafReachabilityforALocus( theLocusPosition, this->ARGnodes.at(nodeIndex).positionOfDataSource);
	}else{ // We are in a recombination node.

		if ( SnipSequence::isPositionAncestral(theLocusPosition, this->ARGnodes.at(nodeIndex).sequence)){
			return getLeafReachabilityforALocus(theLocusPosition, this->ARGnodes.at(nodeIndex).positionOfDataSource);
		}else{
			return {}; // C++11
		}
	}
}

/*********************************************************************************************/
/**                                                           **/
/*********************************************************************************************/
string ARG::getPartialTreeForALocusInNewick(size_t theLocusIndex){
	//string result = "";
	/*
	if (this->ARGnodes.back().typeOfNode != ARGNodeType::COALESCENSENODE){
		throw std::runtime_error (string(__FILE__) + ':' + std::to_string(__LINE__) + " : Unimplemented for non coalescent MRCA or last node in data!" );
	}else{
		if( SnipSequence::isPositionAncestral(theLocusIndex,this->ARGnodes.at(this->ARGnodes.back().positionOfDataSource).sequence ) &&
				SnipSequence::isPositionAncestral(theLocusIndex,this->ARGnodes.at(this->ARGnodes.back().secondDataPoint).sequence ) ){
			result = "( " + getPartialTreeForALocusInNewick(theLocusIndex,this->ARGnodes.back().positionOfDataSource)+ "," +")MRCA";
		}
	}
	 */
	return getPartialTreeForALocusInNewick(theLocusIndex,this->ARGnodes.size()-1) + ";";
}

string ARG::getPartialTreeForALocusInNewick(size_t theLocusIndex, size_t nodeIndex){
	assert (nodeIndex < this->ARGnodes.size() );
	assert (theLocusIndex < this->lenghtOfSequences );

	if ( this->ARGnodes.at(nodeIndex).typeOfNode == ARGNodeType::COALESCENSENODE ){
		if ( SnipSequence::isPositionAncestral(theLocusIndex, this->ARGnodes.at(this->ARGnodes.at(nodeIndex).positionOfDataSource).sequence) &&
				SnipSequence::isPositionAncestral(theLocusIndex, this->ARGnodes.at(this->ARGnodes.at(nodeIndex).secondDataPoint).sequence)	) {
			return "(" +getPartialTreeForALocusInNewick(theLocusIndex,this->ARGnodes.at(nodeIndex).positionOfDataSource) + "," +
					getPartialTreeForALocusInNewick(theLocusIndex,this->ARGnodes.at(nodeIndex).secondDataPoint) + ")" + std::to_string(nodeIndex);

		}else if ( SnipSequence::isPositionAncestral(theLocusIndex, this->ARGnodes.at(this->ARGnodes.at(nodeIndex).positionOfDataSource).sequence) &&
				!(SnipSequence::isPositionAncestral(theLocusIndex, this->ARGnodes.at(this->ARGnodes.at(nodeIndex).secondDataPoint).sequence)) ){

			return getPartialTreeForALocusInNewick(theLocusIndex,this->ARGnodes.at(nodeIndex).positionOfDataSource) + "-" +std::to_string(nodeIndex);

			/*
			return "(" + getPartialTreeForALocusInNewick(theLocusIndex,this->ARGnodes.at(nodeIndex).positionOfDataSource) + ","
					   + std::to_string(this->ARGnodes.at(nodeIndex).secondDataPoint) + ")"+ std::to_string(nodeIndex);
			 */
		}else if ( !(SnipSequence::isPositionAncestral(theLocusIndex, this->ARGnodes.at(this->ARGnodes.at(nodeIndex).positionOfDataSource).sequence)) &&
				SnipSequence::isPositionAncestral(theLocusIndex, this->ARGnodes.at(this->ARGnodes.at(nodeIndex).secondDataPoint).sequence) ){

			/*
			  return "("+getPartialTreeForALocusInNewick(theLocusIndex,this->ARGnodes.at(nodeIndex).secondDataPoint)+ ","
					   + std::to_string(this->ARGnodes.at(nodeIndex).positionOfDataSource) + ")"+ std::to_string(nodeIndex);
			 */
			return getPartialTreeForALocusInNewick(theLocusIndex,this->ARGnodes.at(nodeIndex).secondDataPoint)+ "-" +std::to_string(nodeIndex);
		}else {
			return "";
		}
	}else if ( this->ARGnodes.at(nodeIndex).typeOfNode == ARGNodeType::RECOMBINATIONODE ){
		if ( SnipSequence::isPositionAncestral( theLocusIndex , this->ARGnodes.at(this->ARGnodes.at(nodeIndex).positionOfDataSource).sequence)) {
			return  getPartialTreeForALocusInNewick(theLocusIndex, this->ARGnodes.at(nodeIndex).positionOfDataSource);//+ "-" +std::to_string(nodeIndex);
		}else {
			return "";
		}
	}
	else if (this->ARGnodes.at(nodeIndex).typeOfNode == ARGNodeType::MUTATIONODE){
		return getPartialTreeForALocusInNewick(theLocusIndex, this->ARGnodes.at(nodeIndex).positionOfDataSource);//+ "-" +std::to_string(nodeIndex);
	}else { // Leaf node !
		return std::to_string(nodeIndex);
	}
}


/*********************************************************************************************/
/**                          ARG SUMMARY                                                    **/
/*********************************************************************************************/
void ARG::printARGSummary() {
	size_t sum = 0;
	for (size_t i = 0 ;  i < this->numberOfMutantSNPsByPosition.size() ; ++i ){
		sum += this->numberOfMutantSNPsByPosition.at(i);
	}

	cout << "Basic attributes.\n" ;
	cout << " - Length of sequences   : " << this->lenghtOfSequences  << "\n";
	cout << " - Number of sequences   : " << this->numberOfSequences  << "\n";
	cout << " - Total number of nodes : " << this->ARGnodes.size() << "\n";
	cout << " - Total of lives nodes  : " << this->positionOfAliveNodes.size() << "\n";
	cout << " - Sum of mutant loci    : " << sum << "\n";
	cout << " - Maximum generation    : " << this->maxGenerationSize << "\n\n";

	cout << "Quantity of each type of event in the current state of the ARG." << "\n";
	cout << " - Mutation event      : " << this->totalNumberOfMutationEvent << "\n";
	cout << " - Coalescence event   : " << this->totalNumberOfCoalescenceEvent << "\n";
	cout << " - Recombination event : " << this->totalNumberOfRecombinationEvent << "\n\n";

	cout << "Quantity of possible events for each type.\n";
	cout << " - Possible mutation event        : " << this->numberOfMutationPossible << "\n";
	cout << " - Possible coalescence event     : " << this->numberOfCoalescencePossible << "\n";
	cout << " - Number of position to be muted : " << this->numberOfPositionToBeMuted << "\n" << std::endl;

	/*
	cout << "Data arrays.\n" ;
	cout << " - Vector of mutant snps for each locus : ";
	size_t sum = 0;
	for (size_t i = 0 ;  i < this->numberOfMutantSNPsByPosition.size() ; ++i ){
		cout << '[' << i+1 << "] = " << this->numberOfMutantSNPsByPosition.at(i) << ' ';
		sum += this->numberOfMutantSNPsByPosition.at(i);
	}

	cout << "\n - Sum of mutant loci : " << sum << "\n";

	cout << " - Coordinates                          : ";
	for (size_t i = 0 ; i < this->SnipsDistancesInDNA.size(); ++i ){
		cout << '[' << i+1 << "] = " << this->SnipsDistancesInDNA.at(i) << ' ';
	}

	cout << "\n - Phenotypes                           : ";
	for (size_t i = 0 ; i < this->phenotypes.size(); ++i ){
		cout << '[' << i+1 << "] = ";
		if (this->phenotypes.at(i)){ cout << '1'; } else {cout << '0';}
		cout << ' ';
	}
	 */
}

/*********************************************************************************************/
/**                                                                                         **/
/*********************************************************************************************/
void ARG::printLivesNodes() {
	cout << "             *** Lives nodes in the ARG  ***\n";

	// Prints lives nodes index in the argNodes vector
	cout << this->positionOfAliveNodes.size() << " nodes lives with thoses position in ARGnodes vector: ";
	for (size_t i = 0 ; i < this->positionOfAliveNodes.size(); ++i){
		cout << this->positionOfAliveNodes.at(i)+1 << ' ';
	}
	cout << "\n";

	// Prints the details on all lives nodes.
	for(const auto &i : this->positionOfAliveNodes){
		cout << '(' << i+1 << ") "<< this->ARGnodes.at(i) << "\n";
	}
	cout << "\n";
}


void ARG::printCoalescencePairs(){
	cout << "{ ";
	for (auto itr = this->possibleCoalescence.begin(); itr != this->possibleCoalescence.end();){
		cout << "{" << (*itr).first << "," << (*itr).second << "}";
		++itr;
		if(itr != this->possibleCoalescence.end()){
			cout << ", ";
		}else{
			cout << " ";
		}
	}
	cout << "}";
}

/*********************************************************************************************/
/**                                                                                         **/
/*********************************************************************************************/
void ARG::dumpARGDataInFile(string aFileName){

	std::ofstream outputStream(aFileName);

	if( outputStream ){
		for( size_t i = 0 ; i < this->ARGnodes.size() ; ++i){
			outputStream << "Nodes #" << i+1 << " " << this->ARGnodes.at(i)<< "\n";
		}
	}else{
		std::cerr << "Error : opening output stream.\n";
	}
	cout << "\nFile : \"" << aFileName << "\" successfully generated!\n\n";
	outputStream.close();
}






/*********************************************************************************************/
/*********************************************************************************************/
/*********************************************************************************************/
/*                           __ _        _   _     _   _                                     */
/*                          / _\ |_ __ _| |_(_)___| |_(_) ___ ___                            */
/*                          \ \| __/ _` | __| / __| __| |/ __/ __|                           */
/*                          _\ \ || (_| | |_| \__ \ |_| | (__\__ \                           */
/*                          \__/\__\__,_|\__|_|___/\__|_|\___|___/                           */
/*                                                                                           */
/*********************************************************************************************/
/*********************************************************************************************/
/*********************************************************************************************/

/*   ________________________________________________________________________________
    | Allele/ Phenotype |      Case                Control       |   Total           |
    |   Mutant          |  caseAndMutant      controlAndMutant   | numberOfMutant    |
    |  primitive        | caseAndPrimitive   controlAndPrimitive | numberOfPrimitive |
    |    Total          |   numberOfCase      numberOfConctrol   |                   |
     --------------------------------------------------------------------------------
 */
long double ARG::chi2TestForASetOfLeavesAndAnLocus(vector<uint16_t> theLeaves, size_t aLocus){
	size_t numberOfCase = 0;
	size_t numberOfControl = 0;
	size_t numberOfmutant = 0;
	size_t numberOfPrimitive = 0;

	size_t caseAndMutant = 0;
	size_t caseAndPrimitive = 0;
	size_t controlAndMutant = 0;
	size_t controlAndPrimitive = 0;

	for ( size_t i = 0 ; i < theLeaves.size() ; ++i ){

		if ( this->phenotypes.at(theLeaves.at(i)/2) ){
			++numberOfCase;
			if( SnipSequence::isPositionMutant(aLocus, this->ARGnodes.at(theLeaves.at(i)).sequence)){
				++caseAndMutant;
				++numberOfmutant;
			}else{
				++caseAndPrimitive;
				++numberOfPrimitive;
			}
		}else{
			++numberOfControl;
			if( SnipSequence::isPositionMutant(aLocus, this->ARGnodes.at(theLeaves.at(i)).sequence)){
				++controlAndMutant;
				++numberOfmutant;
			}else{
				++controlAndPrimitive;
				++numberOfPrimitive;
			}
		}
	}

	if(numberOfPrimitive + numberOfmutant  != theLeaves.size()){
		throw std::runtime_error( string(__FILE__) + ':' + std::to_string(__LINE__) + " Warning numberOfPrimitive + numberOfmutant  != theLeaves.size() !" );
	}

	if ( numberOfPrimitive + numberOfmutant != numberOfCase + numberOfControl ){
		throw std::runtime_error( string(__FILE__) + ':' + std::to_string(__LINE__) + " Warning numberOfPrimitive + numberOfmutant != numberOfCase + numberOfControl!" );
	}
	if ( caseAndMutant + controlAndMutant != numberOfmutant ){
		throw std::runtime_error( string(__FILE__) + ':' + std::to_string(__LINE__) + " Warning caseAndMutant + controlAndMutant != numberOfmutant !" );
	}
	if ( caseAndPrimitive + controlAndPrimitive != numberOfPrimitive ){
		throw std::runtime_error( string(__FILE__) + ':' + std::to_string(__LINE__) + " Warning caseAndPrimitive + controlAndPrimitive != numberOfPrimitive !" );
	}
	if( caseAndMutant + caseAndPrimitive != numberOfCase ){
		throw std::runtime_error( string(__FILE__) + ':' + std::to_string(__LINE__) + " Warning caseAndMutant + caseAndPrimitive != numberOfCase !" );
	}
	if( controlAndMutant + controlAndPrimitive != numberOfControl ){
		throw std::runtime_error( string(__FILE__) + ':' + std::to_string(__LINE__) + " Warning controlAndMutant + controlAndPrimitive != numberOfControl !" );
	}

	double chi2CaseMutant;
	if (numberOfCase*numberOfmutant == 0){
		chi2CaseMutant = 0;
	}else{
		chi2CaseMutant = pow( ( caseAndMutant-(double)numberOfCase*numberOfmutant/theLeaves.size() ), 2 )/((double)numberOfCase*numberOfmutant/theLeaves.size());
	}

	double chi2CasePrimitive;
	if (numberOfCase*numberOfPrimitive== 0){
		chi2CasePrimitive = 0;
	}else{
		chi2CasePrimitive = pow((caseAndPrimitive-(double)numberOfCase*numberOfPrimitive/theLeaves.size()), 2)/((double)numberOfCase*numberOfPrimitive/theLeaves.size());
	}

	double chi2ControlMutant;
	if (numberOfControl*numberOfmutant == 0){
		chi2ControlMutant = 0;
	}else{
		chi2ControlMutant = pow((controlAndMutant-(double)numberOfControl*numberOfmutant/theLeaves.size()), 2)/((double)numberOfControl*numberOfmutant/theLeaves.size());
	}

	double chi2ControlPrimitive;
	if (numberOfControl*numberOfPrimitive == 0){
		chi2ControlPrimitive = 0;
	}else{
		chi2ControlPrimitive = 	pow((controlAndPrimitive-(double)numberOfControl*numberOfPrimitive/theLeaves.size()),2)/((double)numberOfControl*numberOfPrimitive/theLeaves.size());
	}

	if ( aLocus == 618 ||aLocus == 619 ){
		cout << "Tables for position " << aLocus << "\n";
		cout << "           case   control Total\n" ;
		cout << "   Mutant : " << caseAndMutant  << "    " << controlAndMutant      << "  " << numberOfmutant    << "\n";
		cout << "Primitive : " << caseAndPrimitive << "    " << controlAndPrimitive << "  " << numberOfPrimitive << "\n";
		cout << "    Total : " << numberOfCase << "    " << numberOfControl << "\n";
		cout << "           case   control Total" << "\n";
		cout << "   Mutant : " << (double)numberOfCase*numberOfmutant/theLeaves.size()  << "    " << (double)numberOfControl*numberOfmutant/theLeaves.size()      << "  " << numberOfmutant    << "\n";
		cout << "Primitive : " << (double)numberOfCase*numberOfPrimitive/theLeaves.size() << "    " << (double)numberOfControl*numberOfPrimitive/theLeaves.size() << "  " << numberOfPrimitive << "\n";
		cout << "    Total : " << numberOfCase << "   " << numberOfControl << "\n";
		cout << "x = " << chi2CaseMutant       << "\n";
		cout << "y = " << chi2ControlMutant    << "\n";
		cout << "w = " << chi2CasePrimitive    << "\n";
		cout << "z = " << chi2ControlPrimitive << "\n";
		cout << chi2CaseMutant + chi2CasePrimitive + chi2ControlMutant + chi2ControlPrimitive << "\n";
	}


	return chi2CaseMutant + chi2CasePrimitive + chi2ControlMutant + chi2ControlPrimitive;
}


/*********************************************************************************************/
/**                                                          **/
/*********************************************************************************************/
vector<long double> ARG::chi2AssociationTestResults(){
	vector<long double> resultOfChiSquaredTest;
	vector<uint16_t> allLeavesIndex;

	// We create a vector containing all the leaves indexes.
	for ( uint16_t i = 0 ; i < this->numberOfSequences ; ++i ){
		allLeavesIndex.push_back(i);
	}
	// For each locus we compute the statistic.
	for (size_t i = 0 ; i < this->lenghtOfSequences ; ++i){
		resultOfChiSquaredTest.push_back(chi2TestForASetOfLeavesAndAnLocus(allLeavesIndex,i));
	}
	return resultOfChiSquaredTest;
}

/*********************************************************************************************/
/**                                                          **/
/*********************************************************************************************/
//
//vector<long double> ARG::AvgChi2PValuesTestForTheARG(){
//	long double resultOfChiSquaredTest;
//	vector<long double> pvalues(this->lenghtOfSequences) ;
//
//	boost::math::chi_squared_distribution<double> aDistribution(1);
//
//	// We iterates on all the Loci on the sequences.
//	for (size_t i = 0 ; i < this->lenghtOfSequences ; ++i){
//		// We iterates only on coalescence nodes except the MRCA.
//		for ( size_t j = this->numberOfSequences ; j < this->ARGnodes.size()-1  ; ++j){
//			// Verify wich kind of node we are on.
//			if (this->ARGnodes.at(j).typeOfNode == ARGNodeType::COALESCENSENODE){
//				// Checks that the position reach at leats some leaf. i.e. It is not non-ancestral material.
//				if(getLeafReachabilityforALocus(i,j).size() != 0 ) {
//					// We extract the partial tree.
//					vector<uint16_t> leavesList = getLeafReachabilityforALocus(i,j);
//					// We compute the chi2 stat.
//					resultOfChiSquaredTest = chi2TestForASetOfLeavesAndAnLocus(leavesList, i );
//					/*
//					if (i == 4){
//						cout << "{";
//						for ( size_t k = 0 ; k < leavesList.size()-1 ; ++k){
//							cout << leavesList.at(k) << ", ";
//
//						}
//						cout << leavesList.back() <<"} -> node index : " << j << " seq : " << this->ARGnodes.at(j) << " chi2 = " << resultOfChiSquaredTest << std::endl;
//					}
//					 */
//					// We average the pvalues.
//					// But this.totalNumberOfCoalescenceEvent != of the number of times we reach here !! Possible bias!
//					// TODO Fabrice Check.
//					pvalues.at(i) += (1-boost::math::cdf(aDistribution, resultOfChiSquaredTest))/this->totalNumberOfCoalescenceEvent;
//				}
//			}
//		}
//	}
//	return pvalues;
//}

vector<long double> ARG::MaxChi2TestForTheARG(){
	long double resultOfChiSquaredTest;
	vector<long double> chi2Results(this->lenghtOfSequences);

	// We iterates on all the Loci on the sequences.
	for (size_t i = 0 ; i < this->lenghtOfSequences ; ++i){
		// We iterates only on coalescence nodes except the MRCA.
		for ( size_t j = this->numberOfSequences ; j < this->ARGnodes.size()-1  ; ++j){
			// Verify wich kind of node we are on.
			if (this->ARGnodes.at(j).typeOfNode == ARGNodeType::COALESCENSENODE){
				// Checks that the position reach at least some leaf. i.e. It is not non-ancestral material.
				if(getLeafReachabilityforALocus(i,j).size() != 0 ) {
					// We extract the partial tree.
					vector<uint16_t> leavesList = getLeafReachabilityforALocus(i,j);
					// We compute the chi2 stat.
					resultOfChiSquaredTest = chi2TestForASetOfLeavesAndAnLocus(leavesList, i );
					// We update the result vector if we found one result bigger.
					if (chi2Results.at(i) <  resultOfChiSquaredTest ){
						chi2Results.at(i) = resultOfChiSquaredTest;
					}
				}
			}
		}
	}
	return chi2Results;
}

