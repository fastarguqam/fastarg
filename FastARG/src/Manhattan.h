/*
 * Manhattan.h
 *
 *  Created on: 2017-02-23
 *      Author: �ric Marcotte
 */

#ifndef MANHATTAN_H_
#define MANHATTAN_H_

#include "ARG.h"

class Manhattan: public ARG {
public:

	Manhattan(Parameters params, vector<ARGnode> Leaves, const vector<bool> & phenotypes,const  vector<size_t> & distances);
	void generatesAnManhattanRecombinationEvents();
	void generatesAnManhattanRecombinationEvents(size_t aLiveNode);
	std::pair<size_t,size_t> nextRecombinationEvent (size_t theliveNode );
	static size_t getRightMostIndex (const SnipSequence &, const SnipSequence & );
	void printARGSummary();
	virtual void run(void) override;
	virtual ~Manhattan();
};

#endif /* MANHATTAN_H_ */
