/*
 * ARGnode.h
 *
 *  Created on: 2015-09-04
 *      Author: �ric Marcotte
 */

#ifndef ARGNODE_H_
#define ARGNODE_H_

#include <iostream>
#include <vector>
#include "SnipSequence.h"

enum class ARGNodeType {LEAFSAMPLEMODE,MUTATIONODE,RECOMBINATIONODE,COALESCENSENODE};

using std::vector;

class  ARGnode {
public:
	size_t numberOfPrimitiveLoci;
	size_t numberOfMutantLoci;
	size_t numberOfNonAncestralLoci;

	SnipSequence sequence;
	uint16_t positionOfDataSource;
	size_t secondDataPoint;
	ARGNodeType typeOfNode;
	// TODO vector of sets ?
	vector<vector<uint16_t>> leafReachabilityForALocus;

	// Constructors.
	ARGnode(ARGNodeType aType, SnipSequence ASequence, uint16_t positionOfTheNodeInData,size_t secondDataPoint, vector<vector<uint16_t>> leafReach );

	// Overloading of << operator.
	friend std::ostream& operator << (std::ostream &, const ARGnode&);
};

#endif /* ARGNODE_H_ */
