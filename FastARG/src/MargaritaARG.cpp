/*
 * MargaritaARG.cpp
 *
 *  Created on: 2016-09-22
 *      Author: �ric Marcotte
 */

#include<boost/progress.hpp>

#include "MargaritaARG.h"

using std::cout;
using std::endl;
using std::uniform_int_distribution;
using std::uniform_real_distribution;
using std::invalid_argument;
using std::to_string;

#define PROGRESS_DISPLAY_FREQUENCY 10

MargaritaARG::MargaritaARG(Parameters params, vector<ARGnode> Leaves, const vector<bool>& phenotypes, const vector<size_t>& distances)
// Call to super !!
:ARG( params, Leaves,  phenotypes,  distances)
{
	this->MaximumRecombinationRate = params.MaximumRecombinationRate;
	this->numberOfRandomRecombinaisonEvent = 0;
}



void MargaritaARG::run(void) {

	uniform_int_distribution<size_t> randomCoalMutDist(0,1);
	uniform_real_distribution<double> maximumRecombinationDistribution(0.0,1.0);

	boost::progress_display show_progress( numberOfPositionToBeMuted );

	// We continue the algorithm until we found the MRCA.
	while (this->positionOfAliveNodes.size() > 1 ){

		show_progress += this->totalNumberOfMutationEvent - show_progress.count();


		// We need to make all the coalescence an mutation in a random order.
		bool flag = true;
		while (flag){

			try{
				if (randomCoalMutDist(this->eng)){ // 50% true 50% false
					flag = generateARandomCoalescentEvent();
					if (!flag){ // if no coalescence is possible make try to make an mutation
						flag = generateARandomMutationEvent(); // if it the function returns false the we exit the while loop.
					}
				}else{
					flag = generateARandomMutationEvent(); // Same as above but inverted.
					if (!flag){
						flag = generateARandomCoalescentEvent();
					}
				}
			}catch(const std::exception & excetion){
				throw std::runtime_error (string(__FILE__) +':'+ to_string(__LINE__) + ": An runtime exception occurred while generating all possible mutation and coalescent event.\n" + excetion.what());
			}

		} // End while

		// Then we need to generate a "Margarita" recombination.
		if(this->positionOfAliveNodes.size() > 1){
			bool recombinationTypeFlag = maximumRecombinationDistribution(this->eng) <= this->MaximumRecombinationRate; // Random boolean flag.
			if (recombinationTypeFlag){ // margaritaSharedTrackMode
				try{
					generateARandomOptimalMargaritaRecombinationEvent();
					//generateARandomMargaritaRecombinationEvent(); //
				}catch(const std::exception & excetion){
					throw std::runtime_error (string(__FILE__) +':'+ to_string(__LINE__) + ": An runtime exception occurred while generation a random Margarita recombination event.\n" +excetion.what());
				}
			}else{ // randomRecombinationMode
				try{
					generateARandomMargaritaRecombinationEvent(); //
					++this->numberOfRandomRecombinaisonEvent;
					//generateARandomRecombinationEvent(); // This will either make a completely random one or one that can't produce totally non-ancestral sequences.
				}catch(const std::exception & excetion){
					throw std::runtime_error (string(__FILE__) +':'+ to_string(__LINE__) + ": An runtime exception occurred while generation a random recombination event.\n" + excetion.what());
				}

			}
		}
	}
	show_progress += this->totalNumberOfMutationEvent - show_progress.count();
}// End run()

std::pair<size_t,size_t> MargaritaARG::bestAlphaBeta(size_t & firstNode,size_t & secondNode){

	std::pair<size_t,size_t> alphaBeta = std::make_pair(0,0);
	std::pair<size_t,size_t> bestAlphaBeta = std::make_pair(0,0);

	for (size_t i = 0 ;i < this->positionOfAliveNodes.size();++i){
		for (size_t j = i+1 ; j < this->positionOfAliveNodes.size();++j){
			alphaBeta = margaritaCommonTractFinder(this->ARGnodes.at(this->positionOfAliveNodes.at(i)).sequence,this->ARGnodes.at(this->positionOfAliveNodes.at(j)).sequence);
			if( (alphaBeta.second-alphaBeta.first) > (bestAlphaBeta.second-bestAlphaBeta.first) ){
				bestAlphaBeta = alphaBeta;
				firstNode = i;
				secondNode = j;
			}
		}
	}
	return bestAlphaBeta;
}

bool MargaritaARG::generateARandomOptimalMargaritaRecombinationEvent(){

	// Initialize two variables that will get the right alive nodes.
	size_t firstNode= 0, secondNode =  0;

	// And then we extract the longest common sequence.
	std::pair<size_t,size_t> alphaBeta = bestAlphaBeta(firstNode,secondNode);
	try{
		// If the is no common material we return false.
		if (alphaBeta.first == 0 && alphaBeta.second == 0){
			return false;
		}else{
			// If there is common material we need to check if one break point or two break points are required.
			if (alphaBeta.first != 0 && alphaBeta.second < this->ARGnodes.at(0).sequence.getSizeOfSequence()){
				// We need to make two recombination.
				generateARecombinationEvent(alphaBeta.first,this->positionOfAliveNodes.at(firstNode));
				generateARecombinationEvent(alphaBeta.second,this->ARGnodes.size()-2);
				generateACoalescentEvent(this->ARGnodes.size()-1,this->positionOfAliveNodes.at(secondNode));

			}else if (alphaBeta.first == 0 && alphaBeta.second < this->lenghtOfSequences ){
				// Then we know that the one before the last node inserted will be a the end of the lives nodes vector and will have a mask for the left side.
				generateARecombinationEvent(alphaBeta.second,this->positionOfAliveNodes.at(firstNode));
				generateACoalescentEvent(this->ARGnodes.size()-1,this->positionOfAliveNodes.at(secondNode));

			}else if (alphaBeta.first != 0 && alphaBeta.second == this->lenghtOfSequences ){
				// Then we know that the last live inserted will be a the end of the lives nodes vector and will have a mask for the left side.
				generateARecombinationEvent(alphaBeta.first,this->positionOfAliveNodes.at(firstNode));
				generateACoalescentEvent(this->ARGnodes.size()-2,this->positionOfAliveNodes.at(secondNode));

			}else{
				throw std::logic_error( string(__FILE__) +':'+ to_string(__LINE__) + ": Bad optimum margarita recombination placement! i.e. it should be a coalescence,");
			}
		}
	}catch(const std::exception & exception){
		throw std::logic_error( string(__FILE__) +':'+ to_string(__LINE__) + ": An error has occured while generating an recombination event.\n" + exception.what());
	}
	return true;
}

bool MargaritaARG::generateARandomMargaritaRecombinationEvent(){

	// First we choose two random nodes between all on the possibles ones.
	uniform_int_distribution<size_t> intDistribution(0,this->positionOfAliveNodes.size()-1);
	size_t firstNode =  intDistribution(this->eng);
	size_t secondNode =  intDistribution(this->eng);

	// We makes sure they are not the same !
	while (firstNode == secondNode){ secondNode =  intDistribution(this->eng);}

	// Now we get the snipsSequence for those to nodes.
	SnipSequence firstSeq = this->ARGnodes.at(this->positionOfAliveNodes.at(firstNode)).sequence;
	SnipSequence secondSeq = this->ARGnodes.at(this->positionOfAliveNodes.at(secondNode)).sequence;

	// And then we extract the longest common sequence between thoses 2 sequences only.
	std::pair<size_t,size_t> alphaBeta;

	alphaBeta = margaritaCommonTractFinder(firstSeq,secondSeq);

	try{
		// If the is no common material we return false.
		if (alphaBeta.first == 0 && alphaBeta.second == 0){
			return false;
		}else{
			// If there is common material we need to check if one break point or two break points are required.
			if (alphaBeta.first != 0 && alphaBeta.second < this->ARGnodes.at(0).sequence.getSizeOfSequence()){
				//
				generateARecombinationEvent(alphaBeta.first,this->positionOfAliveNodes.at(firstNode));
				generateARecombinationEvent(alphaBeta.second,this->ARGnodes.size()-2);
				generateACoalescentEvent(this->ARGnodes.size()-1,this->positionOfAliveNodes.at(secondNode));

			}else if (alphaBeta.first == 0 && alphaBeta.second < this->ARGnodes.at(0).sequence.getSizeOfSequence()){
				// Then we know that the one before the last node inserted will be a the end of the lives nodes vector and will have a mask for the left side.
				generateARecombinationEvent(alphaBeta.second,this->positionOfAliveNodes.at(firstNode));
				generateACoalescentEvent(this->ARGnodes.size()-1,this->positionOfAliveNodes.at(secondNode));

			}else if (alphaBeta.first != 0 && alphaBeta.second == this->ARGnodes.at(0).sequence.getSizeOfSequence() ){
				// Then we know that the last live inserted will be a the end of the lives nodes vector and will have a mask for the left side.
				generateARecombinationEvent(alphaBeta.first,this->positionOfAliveNodes.at(firstNode));
				generateACoalescentEvent(this->ARGnodes.size()-2,this->positionOfAliveNodes.at(secondNode));
			}else{
				throw std::logic_error( string(__FILE__) +':'+ to_string(__LINE__) + ": Bad margarita recombination placement! i.e. it should be a coalescence,");
			}
		}
	}catch(const std::exception & exception){
		throw std::logic_error( string(__FILE__) +':'+ to_string(__LINE__) + ": An error has occured while generating an recombination event.\n" + exception.what());
	}
	return true;
}

std::pair<size_t, size_t> MargaritaARG::margaritaCommonTractFinder( const SnipSequence& firstSequence, const SnipSequence& secondSequence) {
	assert (firstSequence.getSizeOfSequence() ==  secondSequence.getSizeOfSequence());

	std::valarray<DATA_TYPE> A = firstSequence.AncestralDataMask;
	std::valarray<DATA_TYPE> B = firstSequence.snipsData;
	std::valarray<DATA_TYPE> C = secondSequence.AncestralDataMask;
	std::valarray<DATA_TYPE> D = secondSequence.snipsData;


	// First we must found the common ancestral material. Mark with a one when true.
	//	A	B	Y
	//	0	0	1
	//	0	1	0
	//	1	0	0
	//	1	1	0
	std::valarray<DATA_TYPE> knownAncestralMaterial = ((~A)&(~C));
	// 	        M1  D1 	M2	D2	Result
	// 0	    0	0	0	0	  1
	// 1	    0	0	0	1	  0  <- 0 - 1 incompatible
	// 2	    0	0	1	0	  1
	// 3	    0	0	1	1	  1
	// 4	    0	1	0	0	  0  <- 0 - 1 incompatible
	// 5	    0	1	0	1	  1
	// 6	    0	1	1	0	  1
	// 7	    0	1	1	1	  1
	// 8	    1	0	0	0	  1
	// 9	    1	0	0	1	  1
	// 10   	1	0	1	0	  1
	// 11	    1	0	1	1	  1
	// 12	    1	1	0	0	  1
	// 13	    1	1	0	1	  1
	// 14	    1	1	1	0	  1
	// 15	    1	1	1	1	  1

	// This check for 0-1 or 1-0 incompatibilities.
	std::valarray<DATA_TYPE> result = ( C | A | ((~B)&(~D)) | ((B)&(D)) );

	// Now it is "simply" to find the longest tract of one and return the index;
	std::pair<size_t,size_t> bestPair = std::make_pair(0,0);
	std::pair<size_t,size_t> curentPair = std::make_pair(0,0);
	DATA_TYPE binaryIndicator = BIT_INDICATOR;
	DATA_TYPE resultBlock = 0;
	bool flagCounterOn = false;
	bool flagHaveAtLeastOneCommonAncestorPosition = false;
	size_t maxJ;

	for (size_t i = 0 ; i < firstSequence.getnumberOfBlock();++i){
		resultBlock = result[i];
		// Check if we are on the last block.

		// If we are we only go trough until the end of the last locus.
		if ( i == firstSequence.getnumberOfBlock()-1 ){
			maxJ = firstSequence.getSizeOfSequence() % BIT_SIZE_OF_DATA_TYPE;
		}else{ // Else the block is full so we can go trough completely the block.
			maxJ = BIT_SIZE_OF_DATA_TYPE;
		}

		// With the right block and the right limit we can go trough the sequences for a block.
		for (size_t j = 0 ; j < maxJ ; ++j){

			if (resultBlock & binaryIndicator){
				if (knownAncestralMaterial[i] & binaryIndicator){
					flagHaveAtLeastOneCommonAncestorPosition = true;
				}
				if(flagCounterOn){ // if we started to count that is it ok just to move right indicator
					curentPair.second = i*BIT_SIZE_OF_DATA_TYPE+j+1;
					// We need to check if this position is both known on both sequences.

				}else{ // In this case we start to count.
					curentPair.first = i*BIT_SIZE_OF_DATA_TYPE+j;
					curentPair.second = i*BIT_SIZE_OF_DATA_TYPE+j+1;
					flagCounterOn = true;
				}
			}else{ // Other we don't count and put the counter flag to off.
				if(flagCounterOn){
					flagCounterOn = false;
					flagHaveAtLeastOneCommonAncestorPosition = false;
				}
			}
			// Each time we update if we have a better pair result.
			if( (curentPair.second - curentPair.first) >= (bestPair.second - bestPair.first) &&  flagHaveAtLeastOneCommonAncestorPosition){
				bestPair.first = curentPair.first;
				bestPair.second = curentPair.second;
			}
			binaryIndicator = binaryIndicator >> 1;
		}

		binaryIndicator = BIT_INDICATOR;
	}
	return bestPair;
}
void MargaritaARG::printARGSummary() {
	cout << "             ***  Margarita ARG summary *** \n";
	ARG::printARGSummary();

	cout << "Margarita specific attributes :\n";
	cout << " - The seed is                                 : " << this->seed << "\n";
	cout << " - The optimal recombination  rate is          : " << this->MaximumRecombinationRate << std::endl;
	cout << " - The number of random recombination event is : " << this->numberOfRandomRecombinaisonEvent << std::endl;

}

bool MargaritaARG::verifyParameters(const Parameters& parameters) {
	if (parameters.MaximumRecombinationRate <= 0 || parameters.MaximumRecombinationRate  >=1){
		throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + ": The value for \"MargaritaOptimalRecombinationProportion\" needs to set between 0 and 1.");
	}
	return true;
}
