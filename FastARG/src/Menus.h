/*
 * Menu.h
 *
 *  Created on: 2015-12-05
 *      Author: �ric Marcotte
 */

#ifndef MENUS_H_
#define MENUS_H_

#include <iostream>

int mainMenu();
int menuL();
char validateUserChoice(std::string);

#endif /* MENUS_H_ */
