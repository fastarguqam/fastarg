/*
 * Parameters.cpp
 *
 *  Created on: 2016-09-07
 *      Author: �ric Marcotte
 */

#include <fstream>
#include <iostream>
// https://github.com/nlohmann/json
#include "json.hpp"
#include "Parameters.h"
#include "MargaritaARG.h"


using std::cerr;
using std::cout;
using std::endl;
using std::to_string;
using std::ifstream;
using nlohmann::json;
using std::exception;
using std::out_of_range;
using std::invalid_argument;

#define MAX_NUMBER_OF_SEQUENCES 65535

Parameters::Parameters() {
	this->sequencesSize = 0;
	this->numberOfSequences = 0;
	this->algorithmChoice = algorithms::UNSPECIFIED;
	this->dataFile = "";
	this->randomSeed = false;
	this->seed = 1;
	this->MaximumRecombinationRate = 1;
	this->recombinationRate = 0;
	this->mutationRate = 0;
	this->coalescenceRate = 0;
	this->Rplot = false;
}

Parameters::Parameters( string FilePath ) {

	json json;
	ifstream parameterStream(FilePath);

	// Check if the file exist
	if (!parameterStream){
		throw invalid_argument( string(__FILE__) +':'+ to_string(__LINE__)+ ": The parameter file :\"" + FilePath + "\" cannot be found !");
	}

	// Parameters data parsing
	try {
		json << parameterStream;
	} catch (exception & exception){
		throw invalid_argument( string(__FILE__) +':'+ to_string(__LINE__)+ ": An error occurred during the parameter parsing :	" + exception.what());
	}

	// Gets the sequences size attributes
	if (json["lenghtOfSequences"].is_null()){
		throw invalid_argument( string(__FILE__) +':'+ to_string(__LINE__)+ ": A size for the sequences must be specified! ");
	}else{
		if (json["lenghtOfSequences"].is_number_unsigned() ){
			this->sequencesSize = json["lenghtOfSequences"];
		}else{
			throw invalid_argument( string(__FILE__) +':'+ to_string(__LINE__)+ ": The length of sequences needs to be a positive integer! ");
		}
	}

	// Gets the number of sequences
	if (json["numberOfSequences"].is_null()){

		throw std::invalid_argument (string(__FILE__) +':'+to_string(__LINE__) +": A number of sequences must be specified! ");
	}else{
		if (json["numberOfSequences"].is_number_unsigned() ){
			this->numberOfSequences = json["numberOfSequences"];
			if (this->numberOfSequences  > MAX_NUMBER_OF_SEQUENCES ){
				throw invalid_argument( string(__FILE__) +':'+ to_string(__LINE__)+ ": The number of sequences needs to be less than " + std::to_string(MAX_NUMBER_OF_SEQUENCES) + "." );
			}


			if (this->numberOfSequences%2 == 1 ){
				throw invalid_argument( string(__FILE__)+':'+to_string(__LINE__) +": The number of sequences must not be a odd number!");
			}

		}else{
			throw invalid_argument( string(__FILE__)+':'+ to_string(__LINE__) +": The number of sequences needs to be a positive integer ! ");
		}
	}


	// Gets the phenotypes vector
	if (json["phenotypes"].is_null()){
		// throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__)+ ": A \"phenotypes\" must be specified." );
		cerr << "Warning !! No phenotypes vector defined. Generating a random one!" << endl;
		for ( size_t i = 0 ; i <  this->numberOfSequences/2 ; ++i){
			if (std::rand()%2){
				this->phenotypes.push_back(1);
			}else{
				this->phenotypes.push_back(0);
			}
		}
	}else{
		if(json["phenotypes"].is_array()){
			if (json["phenotypes"].size() != this->numberOfSequences/2){
				throw invalid_argument ( string(__FILE__) +':' + to_string(__LINE__)+ ": The \"phenotypes\" must half the number of sequences specified." );
			}else{
				for ( size_t i = 0 ; i < json["phenotypes"].size(); ++i ){
					if(json["phenotypes"].at(i).is_number_unsigned()){
						size_t aPhenotype = json["phenotypes"].at(i);
						if (aPhenotype != 0 && aPhenotype!= 1){
							throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__)+ ": The element a the position : " +  to_string(i+1) + " of the \"phenotypes\" must be 0 or 1." );
						}else{
							if (aPhenotype == 0){
								this->phenotypes.push_back(false);
							}else{
								this->phenotypes.push_back(true);
							}
						}
					}else{
						throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__)+ ": The element a the position : " +  to_string(i+1) + " of the \"phenotypes\" must be an unsigned number." );
					}
				}
				for ( size_t i = 1 ; i < this->locusCoordinates.size(); ++i ){
					if (this->locusCoordinates.at(i-1) >=this->locusCoordinates.at(i) ){
						throw invalid_argument ( string(__FILE__) +':' + to_string(__LINE__)+ " : The element a the position : " +  to_string(i+1) + " of the \"phenotypes\" must be bigger that the one before." );
					}
				}
			}
		}else{
			throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + ": The \"phenotypes\" must be an array.");
		}
	}


	// Gets the position of the snips for the sequences.
	if (json["coordinates"].is_null()){
		cerr << "Warning !! No coordinates vector defined. Generating a one with all snps is 1 bp spaced out!" << endl;
		for ( size_t i = 0 ; i < this->sequencesSize ; ++i){
			this->locusCoordinates.push_back(i+1);
		}
		//throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__)+ ": A \"coordinates\" vector must be specified." );
	}else{
		if(json["coordinates"].is_array()){
			if (json["coordinates"].size() != this->sequencesSize){
				throw invalid_argument ( string(__FILE__) +':' + to_string(__LINE__)+ ": A \"coordinates\" vector must be the same size as the sequence size. Size = " + to_string(this->sequencesSize) + " coordinates size is  " +to_string(json["coordinates"].size()) +"." );
			}else{
				for ( size_t i = 0 ; i < json["coordinates"].size(); ++i ){
					if(json["coordinates"].at(i).is_number_unsigned()){
						this->locusCoordinates.push_back(json["coordinates"].at(i));
					}else{
						throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__)+ ": The element a the position : " +  to_string(i+1) + " of the \"coordinates\" vector must be an unsigned number." );
					}
				}
				for ( size_t i = 1 ; i < this->locusCoordinates.size(); ++i ){
					if (this->locusCoordinates.at(i-1) >= this->locusCoordinates.at(i) ){
						throw invalid_argument ( string(__FILE__) +':' + to_string(__LINE__)+ " : The element (" +to_string(this->locusCoordinates.at(i) )+") at the position : " +  to_string(i+1) + " of the \"coordinates\" vector must be bigger that the one ("+to_string(this->locusCoordinates.at(i-1) )+") before." );
					}
				}
			}
		}else{
			throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + ": The \"coordinateVector\" must be an array.");
		}
	}


	// Gets the data file
	if(json["dataFile"].is_null()){
		throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + ": A data file must be specified!");
	}else{
		if ( json["dataFile"].is_string() ){
			this->dataFile = json["dataFile"];
		}else{
			throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + ": The data file field must be a string!");
		}
	}

	// Gets the R files generation flag.
	if (json["Rplot"].is_null()){
		cerr << "Rplot is not defined, defaulted to false.\n";
		this->Rplot = false;
	}else{
		if (json["Rplot"].is_boolean()){
			this->Rplot = json["Rplot"];
		}else{
			throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + "The Rplot flag must be an boolean!");
		}
	}


	// Gets the algorithm choice
	if (json["algorithm"].is_null()){
		throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + ": An algorithm must be specified.");
	}else{
		if (json["algorithm"].is_string() ){
			//this->algorithmName = json["algorithm"];
			string tempAlgorithmName = json["algorithm"];
			//string tempAlgorithmName = this->algorithmName;
			for (auto & c: tempAlgorithmName) c = toupper(c);

			if ( tempAlgorithmName  == "MARGARITA" ){
				this->algorithmChoice = algorithms::MARGARITA;
			}else if ( tempAlgorithmName  == "MOJITO" ){
				this->algorithmChoice = algorithms::MOJITO;
			}else if ( tempAlgorithmName  == "FEARNHEAD" ){
				this->algorithmChoice = algorithms::FEARNHEAD;
			}else if ( tempAlgorithmName  == "ARG4WG" ){
				this->algorithmChoice = algorithms::ARG4WG;
			}else if ( tempAlgorithmName  == "MANHATTAN" ){
				this->algorithmChoice = algorithms::MANHATTAN;
			}else if  ( tempAlgorithmName  == "MOONSHINE" ){
				this->algorithmChoice = algorithms::MOONSHINE;
			}else{
				string unknownName = json["algorithm"];
				throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + ": The algorithm \""+ unknownName +"\" choice is unknown!");
			}
		}else{
			throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + ": The algorithm choice needs to be a string!");
		}
	}

	// Gets the coalescenceMode
	if (json["coalescenceMode"].is_null()){
		throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + ": An coalescence mode must be specified.");
	}else{
		if (json["coalescenceMode"].is_string() ){
			this->coalescenceModeName = json["coalescenceMode"];
			string tempCoalescenceMode = this->coalescenceModeName;
			for (auto & c: tempCoalescenceMode) c = toupper(c);

			if ( tempCoalescenceMode  == "NO_CONSTRAINT" ){
				this->coalescenceModeChoice = coalescenceMode::NO_CONSTRAINT;
			}else if ( tempCoalescenceMode  == "HAVE_ANCESTRAL_MATERIAL_IN_COMMON" ){
				this->coalescenceModeChoice = coalescenceMode::HAVE_ANCESTRAL_MATERIAL_IN_COMMON;
			}else{
				string unknownName = this->coalescenceModeName;
				throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + ": The coalescence mode  \""+ unknownName +"\" choice is unknown!");
			}
		}else{
			throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + ": The coalescence mode needs to be a string!");
		}
	}

	// Gets the recombinationMode
	if (json["randomRecombinationMode"].is_null()){
		throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + ": An random recombination mode must be specified.");
	}else{
		if (json["randomRecombinationMode"].is_string() ){
			this->recombinationModeName = json["randomRecombinationMode"];
			string tempRecombinationModeName= this->recombinationModeName;
			for (auto & c: tempRecombinationModeName) c = toupper(c);

			if ( tempRecombinationModeName  == "NO_CONSTRAINT" ){
				this->recombinationModeChoice = recombinationMode::NO_CONSTRAINT;
			}else if ( tempRecombinationModeName  == "HAVE_ANCESTRAL_MATERIAL_ONLY" ){
				this->recombinationModeChoice = recombinationMode::HAVE_ANCESTRAL_MATERIAL_ONLY;
			}else{
				string unknownName = this->recombinationModeName;
				throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + ": The random recombination mode  \""+ unknownName +"\" choice is unknown!");
			}
		}else{
			throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + ": The random recombination mode needs to be a string!");
		}
	}


	// Margarita specific parameters
	if(  algorithms::MARGARITA  == this->algorithmChoice){
		// Gets the seed;
		if (json["randomSeed"].is_null()){
			cerr << "randomSeed is not defined, defaulted to false.\n";
			this->randomSeed = false;
		}else{
			if (json["randomSeed"].is_boolean()){
				this->randomSeed = json["randomSeed"];
			}else{
				throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + "The random seed flag must be an boolean!");
			}
		}

		if (json["seed"].is_null()){
			cerr << "seed is not defined, defaulted to 1.\n";
			this->seed = 1;
		}else {
			if (json["seed"].is_number_unsigned()){
				this->seed = json["seed"];
			}else{
				throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + "The seed must be an unsigned number!");
			}
		}


		// Check if rates are assigned and generates a warning only.
		if (!json["recombinationRate"].is_null() || !json["mutationRate"].is_null() || !json["coualescenceRate"].is_null() ){
			cerr << "Warning : events rates are unused in margarita algorithm.\n";
		}
		// Assigns default 0 values;
		this->recombinationRate = 0;
		this->mutationRate = 0;
		this->coalescenceRate = 0;

		// Probability of the "Margarita" specific recombination operation.
		if (json["margaritaOptimalRecombinationProportion"].is_null() ){
			throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + "Margarita algorithm need to have the \"MargaritaOptimalRecombinationProportion\" setting specified.\n");
		}else{
			if (json["margaritaOptimalRecombinationProportion"].is_number_float()){
				this->MaximumRecombinationRate = json["margaritaOptimalRecombinationProportion"];
			}else{
				throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + "Margarita algorithm need to have the \"MargaritaOptimalRecombinationProportion\" setting to be an float.\n");
			}
		}

	}else if( algorithms::MOJITO  == this->algorithmChoice){
		// Check if rates are assigned and generates a warning only.
		if (!json["recombinationRate"].is_null() || !json["mutationRate"].is_null() || !json["coualescenceRate"].is_null() ){
			cerr << "Warning : events rates are unused in margarita algorithm.";
		}
		// Assigns default 0 values;
		this->recombinationRate = 0;
		this->mutationRate = 0;
		this->coalescenceRate = 0;

		if (!json["randomSeed"].is_null() || !json["seed"].is_null() ){
			cerr << "Warning : a seed or a random seed is unused in the Mojito algorithm.";
		}
		// Probability of the "Margarita" specific recombination operation.
		if (!json["margaritaOptimalRecombinationProportion"].is_null() ){
			throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + "Mojito algorithm doesn't need to have the \"MargaritaOptimalRecombinationProportion\" setting specified.\n");
		}

	}else if( algorithms::ARG4WG  == this->algorithmChoice){
		// Check if rates are assigned and generates a warning only.
		if (!json["recombinationRate"].is_null() || !json["mutationRate"].is_null() || !json["coualescenceRate"].is_null() ){
			cerr << "Warning : events rates are unused in margarita algorithm.";
		}
		// Assigns default 0 values;
		this->recombinationRate = 0;
		this->mutationRate = 0;
		this->coalescenceRate = 0;
		this->randomSeed = false;
		this->seed = 1;

		if (!json["randomSeed"].is_null() || !json["seed"].is_null() ){
			cerr << "Warning : a seed or a random seed is unused in the Mojito algorithm.";
		}
		// Probability of the "Margarita" specific recombination operation.
		if (!json["margaritaOptimalRecombinationProportion"].is_null() ){
			throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + "Mojito algorithm doesn't need to have the \"MargaritaOptimalRecombinationProportion\" setting specified.\n");
		}

	}else if( algorithms::MANHATTAN  == this->algorithmChoice){
		// Check if rates are assigned and generates a warning only.
		if (!json["recombinationRate"].is_null() || !json["mutationRate"].is_null() || !json["coualescenceRate"].is_null() ){
			cerr << "Warning : events rates are unused in margarita algorithm.";
		}
		// Assigns default 0 values;
		this->recombinationRate = 0;
		this->mutationRate = 0;
		this->coalescenceRate = 0;

		if (! json["randomSeed"].is_null() ){
			if (json["randomSeed"].is_boolean()){
				this->randomSeed = json["randomSeed"];
			}else{
				throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + "The random seed flag must be an boolean!");
			}
		}else{
			this->randomSeed = false;
		}

		if (json["seed"].is_null()){
			cerr << "Warning the seed isn't defined, defaulted to 1.\n";
			this->seed = 1;
		}else {
			if (json["seed"].is_number_unsigned()){
				this->seed = json["seed"];
			}else{
				throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + "The seed must be an unsigned number!");
			}
		}

		// Probability of the "Margarita" specific recombination operation.
		if (!json["margaritaOptimalRecombinationProportion"].is_null() ){
			throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + "Mojito algorithm doesn't need to have the \"MargaritaOptimalRecombinationProportion\" setting specified.\n");
		}

	}else if( algorithms::MOONSHINE  == this->algorithmChoice){
			// Check if rates are assigned and generates a warning only.
			if (!json["recombinationRate"].is_null() || !json["mutationRate"].is_null() || !json["coualescenceRate"].is_null() ){
				cerr << "Warning : events rates are unused in margarita algorithm.";
			}
			// Assigns default 0 values;
			this->recombinationRate = 0;
			this->mutationRate = 0;
			this->coalescenceRate = 0;

			if (! json["randomSeed"].is_null() ){
				if (json["randomSeed"].is_boolean()){
					this->randomSeed = json["randomSeed"];
				}else{
					throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + "The random seed flag must be an boolean!");
				}
			}else{
				this->randomSeed = false;
			}

			if (json["seed"].is_null()){
				cerr << "Warning the seed isn't defined, defaulted to 1.\n";
				this->seed = 1;
			}else {
				if (json["seed"].is_number_unsigned()){
					this->seed = json["seed"];
				}else{
					throw invalid_argument ( string(__FILE__) +':'+ to_string(__LINE__) + "The seed must be an unsigned number!");
				}
			}



		}else if( algorithms::UNSPECIFIED  == this->algorithmChoice){
		throw invalid_argument( string(__FILE__) +':'+ to_string(__LINE__) + ": The algorithm is unspecified.\n");
	}	else{
		throw invalid_argument( string(__FILE__) +':'+ to_string(__LINE__) + ": The algorithm is unknown.\n");
	}

}

std::ostream& operator<<(std::ostream& os, algorithms c)
{
	switch(c)
	{
	case algorithms::MANHATTAN     : os << "Manhattan";      break;
	case algorithms::ARG4WG        : os << "ARG4WG";         break;
	case algorithms::MOJITO        : os << "Mojitio";        break;
	case algorithms::FEARNHEAD     : os << "Fearnhead";      break;
	case algorithms::MARGARITA     : os << "Margarita";      break;
	case algorithms::MOONSHINE     : os << "MOONSHINE";      break;
    case algorithms::UNSPECIFIED   : os << "Unspecified";    break;
	default                        : os.setstate(std::ios_base::failbit);
	throw invalid_argument( string(__FILE__) +':'+ to_string(__LINE__) + ": Algorithm unimplemented.\n");
	}
	return os;
}



ostream & operator<< ( ostream & out , const Parameters & params){

	try{
		out << "FastARG parameters are : " << endl;
		out << " -The data file is                     : "  << params.dataFile              << endl;
		out << " -The specified number of sequences is : "  << params.numberOfSequences     << endl;
		out << " -The specified size of sequences is   : "  << params.sequencesSize         << endl;
		out << " -The algorithm is                     : "  << params.algorithmChoice       << endl;
		out << " -The coalescence mode is              : "  << params.coalescenceModeName   << endl;
		out << " -The recombination mode is            : "  << params.recombinationModeName << endl;
		out << " -The random seed flag is              : "  << std::boolalpha << params.randomSeed  << std::noboolalpha << endl;
		out << " -The seed is                          : "  << params.seed                  << endl;
		out << " -The Rplot settings                   : "  << std::boolalpha << params.Rplot  << std::noboolalpha << endl;
		/*
		out << " -The coordinate vector is             : [";
		for (size_t i  = 0 ; i < params.locusCoordinates.size()-1;++i){
			out << params.locusCoordinates.at(i) << ',';
		}
		out << params.locusCoordinates.back() << "]\n";
		out << " -The phenotypes vector is             : [";
		for (size_t i  = 0 ; i < params.phenotypes.size()-1;++i){
			out << params.phenotypes.at(i) << ',';
		}
		out << params.phenotypes.back() << "]\n";
		 */
	}catch(out_of_range & error){
		cerr << "Invalid Phylox parameters!";
		cerr<< error.what() << endl;
		exit(EXIT_FAILURE);

	}catch(...){
		cerr << "An error occurred in displaying parameters on the console!";
		exit(EXIT_FAILURE);
	}

	//cerr << Parameters::algorithmChoiceNames.at(params.algorithmChoice-1) << endl;

	if ( params.algorithmChoice == algorithms::MARGARITA ){
		out << "Margarita specialized parameters are : " << endl;
		out << " - Recombination specific proportion : " << params.MaximumRecombinationRate << endl;
	}
	out << std::endl;
	return out;
}

// TODO : Need to be better implemented;
bool Parameters::verifyIfParametersAreValid(const Parameters & params) {


	if (params.numberOfSequences < 2 ){
		cerr << "Number of sequences needs to be at least 2 !";
		return false;
	}

	if (params.sequencesSize < 2 ){
		cerr << "The length of sequences needs to be at least 2 !";
		return false;
	}

	return true;
}

// Destructor
Parameters::~Parameters() {}

