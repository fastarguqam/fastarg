/*
 * SnipSequenceTESTS.cpp
 *
 *  Created on: 2016-05-17
 *      Author: �ric Marcotte
 */

#define BOOST_TEST_MAIN MyTest

#include <boost/test/unit_test.hpp>
#include <boost/test/execution_monitor.hpp>  // for execution_exception
#include <boost/math/distributions/chi_squared.hpp>
#include <iostream>
#include <bitset>
#include "ARG4WG.h"
#include "Mojito.h"
#include "MargaritaARG.h"
#include "Manhattan.h"
#include "SnipSequence.h"
#include "Enums.h"
#include "Parameters.h"
#include "ARGnode.h"
#include "ARG.h"
using std::string;
using std::cout;
using std::endl;

// This site specifies all the output options for the package :
// http://www.boost.org/doc/libs/1_34_1/libs/test/doc/components/utf/parameters/index.html
// Basic example of the boost test package:
// http://www.boost.org/doc/libs/1_42_0/libs/test/doc/html/tutorials/hello-the-testing-world.html
// http://www.boost.org/doc/libs/1_34_1/libs/test/doc/components/utf/parameters/log_level.html
// --build_info=yes --log_level=error --log_format=HRF --random=0 --report_level=detailed


BOOST_AUTO_TEST_SUITE (oneParameterConstrustor)

BOOST_AUTO_TEST_CASE ( sequenceOfSizeOne )
{
	SnipSequence aSnipSequence = SnipSequence("0");
	BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 1);
	BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 1);
	BOOST_CHECK(SnipSequence::toString(aSnipSequence) == "0");

	aSnipSequence = SnipSequence("1");
	BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 1);
	BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 1);
	BOOST_CHECK(SnipSequence::toString(aSnipSequence) == "1");
}

BOOST_AUTO_TEST_CASE ( sequenceOfSizeFour )
{
	SnipSequence aSnipSequence = SnipSequence("0101");
	BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 4);
	BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 1);
	BOOST_CHECK(SnipSequence::toString(aSnipSequence) == "0101");
}

BOOST_AUTO_TEST_CASE ( sequenceOfSizeMaximumMinusOne )
{
	if ((sizeof(DATA_TYPE)*8) == 32 ){
		SnipSequence aSnipSequence = SnipSequence("0101010101010101010101010101010");
		BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 31);
		BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 1);
		BOOST_CHECK(SnipSequence::toString(aSnipSequence) == "0101010101010101010101010101010");
		BOOST_CHECK(aSnipSequence.snipsData[0] == 715827882 );

	}else if ((sizeof(DATA_TYPE)*8) == 64 ){
		SnipSequence aSnipSequence = SnipSequence("010101010101010101010101010101010101010101010101010101010101010");
		BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 63);
		BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 1);
		BOOST_CHECK(SnipSequence::toString(aSnipSequence) == "010101010101010101010101010101010101010101010101010101010101010");
		BOOST_CHECK(aSnipSequence.snipsData[0] == 6148914691236517204 );
	}else{
		BOOST_WARN_MESSAGE(false, "Untested type.");
	}
}

BOOST_AUTO_TEST_CASE ( sequenceOfMaximumSize )
{
	if ((sizeof(DATA_TYPE)*8) == 32 ){
		SnipSequence aSnipSequence = SnipSequence("10000000000000000000000000000000");
		BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 32);
		BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 1);
		BOOST_CHECK(SnipSequence::toString(aSnipSequence) == "10000000000000000000000000000000");
		BOOST_CHECK(aSnipSequence.AncestralDataMask[0] ==  0 );
		BOOST_CHECK(aSnipSequence.snipsData[0] ==  2147483648 );
	}else if ((sizeof(DATA_TYPE)*8) == 64 ){
		SnipSequence aSnipSequence = SnipSequence("1000000000000000000000000000000000000000000000000000000000000000");
		BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 64);
		BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 1);
		BOOST_CHECK(SnipSequence::toString(aSnipSequence) == "1000000000000000000000000000000000000000000000000000000000000000");
		BOOST_CHECK(aSnipSequence.AncestralDataMask[0] ==  0 );
		BOOST_CHECK(aSnipSequence.snipsData[0] ==  0x8000000000000000);

	}else{
		BOOST_WARN_MESSAGE(false, "Untested type.");
	}

}

BOOST_AUTO_TEST_CASE ( sequenceOfMaximumSizePlusOne )
{
	if ((sizeof(DATA_TYPE)*8) == 32 ){
		SnipSequence aSnipSequence = SnipSequence("000000000000000000000000000000011");
		BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 33);
		BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 2);
		BOOST_CHECK(SnipSequence::toString(aSnipSequence) == "000000000000000000000000000000011");
		BOOST_CHECK(aSnipSequence.AncestralDataMask.size() ==  2 );
		BOOST_CHECK(aSnipSequence.AncestralDataMask[0] ==  0 );
		BOOST_CHECK(aSnipSequence.AncestralDataMask[1] ==  0 );
		BOOST_CHECK(aSnipSequence.snipsData.size() ==  2 );
		BOOST_CHECK(aSnipSequence.snipsData[0] ==  1 );
		BOOST_CHECK(aSnipSequence.snipsData[1] ==  2147483648 );
	}else if ((sizeof(DATA_TYPE)*8) == 64 ){
		SnipSequence aSnipSequence = SnipSequence("00000000000000000000000000000000000000000000000000000000000000011");
		BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 65);
		BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 2);
		BOOST_CHECK(SnipSequence::toString(aSnipSequence) == "00000000000000000000000000000000000000000000000000000000000000011");
		BOOST_CHECK(aSnipSequence.AncestralDataMask.size() ==  2 );
		BOOST_CHECK(aSnipSequence.AncestralDataMask[0] ==  0 );
		BOOST_CHECK(aSnipSequence.AncestralDataMask[1] ==  0 );
		BOOST_CHECK(aSnipSequence.snipsData.size() ==  2 );
		BOOST_CHECK(aSnipSequence.snipsData[0] ==  1 );
		BOOST_CHECK(aSnipSequence.snipsData[1] ==  0x8000000000000000 );
	}else{
		BOOST_WARN_MESSAGE(false, "Untested type.");
	}
}

BOOST_AUTO_TEST_CASE ( sequenceOfSize257 )
{

	if ((sizeof(DATA_TYPE)*8) == 32 ){
		string snipSequenceTestData =
				"1000000000000000000000000000000000000000000000000000000000000001"
				"1000100000000000000000000000000000000000000000000000000000000001"
				"1000000000000000000000000000001100000000000000000000000000000001"
				"10000000000000000000000000000000000000000000000011100000000000010";
		SnipSequence aSnipSequence = SnipSequence(snipSequenceTestData);
		BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 257);
		BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 9);
		BOOST_CHECK(SnipSequence::toString(aSnipSequence) == snipSequenceTestData);
		BOOST_CHECK(aSnipSequence.AncestralDataMask.size() ==  9 );
		BOOST_CHECK(aSnipSequence.AncestralDataMask[0] ==  0 );
		BOOST_CHECK(aSnipSequence.AncestralDataMask[8] ==  0 );
		BOOST_CHECK(aSnipSequence.snipsData.size() ==  9 );
		BOOST_CHECK(aSnipSequence.snipsData[0] ==  2147483648 );
		BOOST_CHECK(aSnipSequence.snipsData[1] ==  1 );
		BOOST_CHECK(aSnipSequence.snipsData[8] == 0  );

	}else if ((sizeof(DATA_TYPE)*8) == 64 ){
		string snipSequenceTestData =
				"0000000000000000000000000000000000000000000000000000000000000011"
				"0000100000000000000000000000000000000000000000000000000000000001"
				"1000000000000000000000000000001100000000000000000000000000000001"
				"10000000000000000000000000000000000000000000000011100000000000010";
		SnipSequence aSnipSequence = SnipSequence(snipSequenceTestData);
		BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 257);
		BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 5);
		BOOST_CHECK(SnipSequence::toString(aSnipSequence) == snipSequenceTestData);
		BOOST_CHECK(aSnipSequence.AncestralDataMask.size() ==  5 );
		BOOST_CHECK(aSnipSequence.AncestralDataMask[0] ==  0 );
		BOOST_CHECK(aSnipSequence.AncestralDataMask[4] ==  0 );
		BOOST_CHECK(aSnipSequence.snipsData.size() ==  5 );
		BOOST_CHECK(aSnipSequence.snipsData[0] ==  3 );
		BOOST_CHECK(aSnipSequence.snipsData[1] ==  576460752303423489 );
		BOOST_CHECK(aSnipSequence.snipsData[4] == 0  );
	}else{
		BOOST_WARN_MESSAGE(false, "Untested type.");
	}


}
BOOST_AUTO_TEST_SUITE_END( ) // End oneParameterConstrustor


BOOST_AUTO_TEST_SUITE (twoParameterConstrustor)

BOOST_AUTO_TEST_CASE (sequenceOfSizeOne)
{
	string ancestralMaskTestData = "0";
	string snipSequenceTestData = "0";
	SnipSequence aSnipSequence = SnipSequence(snipSequenceTestData,ancestralMaskTestData);
	BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 1);
	BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 1);
	BOOST_CHECK (SnipSequence::toString(aSnipSequence) == "0");

	ancestralMaskTestData = "0";
	snipSequenceTestData = "1";
	aSnipSequence = SnipSequence(snipSequenceTestData,ancestralMaskTestData);
	BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 1);
	BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 1);
	BOOST_CHECK (SnipSequence::toString(aSnipSequence) == "1");

	ancestralMaskTestData = "1";
	snipSequenceTestData = "0";
	aSnipSequence = SnipSequence(snipSequenceTestData,ancestralMaskTestData);
	BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 1);
	BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 1);
	BOOST_CHECK (SnipSequence::toString(aSnipSequence) == "?");

	ancestralMaskTestData = "1";
	snipSequenceTestData = "1";
	aSnipSequence = SnipSequence(snipSequenceTestData,ancestralMaskTestData);
	BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 1);
	BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 1);
	BOOST_CHECK (SnipSequence::toString(aSnipSequence) == "?");

}

BOOST_AUTO_TEST_CASE (sequenceOfSizeFour)
{
	string ancestralMaskTestData = "0011";
	string snipSequenceTestData = "0101";
	SnipSequence aSnipSequence = SnipSequence(snipSequenceTestData,ancestralMaskTestData);
	BOOST_CHECK(aSnipSequence.getSizeOfSequence() == 4);
	BOOST_CHECK(aSnipSequence.getnumberOfBlock() == 1);
	BOOST_CHECK (SnipSequence::toString(aSnipSequence) == "01??");

}

BOOST_AUTO_TEST_SUITE_END( ) // End twoParameterConstrustor

BOOST_AUTO_TEST_SUITE (TestOfconvertStringToFastSequence)
// This function is already heavily checked trough the constructors.
BOOST_AUTO_TEST_CASE (wrongCaracterTest)
{
	string snipSequenceTestData = "01019";
	BOOST_CHECK_THROW( SnipSequence::convertStringToFastSequence(snipSequenceTestData) , std::runtime_error);
}
BOOST_AUTO_TEST_CASE (valArraySizeTest)
{
	string snipSequenceTestData = "01011";
	std::valarray<DATA_TYPE> test = SnipSequence::convertStringToFastSequence(snipSequenceTestData);
	BOOST_CHECK (test.size() == 1);
	snipSequenceTestData = "000000000000000000000000000000011";
	test = SnipSequence::convertStringToFastSequence(snipSequenceTestData);

	if ((sizeof(DATA_TYPE)*8) == 32 ){
		BOOST_CHECK (test.size() == 2);
	}else if ((sizeof(DATA_TYPE)*8) == 64 ){
		BOOST_CHECK (test.size() == 1);
	}else{
		BOOST_WARN_MESSAGE(false, "Untested type.");
	}
}
BOOST_AUTO_TEST_SUITE_END( )


BOOST_AUTO_TEST_SUITE (isPositionMutant)
BOOST_AUTO_TEST_CASE (smallSequencestest)
{

	SnipSequence aSnipSequence = SnipSequence("1");
	BOOST_CHECK(SnipSequence::isPositionMutant(0,aSnipSequence)==true);
	aSnipSequence = SnipSequence("01");
	BOOST_CHECK(SnipSequence::isPositionMutant(0,aSnipSequence)==false);
	BOOST_CHECK(SnipSequence::isPositionMutant(1,aSnipSequence)==true);
	aSnipSequence = SnipSequence("10");
	BOOST_CHECK(SnipSequence::isPositionMutant(0,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionMutant(1,aSnipSequence)==false);
	aSnipSequence = SnipSequence("0011","0101"); // 0919
	BOOST_CHECK(SnipSequence::isPositionMutant(0,aSnipSequence)==false);
	BOOST_CHECK(SnipSequence::isPositionMutant(1,aSnipSequence)==false);
	BOOST_CHECK(SnipSequence::isPositionMutant(2,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionMutant(3,aSnipSequence)==false);
}

BOOST_AUTO_TEST_CASE (oneFullBlockSequencestestMinusOne)
{
	SnipSequence aSnipSequence = SnipSequence("1100000000000000000000000000001");
	BOOST_CHECK(SnipSequence::isPositionMutant(0,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionMutant(1,aSnipSequence)==true);
	for(size_t i = 2 ; i < 30;++i){
		BOOST_CHECK(SnipSequence::isPositionMutant(i,aSnipSequence)==false);
	}
	BOOST_CHECK(SnipSequence::isPositionMutant(30,aSnipSequence)==true);

}
BOOST_AUTO_TEST_CASE (oneFullBlockSequencestest)
{

	SnipSequence aSnipSequence = SnipSequence("11000000000000000000000000000001");

	BOOST_CHECK(SnipSequence::isPositionMutant(0,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionMutant(1,aSnipSequence)==true);
	for(size_t i = 2 ; i < 31;++i){
		BOOST_CHECK(SnipSequence::isPositionMutant(i,aSnipSequence)==false);

	}
	aSnipSequence = SnipSequence("11000000000000000000000000000001","10000000000000000000000000000000");
	BOOST_CHECK(SnipSequence::isPositionMutant(31,aSnipSequence)==true);
	//std::cerr << aSnipSequence;

}
BOOST_AUTO_TEST_CASE (oneFullPlus1BlockSequencestest){
	SnipSequence aSnipSequence = SnipSequence("110000000000000000000000000000011");
	BOOST_CHECK(SnipSequence::isPositionMutant(0,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionMutant(1,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionMutant(30,aSnipSequence)==false);
	BOOST_CHECK(SnipSequence::isPositionMutant(31,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionMutant(32,aSnipSequence)==true);
	BOOST_CHECK_THROW(SnipSequence::isPositionMutant(33,aSnipSequence), std::logic_error);
	aSnipSequence = SnipSequence("000000000000000000000000000000011");
	BOOST_CHECK(SnipSequence::isPositionMutant(0,aSnipSequence)==false);
	BOOST_CHECK(SnipSequence::isPositionMutant(1,aSnipSequence)==false);
	BOOST_CHECK(SnipSequence::isPositionMutant(30,aSnipSequence)==false);
	BOOST_CHECK(SnipSequence::isPositionMutant(31,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionMutant(32,aSnipSequence)==true);

}

//63

BOOST_AUTO_TEST_CASE(twoFullBlocksSequencesTestMinusOne){
	SnipSequence aSnipSequence = SnipSequence("100000000000000000000000000000111100000000011000000000000000001");
	BOOST_CHECK(SnipSequence::isPositionMutant(0,aSnipSequence)==true);
	for(size_t i = 1 ; i < 30;++i){
		BOOST_CHECK(SnipSequence::isPositionMutant(i,aSnipSequence)==false);
	}
	for(size_t i = 30 ; i < 34;++i){
		BOOST_CHECK(SnipSequence::isPositionMutant(i,aSnipSequence)==true);
	}
	for(size_t i = 34 ; i < 43;++i){
		BOOST_CHECK(SnipSequence::isPositionMutant(i,aSnipSequence)==false);
	}
	BOOST_CHECK(SnipSequence::isPositionMutant(43,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionMutant(44,aSnipSequence)==true);
	for(size_t i = 45 ; i < 62;++i){
		BOOST_CHECK(SnipSequence::isPositionMutant(i,aSnipSequence)==false);
	}
	BOOST_CHECK(SnipSequence::isPositionMutant(62,aSnipSequence)==true);

}
//64
BOOST_AUTO_TEST_CASE(twoFullBlocksSequencesTest){
	SnipSequence aSnipSequence = SnipSequence("1000000000000000000000000000001111000000000110000000000000000010");
	BOOST_CHECK(SnipSequence::isPositionMutant(0,aSnipSequence)==true);
	for(size_t i = 1 ; i < 30;++i){
		BOOST_CHECK(SnipSequence::isPositionMutant(i,aSnipSequence)==false);
	}
	for(size_t i = 30 ; i < 34;++i){
		BOOST_CHECK(SnipSequence::isPositionMutant(i,aSnipSequence)==true);
	}
	for(size_t i = 34 ; i < 43;++i){
		BOOST_CHECK(SnipSequence::isPositionMutant(i,aSnipSequence)==false);
	}
	BOOST_CHECK(SnipSequence::isPositionMutant(43,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionMutant(44,aSnipSequence)==true);
	for(size_t i = 45 ; i < 62;++i){
		BOOST_CHECK(SnipSequence::isPositionMutant(i,aSnipSequence)==false);
	}
	BOOST_CHECK(SnipSequence::isPositionMutant(62,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionMutant(63,aSnipSequence)==false);
}
//65
BOOST_AUTO_TEST_CASE(twoFullBlocksSequencesTestPlusOne){
	SnipSequence aSnipSequence = SnipSequence("10000000000000000000000000000011110000000001100000000000000000101");
	BOOST_CHECK(SnipSequence::isPositionMutant(0,aSnipSequence)==true);
	for(size_t i = 1 ; i < 30;++i){
		BOOST_CHECK(SnipSequence::isPositionMutant(i,aSnipSequence)==false);
	}
	for(size_t i = 30 ; i < 34;++i){
		BOOST_CHECK(SnipSequence::isPositionMutant(i,aSnipSequence)==true);
	}
	for(size_t i = 34 ; i < 43;++i){
		BOOST_CHECK(SnipSequence::isPositionMutant(i,aSnipSequence)==false);
	}
	BOOST_CHECK(SnipSequence::isPositionMutant(43,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionMutant(44,aSnipSequence)==true);
	for(size_t i = 45 ; i < 62;++i){
		BOOST_CHECK(SnipSequence::isPositionMutant(i,aSnipSequence)==false);
	}
	BOOST_CHECK(SnipSequence::isPositionMutant(62,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionMutant(63,aSnipSequence)==false);
	BOOST_CHECK(SnipSequence::isPositionMutant(64,aSnipSequence)==true);
}


BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE (isPositionAncestral)
BOOST_AUTO_TEST_CASE (smallSequencestest)
{

	SnipSequence aSnipSequence = SnipSequence("0","1"); // 9
	BOOST_CHECK(SnipSequence::isPositionAncestral(0,aSnipSequence)==false);
	aSnipSequence = SnipSequence("00","01"); // 09
	BOOST_CHECK(SnipSequence::isPositionAncestral(0,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionAncestral(1,aSnipSequence)==false);

	aSnipSequence = SnipSequence("00","10"); // 90
	BOOST_CHECK(SnipSequence::isPositionAncestral(0,aSnipSequence)==false);
	BOOST_CHECK(SnipSequence::isPositionAncestral(1,aSnipSequence)==true);

	aSnipSequence = SnipSequence("0011","0101"); // 0919
	BOOST_CHECK(SnipSequence::isPositionAncestral(0,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionAncestral(1,aSnipSequence)==false);
	BOOST_CHECK(SnipSequence::isPositionAncestral(2,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionAncestral(3,aSnipSequence)==false);


	aSnipSequence = SnipSequence("000000000000000000000000000000000011","000000000000000000000000000000000101"); // 000000000000000000000000000000000919
	BOOST_CHECK(SnipSequence::isPositionAncestral(32,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionAncestral(33,aSnipSequence)==false);
	BOOST_CHECK(SnipSequence::isPositionAncestral(34,aSnipSequence)==true);
	BOOST_CHECK(SnipSequence::isPositionAncestral(35,aSnipSequence)==false);
	BOOST_CHECK_THROW(SnipSequence::isPositionMutant(36,aSnipSequence), std::logic_error);
}


BOOST_AUTO_TEST_SUITE_END( )

BOOST_AUTO_TEST_SUITE (mutateAPosition)
SnipSequence aSnipSequence = SnipSequence("0");


BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE (margaritaCommonTractFinder)
BOOST_AUTO_TEST_CASE (smallSequencestest)
{

	SnipSequence aFirstSnipSequence = SnipSequence("1");
	SnipSequence aSecondSnipSequence = SnipSequence("0");
	std::pair<size_t,size_t> alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	//cout << "alphaBeta.first = " << alphaBeta.first << " alphaBeta.second = " << alphaBeta.second << endl;
	BOOST_CHECK(alphaBeta.first == 0);
	BOOST_CHECK(alphaBeta.second == 0);


	aFirstSnipSequence = SnipSequence("1");
	aSecondSnipSequence = SnipSequence("1");
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	//cout << "alphaBeta.first = " << alphaBeta.first << " alphaBeta.second = " << alphaBeta.second << endl;
	BOOST_CHECK(alphaBeta.first == 0);
	BOOST_CHECK(alphaBeta.second == 1);
}
BOOST_AUTO_TEST_CASE (size4SequencesSizeTest)
{
	SnipSequence aFirstSnipSequence = SnipSequence("1000");
	SnipSequence aSecondSnipSequence = SnipSequence("1000");
	std::pair<size_t,size_t> alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	//cout << "alphaBeta.first = " << alphaBeta.first << " alphaBeta.second = " << alphaBeta.second << endl;
	BOOST_CHECK(alphaBeta.first == 0);
	BOOST_CHECK(alphaBeta.second == 4);

	aFirstSnipSequence = SnipSequence("1000");
	aSecondSnipSequence = SnipSequence("1001");
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);

	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 3);
	// cout << "alphaBeta.first = " << alphaBeta.first << " alphaBeta.second = " << alphaBeta.second << endl;
}
BOOST_AUTO_TEST_CASE (size33SequencesSizeTest)
{
	SnipSequence aFirstSnipSequence = SnipSequence("010101010101010101010101010101011");
	SnipSequence aSecondSnipSequence = SnipSequence("101010101010101010101010101010101");
	std::pair<size_t,size_t> alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	//cout << "alphaBeta.first = " << alphaBeta.first << " alphaBeta.second = " << alphaBeta.second << endl;
	BOOST_CHECK(alphaBeta.first == 32 && alphaBeta.second == 33);

}

BOOST_AUTO_TEST_CASE (oneLocusSequences){

	// Test #1
	SnipSequence aFirstSnipSequence = SnipSequence( "0","0"); // 0
	SnipSequence aSecondSnipSequence = SnipSequence("0","0"); // 0
	std::pair<size_t,size_t> alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 1);

	// Test #2
	aFirstSnipSequence = SnipSequence( "0","0"); // 0
	aSecondSnipSequence = SnipSequence("0","1"); // 9
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0 );

	// Test #3
	aFirstSnipSequence = SnipSequence( "0","0"); // 0
	aSecondSnipSequence = SnipSequence("1","0"); // 1
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #4
	aFirstSnipSequence = SnipSequence( "0","0"); // 0
	aSecondSnipSequence = SnipSequence("1","1"); // 9
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #5
	aFirstSnipSequence = SnipSequence( "0","1"); // 9
	aSecondSnipSequence = SnipSequence("0","0"); // 0
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #6
	aFirstSnipSequence = SnipSequence( "0","1"); // 9
	aSecondSnipSequence = SnipSequence("0","1"); // 9
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #7
	aFirstSnipSequence = SnipSequence( "0","1"); // 9
	aSecondSnipSequence = SnipSequence("1","0"); // 1
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #8
	aFirstSnipSequence = SnipSequence( "0","1"); // 9
	aSecondSnipSequence = SnipSequence("1","1"); // 9
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #9
	aFirstSnipSequence = SnipSequence( "1","0"); // 1
	aSecondSnipSequence = SnipSequence("0","0"); // 0
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #10
	aFirstSnipSequence = SnipSequence( "1","0"); // 1
	aSecondSnipSequence = SnipSequence("0","1"); // 9
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #11
	aFirstSnipSequence = SnipSequence( "1","0"); // 1
	aSecondSnipSequence = SnipSequence("1","0"); // 1
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 1);

	// Test #12
	aFirstSnipSequence = SnipSequence( "1","0"); // 1
	aSecondSnipSequence = SnipSequence("1","1"); // 9
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #13
	aFirstSnipSequence = SnipSequence( "1","1"); // 9
	aSecondSnipSequence = SnipSequence("0","0"); // 0
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #14
	aFirstSnipSequence = SnipSequence( "1","1"); // 9
	aSecondSnipSequence = SnipSequence("0","1"); // 9
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #15
	aFirstSnipSequence = SnipSequence( "1","1"); // 9
	aSecondSnipSequence = SnipSequence("1","0"); // 1
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #16
	aFirstSnipSequence = SnipSequence( "1","1"); // 9
	aSecondSnipSequence = SnipSequence("1","1"); // 9
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);
}

BOOST_AUTO_TEST_CASE (smallSequencesWithNonAncestralMaterial){
	SnipSequence aFirstSnipSequence = SnipSequence( "000111000","000000111"); // 000 111 999
	SnipSequence aSecondSnipSequence = SnipSequence("010010010","001001001"); // 019 019 019
	std::pair<size_t,size_t> alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 4 && alphaBeta.second == 9);


	aFirstSnipSequence = SnipSequence( "1111111100000000","1111000011110000"); // 9999111199990000
	aSecondSnipSequence = SnipSequence("1100110011001100","1010101010101010"); // 9190919091909190
	//	                                                                       // 1111111011111011
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 7);

}

BOOST_AUTO_TEST_CASE (longSequencesWithNonAncestralMaterial){
	SnipSequence aFirstSnipSequence = SnipSequence( "0000000000000000000000000000000000000000000000000000000000000000000111000","0000000000000000000000000000000000000000000000000000000000000000000000111"); // 64*0 000 111 999
	SnipSequence aSecondSnipSequence = SnipSequence("1111111111111111111111111111111111111111111111111111111111111111010010010","0000000000000000000000000000000000000000000000000000000000000000001001001"); // 64*1 019 019 019
	std::pair<size_t,size_t> alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);                                                                                    //      101 011 111
	BOOST_CHECK(alphaBeta.first == 68 && alphaBeta.second == 73);


	aFirstSnipSequence = SnipSequence( "11111111111111111111111111111111111111111111111111111111111111111111111100000000","00000000000000000000000000000000000000000000000000000000000000001111000011110000"); // 64*1 9999111199990000
	aSecondSnipSequence = SnipSequence("11111111111111111111111111111111111111111111111111111111111111111100110011001100","00000000000000000000000000000000000000000000000000000000000000001010101010101010"); // 64*1 9190919091909190
	//	                                                                                                                                                                                                      //       1111111001011011
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 71);

	aFirstSnipSequence = SnipSequence( "00000000000000000000000000000000000000000000000000000000000000001111111100000000","00000000000000000000000000000000000000000000000000000000000000001111000011110000"); // 64*0 9999111199990000
	aSecondSnipSequence = SnipSequence("11111111111111111111111111111111111111111111111111111111111111111100110011001100","00000000000000000000000000000000000000000000000000000000000000001010101010101010"); // 64*1 9190919091909190
	//	                                                                                                                                                                                                            // 1111111001011011
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 64 && alphaBeta.second == 71);
	//cout << "alphaBeta.first = " << alphaBeta.first << " alphaBeta.second = " << alphaBeta.second << endl;
}

BOOST_AUTO_TEST_CASE (checkForAtLeastOneCommonAncestralLocus){
	SnipSequence aFirstSnipSequence = SnipSequence( "10000","00000"); // 10000
	SnipSequence aSecondSnipSequence = SnipSequence("00100","00011"); // 00199
	std::pair<size_t,size_t> alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);                                                                                    //      101 011 110
	BOOST_CHECK(alphaBeta.first == 1 && alphaBeta.second == 2);

	aFirstSnipSequence = SnipSequence( "1111111100000000","1111000011110000"); // 9999111199990000
	aSecondSnipSequence = SnipSequence("1111111100000000","0000111100001111"); // 1111999900009999
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);
	//cout << "alphaBeta.first = " << alphaBeta.first << " alphaBeta.second = " << alphaBeta.second << endl;

	aFirstSnipSequence = SnipSequence( "10100","00010"); // 10190
	aSecondSnipSequence = SnipSequence("00101","00010"); // 00191
	alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 1 && alphaBeta.second == 4);
}

BOOST_AUTO_TEST_CASE(sequenceWithNonAncestralMaterial){
	SnipSequence aFirstSnipSequence = SnipSequence( "000111000","111111111"); // 999 999 999
	SnipSequence aSecondSnipSequence = SnipSequence("010010010","001001001"); // 019 019 019
	std::pair<size_t,size_t> alphaBeta = MargaritaARG::margaritaCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);
}
BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE (mojitoCommonTractFinder)
BOOST_AUTO_TEST_CASE (oneLocusSequences){

	// Test #1
	SnipSequence aFirstSnipSequence = SnipSequence( "0","0"); // 0
	SnipSequence aSecondSnipSequence = SnipSequence("0","0"); // 0
	std::pair<size_t,size_t> alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 1);

	// Test #2
	aFirstSnipSequence = SnipSequence( "0","0"); // 0
	aSecondSnipSequence = SnipSequence("0","1"); // 9
	alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0 );

	// Test #3
	aFirstSnipSequence = SnipSequence( "0","0"); // 0
	aSecondSnipSequence = SnipSequence("1","0"); // 1
	alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #4
	aFirstSnipSequence = SnipSequence( "0","0"); // 0
	aSecondSnipSequence = SnipSequence("1","1"); // 9
	alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #5
	aFirstSnipSequence = SnipSequence( "0","1"); // 9
	aSecondSnipSequence = SnipSequence("0","0"); // 0
	alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #6
	aFirstSnipSequence = SnipSequence( "0","1"); // 9
	aSecondSnipSequence = SnipSequence("0","1"); // 9
	alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #7
	aFirstSnipSequence = SnipSequence( "0","1"); // 9
	aSecondSnipSequence = SnipSequence("1","0"); // 1
	alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #8
	aFirstSnipSequence = SnipSequence( "0","1"); // 9
	aSecondSnipSequence = SnipSequence("1","1"); // 9
	alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #9
	aFirstSnipSequence = SnipSequence( "1","0"); // 1
	aSecondSnipSequence = SnipSequence("0","0"); // 0
	alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #10
	aFirstSnipSequence = SnipSequence( "1","0"); // 1
	aSecondSnipSequence = SnipSequence("0","1"); // 9
	alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #11
	aFirstSnipSequence = SnipSequence( "1","0"); // 1
	aSecondSnipSequence = SnipSequence("1","0"); // 1
	alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 1);

	// Test #12
	aFirstSnipSequence = SnipSequence( "1","0"); // 1
	aSecondSnipSequence = SnipSequence("1","1"); // 9
	alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #13
	aFirstSnipSequence = SnipSequence( "1","1"); // 9
	aSecondSnipSequence = SnipSequence("0","0"); // 0
	alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #14
	aFirstSnipSequence = SnipSequence( "1","1"); // 9
	aSecondSnipSequence = SnipSequence("0","1"); // 9
	alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #15
	aFirstSnipSequence = SnipSequence( "1","1"); // 9
	aSecondSnipSequence = SnipSequence("1","0"); // 1
	alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	// Test #16
	aFirstSnipSequence = SnipSequence( "1","1"); // 9
	aSecondSnipSequence = SnipSequence("1","1"); // 9
	alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);
}

BOOST_AUTO_TEST_CASE (twoLocusSequences)
{
	SnipSequence aFirstSnipSequence = SnipSequence( "00","00"); // 00
	SnipSequence aSecondSnipSequence = SnipSequence("00","00"); // 00
	std::pair<size_t,size_t> alphaBeta = Mojito::mojitoLongestCommonTractFinder(aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 2);

	aFirstSnipSequence = SnipSequence( "00","00"); // 00
	aSecondSnipSequence = SnipSequence("10","00"); // 10
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 1 && alphaBeta.second == 2 );
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence,aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 1 && alphaBeta.second == 2 );

	aFirstSnipSequence = SnipSequence( "00","00"); // 00
	aSecondSnipSequence = SnipSequence("01","00"); // 01
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 1 );
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 1 );

	aFirstSnipSequence = SnipSequence( "00","00"); // 00
	aSecondSnipSequence = SnipSequence("11","00"); // 11
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0 );
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0 );

	aFirstSnipSequence = SnipSequence( "11","11"); // 99
	aSecondSnipSequence = SnipSequence("11","00"); // 11
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0 );
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0 );

	aFirstSnipSequence = SnipSequence( "00","11"); // 99
	aSecondSnipSequence = SnipSequence("11","00"); // 11
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0 );
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0 );

}

BOOST_AUTO_TEST_CASE (treeLocusSequences)
{
	SnipSequence aFirstSnipSequence = SnipSequence( "010","000"); // 010
	SnipSequence aSecondSnipSequence = SnipSequence("010","001"); // 019
	std::pair<size_t,size_t> alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 3);
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 3);

	aFirstSnipSequence = SnipSequence( "010","101"); // 919
	aSecondSnipSequence = SnipSequence("010","010"); // 090
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 0);

	aFirstSnipSequence = SnipSequence( "110","000"); // 110
	aSecondSnipSequence = SnipSequence("011","000"); // 011
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 1 && alphaBeta.second == 2);
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 1 && alphaBeta.second == 2);

	aFirstSnipSequence = SnipSequence( "011","000"); // 011
	aSecondSnipSequence = SnipSequence("001","000"); // 001
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 2 && alphaBeta.second == 3);
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 2 && alphaBeta.second == 3);

	aFirstSnipSequence = SnipSequence( "110","000"); // 110
	aSecondSnipSequence = SnipSequence("100","000"); // 100
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 1);
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 1);
}

BOOST_AUTO_TEST_CASE (longSequences)
{
	SnipSequence aFirstSnipSequence = SnipSequence( "000100100000000","110000000110010"); // 990100100990090
	SnipSequence aSecondSnipSequence = SnipSequence("110100000110000","001011000000000"); // 119199000110000
	std::pair<size_t,size_t> alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 6);
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 6);

	aFirstSnipSequence = SnipSequence( "00010010000000011","11000000011001000"); // 99010010099009011
	aSecondSnipSequence = SnipSequence("11010000011000011","00101100000000000"); // 11919900011000011
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 7 && alphaBeta.second == 17);
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 7 && alphaBeta.second == 17);

	aFirstSnipSequence = SnipSequence( "00010000100000011","11000000011001000"); // 99010000199009011
	aSecondSnipSequence = SnipSequence("11010000011000011","00101100000000000"); // 11919900011000011
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 9 && alphaBeta.second == 17);
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 9 && alphaBeta.second == 17);

	aFirstSnipSequence = SnipSequence( "00010100100000011","11000000011001000"); // 99010100199009011
	aSecondSnipSequence = SnipSequence("11010100011000011","00101000000000000"); // 11919100011000011
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 8);
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 8);

	aFirstSnipSequence = SnipSequence( "000010100100000011","011000000011001000"); // 099010100199009011
	aSecondSnipSequence = SnipSequence("011010100011000011","000101000000000000"); // 011919100011000011
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 9);
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 9);
}

BOOST_AUTO_TEST_CASE (atLeastTwoBlockSequences)
{
	SnipSequence aFirstSnipSequence = SnipSequence(
			"0000000000000000000000000000000000000000000000000000000000000001000010100100000011",
			"0000000000000000000000000000000000000000000000000000000000000000011000000011001000"); // 63*0 1 099010100199009011
	SnipSequence aSecondSnipSequence = SnipSequence(
			"0000000000000000000000000000000000000000000000000000000000000000011010100011000011",
			"0000000000000000000000000000000000000000000000000000000000000000000101000000000000"); // 64*0 011919100011000011
	std::pair<size_t,size_t> alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 64 && alphaBeta.second == 73);
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 64 && alphaBeta.second == 73);

	aFirstSnipSequence = SnipSequence(
			"0000000000000000000000000000000000000000000000000000000000000000000010100100000011",
			"0000000000000000000000000000000000000000000000000000000000000000011000000011001000"); // 64*0 099010100199009011
	aSecondSnipSequence = SnipSequence(
			"0000000000000000000000000000000000000000000000000000000000000000011010100011000011",
			"0000000000000000000000000000000000000000000000000000000000000000000101000000000000"); // 64*0 011919100011000011
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 73);
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 73);

	aFirstSnipSequence = SnipSequence(
			"000000000000000000000000000000000000000000000000000000000000000111",
			"000000000000000000000000000000000000000000000000000000000000000000");
	aSecondSnipSequence = SnipSequence(
			"000000000000000000000000000000000000000000000000000000000000001110",
			"000000000000000000000000000000000000000000000000000000000000000000");
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 63 && alphaBeta.second == 65);
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 63 && alphaBeta.second == 65);

	aFirstSnipSequence = SnipSequence(
			"000011000000000000000000000000000001000000000000000000000000000111",
			"000000000000000000000000000000000000000000000000000000000000000000");
	aSecondSnipSequence = SnipSequence(
			"000011000000000000000000000000000001000000000000000000000000001110",
			"000000000000000000000000000000000000000000000000000000000000000000");
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 62);
	alphaBeta = Mojito::mojitoLongestCommonTractFinder( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(alphaBeta.first == 0 && alphaBeta.second == 62);
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_AUTO_TEST_SUITE (equalOperator)
BOOST_AUTO_TEST_CASE (smallSequences)
{
	SnipSequence::coalescenceModeFlag = coalescenceMode::NO_CONSTRAINT;
	SnipSequence aFirstSnipSequence = SnipSequence("0");
	SnipSequence aSecondSnipSequence = SnipSequence("0");
	BOOST_CHECK(aFirstSnipSequence == aSecondSnipSequence);  // 0 0
	aSecondSnipSequence = SnipSequence("0","1");
	BOOST_CHECK(aFirstSnipSequence == aSecondSnipSequence);  // 0 9
	BOOST_CHECK( aSecondSnipSequence== aFirstSnipSequence ); // 9 0
	aSecondSnipSequence = SnipSequence("1");
	BOOST_CHECK(aFirstSnipSequence != aSecondSnipSequence);  // 0 1
	BOOST_CHECK( aSecondSnipSequence!= aFirstSnipSequence ); // 1 0
	aSecondSnipSequence = SnipSequence("0","1");
	aSecondSnipSequence = SnipSequence("0","1");
	BOOST_CHECK(aFirstSnipSequence == aSecondSnipSequence);  // 9 9

	SnipSequence::coalescenceModeFlag = coalescenceMode::HAVE_ANCESTRAL_MATERIAL_IN_COMMON;
	aFirstSnipSequence = SnipSequence("0");
	aSecondSnipSequence = SnipSequence("0");
	BOOST_CHECK(aFirstSnipSequence == aSecondSnipSequence);  // 0 0
	aSecondSnipSequence = SnipSequence("0","1");
	BOOST_CHECK(aFirstSnipSequence != aSecondSnipSequence);  // 0 9
	BOOST_CHECK( aSecondSnipSequence!= aFirstSnipSequence ); // 9 0
	aSecondSnipSequence = SnipSequence("1");
	BOOST_CHECK(aFirstSnipSequence != aSecondSnipSequence);  // 0 1
	BOOST_CHECK( aSecondSnipSequence!= aFirstSnipSequence ); // 1 0
	aSecondSnipSequence = SnipSequence("0","1");
	aSecondSnipSequence = SnipSequence("0","1");
	BOOST_CHECK(aFirstSnipSequence != aSecondSnipSequence);  // 9 9 Shouldn't work now !!

}
BOOST_AUTO_TEST_SUITE_END( )





/*********************************************************************************************/
/**  THIS FUNCTION CONSTUCT A ARG WITH A SPECIFIC SET OF DATA FOR TESTING PURPOSES          **/
/**  HERE IS THE DATA YOU NEED TO INPUT TO GET THE MRCA:                                    **/
/*********************************************************************************************/
//void ARG::constructExampleARG() {
//	auto startTime = Clock::now();
//	auto endTime = Clock::now();
//	this->resetARG();
//	random_shuffle(this->positionOfLiveNodes.begin(),this->positionOfLiveNodes.end()); // Should not change anything.

BOOST_AUTO_TEST_SUITE (fullArgConstruct)

BOOST_AUTO_TEST_CASE (RandomRecombinationTest){
	// Test for the completely random recombination with no constraint.
	Parameters params =  Parameters();
	params.algorithmChoice = algorithms::MARGARITA;
	params.recombinationModeChoice = recombinationMode::NO_CONSTRAINT;
	params.coalescenceModeChoice = coalescenceMode::HAVE_ANCESTRAL_MATERIAL_IN_COMMON;
	params.dataFile = "";
	params.numberOfSequences = 2;
	params.sequencesSize = 5;
	vector<size_t> distances {1,1,1,1,1};

	SnipSequence Leaf0("10000");
	ARGnode Node0(ARGNodeType::LEAFSAMPLEMODE, Leaf0, 0, std::numeric_limits<size_t>::max(), {{0},{0},{0},{0},{0}});
	SnipSequence Leaf1("11000");
	ARGnode Node1(ARGNodeType::LEAFSAMPLEMODE, Leaf1, 1, std::numeric_limits<size_t>::max(), {{1},{1},{1},{1},{1}});
	vector<bool> phenotypes {0};
	vector<ARGnode> leaves {Node0, Node1};
	MargaritaARG aArg (params, leaves, phenotypes, distances);

	BOOST_CHECK(aArg.lenghtOfSequences == 5);
	BOOST_CHECK(aArg.numberOfSequences == 2);
	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  0);
	aArg.generateARandomRecombinationEvent();
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  1);
	BOOST_CHECK(aArg.ARGnodes.size() ==  4);
	BOOST_CHECK(aArg.positionOfAliveNodes.size() ==  3);

	bool flagSuccessfullRecombination= true;

	for (size_t i = 0; i < 100 ; ++i){
		flagSuccessfullRecombination = aArg.generateARandomRecombinationEvent();
		BOOST_CHECK(flagSuccessfullRecombination);
		BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  (2+i));
		BOOST_CHECK(aArg.ARGnodes.size() ==  4+2*(i+1));
	}

	size_t numberOfRecombinationByPosition [5] = {0,0,0,0,0};

	for (size_t i = 2 ; i < aArg.ARGnodes.size(); ++i){
		++numberOfRecombinationByPosition[aArg.ARGnodes.at(i).secondDataPoint-1];

	}
	BOOST_CHECK( numberOfRecombinationByPosition[0]/(double)20 > (0.1) );
	BOOST_CHECK( numberOfRecombinationByPosition[1]/(double)20 > (0.1) );
	BOOST_CHECK( numberOfRecombinationByPosition[2]/(double)20 > (0.1) );
	BOOST_CHECK( numberOfRecombinationByPosition[3]/(double)20 > (0.1) );
	BOOST_CHECK( numberOfRecombinationByPosition[4] ==0 );
}

BOOST_AUTO_TEST_CASE (MargaritaRecombinationTest){
	// Test for the completely random recombination with no constraint.
	Parameters params =  Parameters();
	params.algorithmChoice = algorithms::MARGARITA;
	params.recombinationModeChoice = recombinationMode::HAVE_ANCESTRAL_MATERIAL_ONLY;
	params.coalescenceModeChoice = coalescenceMode::HAVE_ANCESTRAL_MATERIAL_IN_COMMON;
	params.dataFile = "";
	params.numberOfSequences = 2;
	params.sequencesSize = 5;
	vector<size_t> distances {1,1,1,1,1};

	SnipSequence Leaf0("10000");
	ARGnode Node0(ARGNodeType::LEAFSAMPLEMODE, Leaf0, 0, std::numeric_limits<size_t>::max(), {{0},{0},{0},{0},{0}});
	SnipSequence Leaf1("11000");
	ARGnode Node1(ARGNodeType::LEAFSAMPLEMODE, Leaf1, 1, std::numeric_limits<size_t>::max(), {{1},{1},{1},{1},{1}});
	vector<bool> phenotypes {0};
	vector<ARGnode> leaves {Node0, Node1};
	MargaritaARG aArg (params, leaves, phenotypes, distances);

	BOOST_CHECK(aArg.lenghtOfSequences == 5);
	BOOST_CHECK(aArg.numberOfSequences == 2);
	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  0);

	size_t limitOfRandomRecobinationNumer = 0;
	// Since we have ten snips in two sequences we are suppose to reach 10 lives nodes maximum
	while (aArg.positionOfAliveNodes.size() < 10 && limitOfRandomRecobinationNumer < 1000){
		aArg.generateARandomRecombinationEvent();
		++limitOfRandomRecobinationNumer;
	}

	// There is a maximum of 8 recombination.
	BOOST_CHECK (aArg.totalNumberOfRecombinationEvent == 8);

	// Then if we try to make some more it should failed each time.
	for( size_t i = 0 ; i < 100 ; ++i){
		BOOST_CHECK (!aArg.generateARandomRecombinationEvent());
	}

	// It is not suppose to take that much try to get it.
	if (limitOfRandomRecobinationNumer > 999){
		BOOST_CHECK(false);
	}
}

BOOST_AUTO_TEST_CASE (smallArg){

	Parameters params =  Parameters();
	params.algorithmChoice = algorithms::MARGARITA;
	params.coalescenceModeChoice = coalescenceMode::HAVE_ANCESTRAL_MATERIAL_IN_COMMON;
	params.dataFile = "";
	params.numberOfSequences = 8;
	params.sequencesSize = 5;
	vector<size_t> distances {1,1,1,1,1};

	SnipSequence Leaf0("10000");
	ARGnode Node0(ARGNodeType::LEAFSAMPLEMODE, Leaf0, 0, std::numeric_limits<size_t>::max(), {{0},{0},{0},{0},{0}});
	SnipSequence Leaf1("11000");
	ARGnode Node1(ARGNodeType::LEAFSAMPLEMODE, Leaf1, 1, std::numeric_limits<size_t>::max(), {{1},{1},{1},{1},{1}});
	SnipSequence Leaf2("11100");
	ARGnode Node2(ARGNodeType::LEAFSAMPLEMODE, Leaf2, 2, std::numeric_limits<size_t>::max(), {{2},{2},{2},{2},{2}});
	SnipSequence Leaf3("10100");
	ARGnode Node3(ARGNodeType::LEAFSAMPLEMODE, Leaf3, 3, std::numeric_limits<size_t>::max(),{{3},{3},{3},{3},{3}});
	SnipSequence Leaf4("10110");
	ARGnode Node4(ARGNodeType::LEAFSAMPLEMODE, Leaf4, 4, std::numeric_limits<size_t>::max(),{{4},{4},{4},{4},{4}});
	SnipSequence Leaf5("00100");
	ARGnode Node5(ARGNodeType::LEAFSAMPLEMODE, Leaf5, 5, std::numeric_limits<size_t>::max(),{{5},{5},{5},{5},{5}});
	SnipSequence Leaf6("00101");
	ARGnode Node6(ARGNodeType::LEAFSAMPLEMODE, Leaf6, 6, std::numeric_limits<size_t>::max(),{{6},{6},{6},{6},{6}});
	SnipSequence Leaf7("00101");
	ARGnode Node7(ARGNodeType::LEAFSAMPLEMODE, Leaf7, 7, std::numeric_limits<size_t>::max(),{{7},{7},{7},{7},{7}});
	vector<bool> phenotypes {0,1,1,0};
	vector<ARGnode> leaves {Node0, Node1, Node2, Node3, Node4, Node5, Node6, Node7};
	MargaritaARG aArg (params, leaves, phenotypes, distances);

	BOOST_CHECK(aArg.lenghtOfSequences == 5);
	BOOST_CHECK(aArg.numberOfSequences == 8);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  0);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  1);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  5);

	BOOST_CHECK(aArg.isAMutationEventPossible);
	vector<size_t> numberOfMutantLocus = {5,2,6,1,2};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	for( uint16_t i = 0 ; i < 8 ; ++i){
		for( uint16_t j = 0 ; j < 5 ; ++j){
			vector<uint16_t> leafReachTemp = {i};
			BOOST_CHECK(aArg.getLeafReachabilityforALocus(j, i) == leafReachTemp );
		}
	}

	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  1);

	vector<size_t> aliveNodes = {0,1,2,3,4,5,6,7};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	std::set<std::pair<size_t,size_t>> setOfPairs = {{6,7}} ;
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(6,7);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  0);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  5);

	BOOST_CHECK(aArg.isAMutationEventPossible);
	numberOfMutantLocus = {5,2,5,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	vector<uint16_t> leafReachTemp = {6,7};
	for( uint16_t j = 0 ; j < 5 ; ++j){
		BOOST_CHECK(aArg.getLeafReachabilityforALocus(j,8) == leafReachTemp );
	}

	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  0);

	aliveNodes = {0,1,2,3,4,5,8};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = {} ;
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generateARecombinationEvent(2,5);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  1);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  5);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {5,2,5,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 9) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 9) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 10) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 10) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 10) == leafReachTemp );
	leafReachTemp = {5};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 10) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 10) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 9) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 9) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 9) == leafReachTemp );

	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  3);

	aliveNodes = {0,1,2,3,4,9,8,10};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = {{2,9}, {3,9}, {8,10}} ;
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generateARecombinationEvent(1,4);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  2);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  5);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {5,2,5,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0,11) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1,12) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2,12) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3,12) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4,12) == leafReachTemp );
	leafReachTemp = {4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0,12) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1,11) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2,11) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3,11) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4,11) == leafReachTemp );

	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  8);

	aliveNodes = {0,1,2,3,11,9,8,10,12};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = {{0,12}, {1,12}, {2,9}, {2,12}, {3,9}, {3,12}, {10,11}, {8,10}} ;
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generateARecombinationEvent(2,2);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  5);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {5,2,5,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 13) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 13) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 14) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 14) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 14) == leafReachTemp );
	leafReachTemp = {2};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 14) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 14) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 13) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 13) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 13) == leafReachTemp );

	//{3,13}, {9,13}, {9,3}, {10,11}, {10,8}, {12,0}, {12,1}, {12,3}, {14,1}, {14,12}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  10);

	aliveNodes = {0,1,13,3,11,9,8,10,12,14};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = {{1,12},{0,12},{1,14},{3,9},{3,12},{3,13},{8,10},{9,13},{10,11},{12,14}};
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(1,14);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  2);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  3);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  5);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {4,1,5,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {1,2};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 15) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 15) == leafReachTemp );

	leafReachTemp = {1};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 15) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 15) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 15) == leafReachTemp );

	// {3,13}, {9,13}, {9,3}, {10,11}, {10,8}, {12,0}, {12,15}, {12,3}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  8 );

	aliveNodes = {0,15,13,3,11,9,8,10,12};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = {{3,13}, {9,13}, {3,9}, {10,11}, {8,10}, {0,12}, {12,15}, {3,12}};
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generetaAMutationEvent(1, 15);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  2);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  4);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {4,0,5,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {1,2};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 16) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 16) == leafReachTemp );

	leafReachTemp = {1};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 16) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 16) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 16) == leafReachTemp );

	// {16,0}, {3,13}, {9,13}, {9,3}, {10,11}, {10,8}, {12,0}, {12,16}, {12,3}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  9 );

	aliveNodes = {0,16,13,3,11,9,8,10,12};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = {{0,16}, {3,13}, {9,13}, {3,9}, {10,11}, {8,10}, {0,12}, {12,16}, {3,12}};
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(3,13);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  3);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  4);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {4,0,4,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {3};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 17) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 17) == leafReachTemp );

	leafReachTemp = {2,3};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 17) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 17) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 17) == leafReachTemp );

	// {16,0}, {9,17}, {10,11}, {10,8}, {12,0}, {12,16}, {12,17}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  7 );

	aliveNodes = {0,16,17,11,9,8,10,12};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = {{0,16}, {10,11}, {8,10}, {0,12}, {12,16}, {12,17},{9,17} };
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(12,17);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  4);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  4);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {3,0,4,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {3,4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 18) == leafReachTemp );

	leafReachTemp = {3};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 18) == leafReachTemp );

	leafReachTemp = {2,3};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 18) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 18) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 18) == leafReachTemp );

	// {16,0}, {9,18},{10,11},{10,8}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  4 );

	aliveNodes = {0,16,18,11,9,8,10};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = {{0,16}, {10,11}, {8,10}, {9,18}};
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(10,11);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  5);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);
	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);

	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  4);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {3,0,4,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {5};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 19) == leafReachTemp );

	leafReachTemp = {4,5};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 19) == leafReachTemp );

	leafReachTemp = {4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 19) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 19) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 19) == leafReachTemp );

	// {16,0}, {9,18}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  2 );

	aliveNodes = {0,16,18,19,9,8};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = {{0,16}, {9,18} };
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generetaAMutationEvent(4,8);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  2);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  5);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  1);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  3);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {3,0,4,1,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 20) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 20) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 20) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 20) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 20) == leafReachTemp );

	// {16,0}, {9,18}, {20,9}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  3 );

	aliveNodes = {0,16,18,19,9,20};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = {{0,16}, {9,18}, {9,20} };
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(9,20);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  2);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  6);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  1);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  3);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {3,0,3,1,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {6,7};
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(0, 21) == leafReachTemp );
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(1, 21) == leafReachTemp );
	leafReachTemp = {5,6,7};
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(2, 21) == leafReachTemp );
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(3, 21) == leafReachTemp );
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(4, 21) == leafReachTemp );

	// {16,0}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  1 );

	aliveNodes = {0,16,18,19,21};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = {{0,16}};
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generateARecombinationEvent(1,18);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  2);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  6);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  1);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  3);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {3,0,3,1,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {};
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(0,22) == leafReachTemp );
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(1, 23) == leafReachTemp );
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(2, 23) == leafReachTemp );
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(3, 23) == leafReachTemp );
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(4, 23) == leafReachTemp );

	leafReachTemp = {3,4};
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(0, 23) == leafReachTemp );

	leafReachTemp = {3};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1,22) == leafReachTemp );

	leafReachTemp = {2,3};
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(2,22) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3,22) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4,22) == leafReachTemp );

	// {16,0}, {21,22}, {23,0}, {23,16}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  4 );

	aliveNodes = {0,16,22,19,21,23};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = {{0,16}, {21,22}, {0,23}, {16,23}};
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	aArg.printCoalescencePairs();

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(23,16);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  2);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  7);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  1);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  3);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {2,0,3,1,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {1,2,3,4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 24) == leafReachTemp );

	leafReachTemp = {1,2};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 24) == leafReachTemp );

	leafReachTemp = {1};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 24) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 24) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 24) == leafReachTemp );

	// {24,0}, {21,22}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  2 );

	aliveNodes = {0,24,22,19,21};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = { {21,22}, {0,24} };
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(0,24);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  2);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  8);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  3);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {1,0,3,1,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {0,1,2,3,4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0,25) == leafReachTemp );

	leafReachTemp = {0,1,2};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1,25) == leafReachTemp );

	leafReachTemp = {0,1};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2,25) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3,25) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4,25) == leafReachTemp );

	//  {21,22}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  1 );

	aliveNodes = {25,22,19,21};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = { {21,22}};
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );


	/*******************************************************************************************/
	aArg.generetaAMutationEvent(0, 25);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  3);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  8);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  1);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  2);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {0,0,3,1,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {0,1,2,3,4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 26) == leafReachTemp );

	leafReachTemp = {0,1,2};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 26) == leafReachTemp );

	leafReachTemp = {0,1};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 26) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 26) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 26) == leafReachTemp );

	//  {21,22}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  1 );

	aliveNodes = {26,22,19,21};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = { {21,22}};
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generetaAMutationEvent(3, 19);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  4);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  8);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  0);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  1);
	BOOST_CHECK(!aArg.isAMutationEventPossible);

	numberOfMutantLocus = {0,0,3,0,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {5};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 27) == leafReachTemp );

	leafReachTemp = {4,5};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 27) == leafReachTemp );

	leafReachTemp = {4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 27) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 27) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 27) == leafReachTemp );

	//  {27,22}, {21,22}, {21,27}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  3);

	aliveNodes = {26,22,27,21};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = { {21,22}, {22,27}, {21,27} };
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(22,27);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  4);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  9);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  0);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  1);
	BOOST_CHECK(!aArg.isAMutationEventPossible);

	numberOfMutantLocus = {0,0,2,0,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {5};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 28) == leafReachTemp );

	leafReachTemp = {3,4,5};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 28) == leafReachTemp );

	leafReachTemp = {2,3,4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 28) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 28) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 28) == leafReachTemp );

	//  {28,21}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  1);

	aliveNodes = {26,28,21};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = { {21,28} };
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(28,21);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  4);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  10);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  1);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  1);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {0,0,1,0,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {5,6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 29) == leafReachTemp );

	leafReachTemp = {3,4,5,6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 29) == leafReachTemp );

	leafReachTemp = {2,3,4,5,6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 29) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 29) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 29) == leafReachTemp );

	//
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  0);

	aliveNodes = {26,29};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = {  };
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );

	/*******************************************************************************************/
	aArg.generetaAMutationEvent(2, 29);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  5);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  10);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  0);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  0);
	BOOST_CHECK(!aArg.isAMutationEventPossible);

	numberOfMutantLocus = {0,0,0,0,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {5,6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 30) == leafReachTemp );

	leafReachTemp = {3,4,5,6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 30) == leafReachTemp );

	leafReachTemp = {2,3,4,5,6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 30) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 30) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 30) == leafReachTemp );

	//	{26,30}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  1);

	aliveNodes = {26,30};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = { {26,30} };
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );
	aArg.printCoalescencePairs();

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(26,30);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  5);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  11);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  0);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  0);
	BOOST_CHECK(!aArg.isAMutationEventPossible);

	numberOfMutantLocus = {0,0,0,0,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {0,1,2,3,4,5,6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0,31) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1,31) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2,31) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3,31) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4,31) == leafReachTemp );

	//
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  0);

	aliveNodes = {31};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	setOfPairs = { };
	BOOST_CHECK( aArg.possibleCoalescence ==  setOfPairs );


	/*
	cout << "\n--- Partial tree in Newick format for all loci.---\n";
	for ( size_t i = 0 ; i < aArg.lenghtOfSequences ; ++i){
		cout  << i << " : " << aArg.getPartialTreeForALocusInNewick(i) << "\n";
	}
	cout << std::endl;
	 */
	/*
	vector<long double> pvalues = aArg.pvaluesForChi2AssociationTest();
	long double minimalPValue = std::numeric_limits<long double>::max();
	vector<uint16_t> bestPositionsOfPValues;


	cout << "\nP-values vector for all loci.\n{";
	for ( size_t i = 0 ; i < pvalues.size()-1  ; ++i){
		cout << pvalues.at(i) << ", ";
	}
	cout << pvalues.back() << "}\n";

	for (size_t i = 0 ; i < pvalues.size() ; ++i){
		if ( minimalPValue >=  pvalues.at(i)  ){
			if ( minimalPValue > pvalues.at(i)  ){
				bestPositionsOfPValues.clear();
				minimalPValue = pvalues.at(i);
			}
			bestPositionsOfPValues.push_back(i+1);
		}
	}

	cout << "\n\nThe best candidate position(s) is/are : ";

	for (const auto &i : bestPositionsOfPValues){
		cout << i << ' ';

	}
	cout << "with a p-value of = " << minimalPValue << ".\n";


	boost::math::chi_squared_distribution<double> aDistribution(1);


	cout << "\n--- Chi square association test on the ARG with averaged pvalues --- " << "\n";
	vector<long double> ARGpvaluesAVG = aArg.AvgChi2PValuesTestForTheARG();
	long double minimalAVGPValues = std::numeric_limits<long double>::max();
	vector<uint16_t> bestAVGPvaluesLoci;

	cout << "\nAverage p-values vector with the ARG for all loci.\n{";
	for ( size_t i = 0 ; i < ARGpvaluesAVG.size() ; ++i){
		if (i == ARGpvaluesAVG.size()-1){
			cout << ARGpvaluesAVG.at(i) << "}\n";
		}else{
			cout << ARGpvaluesAVG.at(i) << ", ";
		}
		if (minimalAVGPValues >= ARGpvaluesAVG.at(i)){
			if (minimalAVGPValues > ARGpvaluesAVG.at(i)){
				bestAVGPvaluesLoci.clear();
				minimalAVGPValues = ARGpvaluesAVG.at(i);
			}
			bestAVGPvaluesLoci.push_back(i+1);
		}
	}
	cout << "The best candidate position(s) is/are : ";
	for (const auto &i : bestAVGPvaluesLoci){
		cout << i << ' ';
	}
	cout  << "with a p-value of = " << minimalAVGPValues << ".\n";


	cout <<
	 */
	/*
	cout << "\n--- Chi-2 association test on the ARG with the maximum value. ---\n";
	boost::math::chi_squared_distribution<double> aDistribution(1);
	vector<long double> maxChi2Values = aArg.MaxChi2TestForTheARG();
	long double maxValueTemp = 0;
	vector<long double> maxValueTempPositions;

	cout << "Maximum chi-2 vector with the ARG for all loci.\n{";
	for ( size_t i = 0 ; i < maxChi2Values.size() ; ++i){
		if (i  == maxChi2Values.size()-1 ){
			cout << maxChi2Values.back() << "}\n";
		}else{
			cout << maxChi2Values.at(i) << ", ";
		}
		if ( maxValueTemp < maxChi2Values.at(i) ){
			maxValueTemp = maxChi2Values.at(i);
			maxValueTempPositions.clear();
		}
		maxValueTempPositions.push_back(i);
	}
	cout << "The best candidate position(s) is/are : ";
	for (const auto &i : maxValueTempPositions){
		cout << i << ' ';
	}
	cout << " with a chi-2 statistic of = " << maxValueTemp << " and with a p-value of " <<  (1-boost::math::cdf(aDistribution, maxValueTemp)) <<".\n";
	 */
}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE (ARG4WGzerosFinder)
BOOST_AUTO_TEST_CASE (oneLocusSequences){

	// Test #1
	SnipSequence aFirstSnipSequence = SnipSequence( "0","0"); // 0
	SnipSequence aSecondSnipSequence = SnipSequence("0","0"); // 0
	std::pair<size_t,size_t> leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);

	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1);

	// Test #2
	aFirstSnipSequence = SnipSequence( "0","0"); // 0
	aSecondSnipSequence = SnipSequence("0","1"); // 9
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1 );

	// Test #3
	aFirstSnipSequence = SnipSequence( "0","0"); // 0
	aSecondSnipSequence = SnipSequence("1","0"); // 1
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1);

	// Test #4
	aFirstSnipSequence = SnipSequence( "0","0"); // 0
	aSecondSnipSequence = SnipSequence("1","1"); // 9
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1);

	// Test #5
	aFirstSnipSequence = SnipSequence( "0","1"); // 9
	aSecondSnipSequence = SnipSequence("0","0"); // 0
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1);

	// Test #6
	aFirstSnipSequence = SnipSequence( "0","1"); // 9
	aSecondSnipSequence = SnipSequence("0","1"); // 9
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1);

	// Test #7
	aFirstSnipSequence = SnipSequence( "0","1"); // 9
	aSecondSnipSequence = SnipSequence("1","0"); // 1
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1);

	// Test #8
	aFirstSnipSequence = SnipSequence( "0","1"); // 9
	aSecondSnipSequence = SnipSequence("1","1"); // 9
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1);

	// Test #9
	aFirstSnipSequence = SnipSequence( "1","0"); // 1
	aSecondSnipSequence = SnipSequence("0","0"); // 0
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1);

	// Test #10
	aFirstSnipSequence = SnipSequence( "1","0"); // 1
	aSecondSnipSequence = SnipSequence("0","1"); // 9
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1);

	// Test #11
	aFirstSnipSequence = SnipSequence( "1","0"); // 1
	aSecondSnipSequence = SnipSequence("1","0"); // 1
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1);

	// Test #12
	aFirstSnipSequence = SnipSequence( "1","0"); // 1
	aSecondSnipSequence = SnipSequence("1","1"); // 9
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1);

	// Test #13
	aFirstSnipSequence = SnipSequence( "1","1"); // 9
	aSecondSnipSequence = SnipSequence("0","0"); // 0
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1);

	// Test #14
	aFirstSnipSequence = SnipSequence( "1","1"); // 9
	aSecondSnipSequence = SnipSequence("0","1"); // 9
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1);

	// Test #15
	aFirstSnipSequence = SnipSequence( "1","1"); // 9
	aSecondSnipSequence = SnipSequence("1","0"); // 1
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1);

	// Test #16
	aFirstSnipSequence = SnipSequence( "1","1"); // 9
	aSecondSnipSequence = SnipSequence("1","1"); // 9
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1);

}

BOOST_AUTO_TEST_CASE (threeLocusSequences)
{
	// Test #1
	SnipSequence aFirstSnipSequence = SnipSequence( "000","000"); // 000
	SnipSequence aSecondSnipSequence = SnipSequence("000","000"); // 000
	std::pair<size_t,size_t> leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 3 && leftRightSharedEnds.second == 3);

	// Test #2
	aFirstSnipSequence = SnipSequence( "000","000"); // 000
	aSecondSnipSequence = SnipSequence("010","000"); // 010
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 1 );

	// Test #3
	aFirstSnipSequence = SnipSequence( "000","000"); // 000
	aSecondSnipSequence = SnipSequence("010","010"); // 0?0
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 3 && leftRightSharedEnds.second == 3 );

	// Test #4
	aFirstSnipSequence = SnipSequence( "000","000"); // 000
	aSecondSnipSequence = SnipSequence("010","111"); // ???
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 3 && leftRightSharedEnds.second == 3 );

	// Test #5
	aFirstSnipSequence = SnipSequence( "000","000"); // 000
	aSecondSnipSequence = SnipSequence("001","000"); // 001
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 2 && leftRightSharedEnds.second == 3 );

	// Test #6
	aFirstSnipSequence = SnipSequence( "000","000"); // 000
	aSecondSnipSequence = SnipSequence("100","000"); // 100
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 3 && leftRightSharedEnds.second == 0 );

	// Test #7
	aFirstSnipSequence = SnipSequence( "010","000"); // 010
	aSecondSnipSequence = SnipSequence("000","100"); // ?00
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 3 && leftRightSharedEnds.second == 1 );

	// Test #8
	aFirstSnipSequence = SnipSequence( "010","000"); // 010
	aSecondSnipSequence = SnipSequence("000","001"); // 00?
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 3 );

	// Test #9
	aFirstSnipSequence = SnipSequence( "010","000"); // 010
	aSecondSnipSequence = SnipSequence("000","101"); // ?0?
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 3 && leftRightSharedEnds.second == 3 );

	// Test #10
	aFirstSnipSequence = SnipSequence( "010","000"); // 010
	aSecondSnipSequence = SnipSequence("000","111"); // ???
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 3 && leftRightSharedEnds.second == 3 );

	// Test #11
	aFirstSnipSequence = SnipSequence( "010","000"); // 010
	aSecondSnipSequence = SnipSequence("101","010"); // 1?1
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 3 && leftRightSharedEnds.second == 3 );

	// Test #12
	aFirstSnipSequence = SnipSequence( "010","000"); // 010
	aSecondSnipSequence = SnipSequence("111","000"); // 111
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 3 && leftRightSharedEnds.second == 3 );

	// Test #13
	aFirstSnipSequence = SnipSequence( "100","000"); // 100
	aSecondSnipSequence = SnipSequence("101","000"); // 101
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 2 && leftRightSharedEnds.second == 3 );

	// Test #14
	aFirstSnipSequence = SnipSequence( "111","000"); // 111
	aSecondSnipSequence = SnipSequence("111","000"); // 111
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 3 && leftRightSharedEnds.second == 3 );

	// Test #15
	aFirstSnipSequence = SnipSequence( "011","000"); // 011
	aSecondSnipSequence = SnipSequence("111","010"); // 1?1
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 3 && leftRightSharedEnds.second == 0 );

	// Test #16
	aFirstSnipSequence = SnipSequence( "011","000"); // 011
	aSecondSnipSequence = SnipSequence("111","001"); // 11?
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 3 && leftRightSharedEnds.second == 0 );

	// Test #17
	aFirstSnipSequence = SnipSequence( "010","000"); // 010
	aSecondSnipSequence = SnipSequence("011","010"); // 0?1
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 2 && leftRightSharedEnds.second == 3 );

	// Test #18
	aFirstSnipSequence = SnipSequence( "010","000"); // 010
	aSecondSnipSequence = SnipSequence("011","100"); // ?11
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 2 && leftRightSharedEnds.second == 3 );

	// Test #19
	aFirstSnipSequence = SnipSequence( "010","000"); // 010
	aSecondSnipSequence = SnipSequence("011","111"); // ???
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 3 && leftRightSharedEnds.second == 3 );

	//cout << "l:" << std::get<0>(leftRightSharedEnds) << " r: " << std::get<1>(leftRightSharedEnds) << endl;
}

BOOST_AUTO_TEST_CASE (otherSizesLocusSequences)
{
	// Test #1
	SnipSequence aFirstSnipSequence = SnipSequence( "10100","00000"); // 10100
	SnipSequence aSecondSnipSequence = SnipSequence("00100","00000"); // 00100
	std::pair<size_t,size_t> leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 5 && leftRightSharedEnds.second == 0);

	// Test #2
	aFirstSnipSequence = SnipSequence( "10000","00000"); // 10100
	aSecondSnipSequence = SnipSequence("11100","00000"); // 11100
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 1 && leftRightSharedEnds.second == 2);
}

BOOST_AUTO_TEST_CASE (longSequences)
{
	SnipSequence aFirstSnipSequence = SnipSequence( "000100100000000","110000000110010"); // 990100100990090
	SnipSequence aSecondSnipSequence = SnipSequence("110100000110000","001011000000000"); // 119199000110000
	std::pair<size_t,size_t> leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 6 && leftRightSharedEnds.second == 6);
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 6 && leftRightSharedEnds.second == 6);

	aFirstSnipSequence = SnipSequence( "00010010000000011","11000000011001000"); // 99010010099009011
	aSecondSnipSequence = SnipSequence("11010000011100011","00101100000000000"); // 11919900011100011
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 6 && leftRightSharedEnds.second == 11);
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 6 && leftRightSharedEnds.second == 11);

	aFirstSnipSequence = SnipSequence( "00010000100000010","11000000011001000"); // 99010000199009010
	aSecondSnipSequence = SnipSequence("11010000011000011","00101100000000000"); // 11919900011000011
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 8 && leftRightSharedEnds.second == 17);
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 8 && leftRightSharedEnds.second == 17);

	aFirstSnipSequence = SnipSequence( "00010100100000001","11000000011001000"); // 99010100199009001
	aSecondSnipSequence = SnipSequence("11010100011000011","00101000000000000"); // 11919100011000011
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 8 && leftRightSharedEnds.second == 15);
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 8 && leftRightSharedEnds.second == 15);

	aFirstSnipSequence = SnipSequence( "100010100100000011","011000000011001000"); // 199010100199009011
	aSecondSnipSequence = SnipSequence("011010100011000011","000101000000000000"); // 011919100011000011
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 18 && leftRightSharedEnds.second == 9);
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 18 && leftRightSharedEnds.second == 9);

	aFirstSnipSequence = SnipSequence( "000000001000000000","111111110111111111"); // ????????1?????????
	aSecondSnipSequence = SnipSequence("000000001000000000","111111110111111111"); // ????????1?????????
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 18 && leftRightSharedEnds.second == 18);
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 18 && leftRightSharedEnds.second == 18);

	aFirstSnipSequence = SnipSequence( "000000001000000000","111111110111111111"); // ????????1?????????
	aSecondSnipSequence = SnipSequence("000000000000000000","111111110111111111"); // ????????0?????????
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 18 && leftRightSharedEnds.second == 18);
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 18 && leftRightSharedEnds.second == 18);

	aFirstSnipSequence = SnipSequence( "000000001000000000","011111110111111111"); // 0???????1?????????
	aSecondSnipSequence = SnipSequence("000000000000000000","011111110111111111"); // 0???????0?????????
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 8 && leftRightSharedEnds.second == 18);
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 8 && leftRightSharedEnds.second == 18);

	aFirstSnipSequence = SnipSequence( "000000001000000000","111111110111111110"); // ????????1????????0
	aSecondSnipSequence = SnipSequence("000000000000000000","111111110111111110"); // ????????0????????0
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 18 && leftRightSharedEnds.second == 8);
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 18 && leftRightSharedEnds.second == 8);

	aFirstSnipSequence = SnipSequence( "000000001000000000","011111110111111110"); // 0???????1????????0
	aSecondSnipSequence = SnipSequence("000000000000000000","011111110111111110"); // 0???????0????????0
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aFirstSnipSequence, aSecondSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 8 && leftRightSharedEnds.second == 8);
	leftRightSharedEnds = ARG4WG::getleftAndRightSharedIndex( aSecondSnipSequence, aFirstSnipSequence);
	BOOST_CHECK(leftRightSharedEnds.first == 8 && leftRightSharedEnds.second == 8);

}

BOOST_AUTO_TEST_CASE (FullArgData)
{
	Parameters params =  Parameters();
	params.algorithmChoice = algorithms::ARG4WG;
	params.coalescenceModeChoice = coalescenceMode::HAVE_ANCESTRAL_MATERIAL_IN_COMMON;
	params.dataFile = "";
	params.numberOfSequences = 8;
	params.sequencesSize = 5;
	vector<size_t> distances {1,1,1,1,1};

	SnipSequence Leaf0("10000");
	ARGnode Node0(ARGNodeType::LEAFSAMPLEMODE, Leaf0, 0, std::numeric_limits<size_t>::max(), {{0},{0},{0},{0},{0}});
	SnipSequence Leaf1("11000");
	ARGnode Node1(ARGNodeType::LEAFSAMPLEMODE, Leaf1, 1, std::numeric_limits<size_t>::max(), {{1},{1},{1},{1},{1}});
	SnipSequence Leaf2("11100");
	ARGnode Node2(ARGNodeType::LEAFSAMPLEMODE, Leaf2, 2, std::numeric_limits<size_t>::max(), {{2},{2},{2},{2},{2}});
	SnipSequence Leaf3("10100");
	ARGnode Node3(ARGNodeType::LEAFSAMPLEMODE, Leaf3, 3, std::numeric_limits<size_t>::max(),{{3},{3},{3},{3},{3}});
	SnipSequence Leaf4("10110");
	ARGnode Node4(ARGNodeType::LEAFSAMPLEMODE, Leaf4, 4, std::numeric_limits<size_t>::max(),{{4},{4},{4},{4},{4}});
	SnipSequence Leaf5("00100");
	ARGnode Node5(ARGNodeType::LEAFSAMPLEMODE, Leaf5, 5, std::numeric_limits<size_t>::max(),{{5},{5},{5},{5},{5}});
	SnipSequence Leaf6("00101");
	ARGnode Node6(ARGNodeType::LEAFSAMPLEMODE, Leaf6, 6, std::numeric_limits<size_t>::max(),{{6},{6},{6},{6},{6}});
	SnipSequence Leaf7("00101");
	ARGnode Node7(ARGNodeType::LEAFSAMPLEMODE, Leaf7, 7, std::numeric_limits<size_t>::max(),{{7},{7},{7},{7},{7}});
	vector<bool> phenotypes {0,1,1,0};
	vector<ARGnode> leaves {Node0, Node1, Node2, Node3, Node4, Node5, Node6, Node7};
	ARG4WG aArg (params, leaves, phenotypes, distances);
	/*
	while (aArg.generateARandomCoalescentEvent() || aArg.generateARandomMutationEvent()){}
	for ( size_t i = 0 ; i < aArg.positionOfAliveNodes.size() ; ++i){
		cout << "(" << aArg.positionOfAliveNodes.at(i) << ") " << aArg.ARGnodes.at(aArg.positionOfAliveNodes.at(i)) << endl;
	}
	 */
	//std::vector<std::tuple<bool,size_t,size_t,size_t>> longestSharedEnds = aArg.findLongestSharedEnds();
	/*
	for ( size_t i = 0 ; i < longestSharedEnds.size() ; ++i){
		cout << i << " : " << std::boolalpha << std::get<0>(longestSharedEnds.at(i))  << std::noboolalpha <<
				" index : " << std::get<1>(longestSharedEnds.at(i)) <<
				" source node 1 : "  << std::get<2>(longestSharedEnds.at(i)) <<
				" source node 2 : "  << std::get<3>(longestSharedEnds.at(i)) << endl;
	}
	 */
	BOOST_CHECK(aArg.lenghtOfSequences == 5);
	BOOST_CHECK(aArg.numberOfSequences == 8);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  0);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  1);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  5);

	BOOST_CHECK(aArg.isAMutationEventPossible);
	vector<size_t> numberOfMutantLocus = {5,2,6,1,2};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	for( uint16_t i = 0 ; i < 8 ; ++i){
		for( uint16_t j = 0 ; j < 5 ; ++j){
			vector<uint16_t> leafReachTemp = {i};
			BOOST_CHECK(aArg.getLeafReachabilityforALocus(j, i) == leafReachTemp );
		}
	}

	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  1);
	vector<vector<bool>> coal = {
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,1},
			{0,0,0,0,0,0,1,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	vector<size_t> aliveNodes = {0,1,2,3,4,5,6,7};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	vector<vector<std::pair<size_t,size_t>>> leftRight = {
			{{5,5}, {1,1}, {1,2}, {2,2}, {2,3}, {5,2}, {5,5}, {5,5}},
			{{1,1}, {5,5}, {2,2}, {1,2}, {1,3}, {5,2}, {5,5}, {5,5}},
			{{1,2}, {2,2}, {5,5}, {1,1}, {1,3}, {5,1}, {5,5}, {5,5}},
			{{2,2}, {1,2}, {1,1}, {5,5}, {3,3}, {5,0}, {5,5}, {5,5}},
			{{2,3}, {1,3}, {1,3}, {3,3}, {5,5}, {5,3}, {5,5}, {5,5}},
			{{5,2}, {5,2}, {5,1}, {5,0}, {5,3}, {5,5}, {4,5}, {4,5}},
			{{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {4,5}, {5,5}, {5,5}},
			{{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {4,5}, {5,5}, {5,5}}
	};
	/*
	cout << "{\n";
	for ( size_t i = 0 ; i < aArg.leftRightSharedIndexMatrixForLiveNodes.size() ; ++i){
		cout << "{";
		for ( size_t j = 0 ; j < aArg.leftRightSharedIndexMatrixForLiveNodes.at(i).size()  ; ++j){
			cout << "{" << aArg.leftRightSharedIndexMatrixForLiveNodes.at(i).at(j).first << "," << aArg.leftRightSharedIndexMatrixForLiveNodes.at(i).at(j).first << "} ";
					if (j != aArg.leftRightSharedIndexMatrixForLiveNodes.at(i).size()-1 ){
						cout << ",";
					}

		}
		cout << "}";
		if (i != aArg.leftRightSharedIndexMatrixForLiveNodes.size()-1 ){
			cout << ",\n";
		}else{
			cout << "\n";
		}
	}
	cout << "}\n";
	 */
	BOOST_CHECK( leftRight == aArg.leftRightSharedIndexMatrixForLiveNodes);

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(6,7);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  0);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  5);

	BOOST_CHECK(aArg.isAMutationEventPossible);
	numberOfMutantLocus = {5,2,5,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	vector<uint16_t> leafReachTemp = {6,7};
	for( uint16_t j = 0 ; j < 5 ; ++j){
		BOOST_CHECK(aArg.getLeafReachabilityforALocus(j,8) == leafReachTemp );
	}

	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  0);
	coal = {{0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {0,1,2,3,4,5,8};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	leftRight = {
			{{5,5}, {1,1}, {1,2}, {2,2}, {2,3}, {5,2}, {5,5}},
			{{1,1}, {5,5}, {2,2}, {1,2}, {1,3}, {5,2}, {5,5}},
			{{1,2}, {2,2}, {5,5}, {1,1}, {1,3}, {5,1}, {5,5}},
			{{2,2}, {1,2}, {1,1}, {5,5}, {3,3}, {5,0}, {5,5}},
			{{2,3}, {1,3}, {1,3}, {3,3}, {5,5}, {5,3}, {5,5}},
			{{5,2}, {5,2}, {5,1}, {5,0}, {5,3}, {5,5}, {4,5}},
			{{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {4,5}, {5,5}}
	};

	BOOST_CHECK( leftRight == aArg.leftRightSharedIndexMatrixForLiveNodes);

	/*******************************************************************************************/
	aArg.generateARecombinationEvent(2,5);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  1);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  5);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {5,2,5,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 9) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 9) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 10) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 10) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 10) == leafReachTemp );
	leafReachTemp = {5};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 10) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 10) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 9) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 9) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 9) == leafReachTemp );

	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  3);
	coal = {{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0},
			{0,0,0,0,0,1,0,0},
			{0,0,0,0,0,1,0,0},
			{0,0,0,0,0,0,0,0},
			{0,0,1,1,0,0,0,0},
			{0,0,0,0,0,0,0,1},
			{0,0,0,0,0,0,1,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {0,1,2,3,4,9,8,10};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	leftRight = {
			{{5,5}, {1,1}, {1,2}, {2,2}, {2,3}, {5,2}, {5,5}, {5,0} },
			{{1,1}, {5,5}, {2,2}, {1,2}, {1,3}, {5,2}, {5,5}, {5,5} },
			{{1,2}, {2,2}, {5,5}, {1,1}, {1,3}, {5,5}, {5,5}, {5,5} },
			{{2,2}, {1,2}, {1,1}, {5,5}, {3,3}, {5,5}, {5,5}, {5,0} },
			{{2,3}, {1,3}, {1,3}, {3,3}, {5,5}, {3,3}, {5,5}, {5,0} },
			{{5,2}, {5,2}, {5,5}, {5,5}, {3,3}, {5,5}, {4,5}, {5,5} },
			{{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {4,5}, {5,5}, {5,5} },
			{{5,0}, {5,5}, {5,5}, {5,0}, {5,0}, {5,5}, {5,5}, {5,5} }
	};

	BOOST_CHECK( leftRight == aArg.leftRightSharedIndexMatrixForLiveNodes);

	/*******************************************************************************************/
	aArg.generateARecombinationEvent(1,4);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  2);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  5);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {5,2,5,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0,11) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1,12) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2,12) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3,12) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4,12) == leafReachTemp );
	leafReachTemp = {4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0,12) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1,11) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2,11) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3,11) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4,11) == leafReachTemp );

	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  8);
	coal = {{0,0,0,0,0,0,0,0,1},
			{0,0,0,0,0,0,0,0,1},
			{0,0,0,0,0,1,0,0,1},
			{0,0,0,0,0,1,0,0,1},
			{0,0,0,0,0,0,0,1,0},
			{0,0,1,1,0,0,0,0,0},
			{0,0,0,0,0,0,0,1,0},
			{0,0,0,0,1,0,1,0,0},
			{1,1,1,1,0,0,0,0,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {0,1,2,3,11,9,8,10,12};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	/*******************************************************************************************/
	aArg.generateARecombinationEvent(2,2);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  5);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {5,2,5,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 13) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 13) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 14) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 14) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 14) == leafReachTemp );
	leafReachTemp = {2};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 14) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 14) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 13) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 13) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 13) == leafReachTemp );

	//{3,13}, {9,13}, {9,3}, {10,11}, {10,8}, {12,0}, {12,1}, {12,3}, {14,1}, {14,12}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  10);
	coal = {{0,0,0,0,0,0,0,0,1,0},
			{0,0,0,0,0,0,0,0,1,1},
			{0,0,0,1,0,1,0,0,0,0},
			{0,0,1,0,0,1,0,0,1,0},
			{0,0,0,0,0,0,0,1,0,0},
			{0,0,1,1,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,1,0,0},
			{0,0,0,0,1,0,1,0,0,0},
			{1,1,0,1,0,0,0,0,0,1},
			{0,1,0,0,0,0,0,0,1,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {0,1,13,3,11,9,8,10,12,14};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(1,14);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  0);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  2);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  3);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  5);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {4,1,5,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {1,2};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 15) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 15) == leafReachTemp );

	leafReachTemp = {1};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 15) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 15) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 15) == leafReachTemp );

	// {3,13}, {9,13}, {9,3}, {10,11}, {10,8}, {12,0}, {12,15}, {12,3}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  8 );
	coal = {{0,0,0,0,0,0,0,0,1},
			{0,0,0,0,0,0,0,0,1},
			{0,0,0,1,0,1,0,0,0},
			{0,0,1,0,0,1,0,0,1},
			{0,0,0,0,0,0,0,1,0},
			{0,0,1,1,0,0,0,0,0},
			{0,0,0,0,0,0,0,1,0},
			{0,0,0,0,1,0,1,0,0},
			{1,1,0,1,0,0,0,0,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {0,15,13,3,11,9,8,10,12};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	/*******************************************************************************************/
	aArg.generetaAMutationEvent(1, 15);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  2);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  4);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {4,0,5,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {1,2};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 16) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 16) == leafReachTemp );

	leafReachTemp = {1};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 16) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 16) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 16) == leafReachTemp );

	// {16,0}, {3,13}, {9,13}, {9,3}, {10,11}, {10,8}, {12,0}, {12,16}, {12,3}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  9 );
	coal = {{0,1,0,0,0,0,0,0,1},
			{1,0,0,0,0,0,0,0,1},
			{0,0,0,1,0,1,0,0,0},
			{0,0,1,0,0,1,0,0,1},
			{0,0,0,0,0,0,0,1,0},
			{0,0,1,1,0,0,0,0,0},
			{0,0,0,0,0,0,0,1,0},
			{0,0,0,0,1,0,1,0,0},
			{1,1,0,1,0,0,0,0,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {0,16,13,3,11,9,8,10,12};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );
	/*
	leftRight = {
			{{5,5}, {1,1}, {1,2}, {2,2}, {2,3}, {5,2}, {5,5}, {5,0} },
			{{1,1}, {5,5}, {2,2}, {1,2}, {1,3}, {5,2}, {5,5}, {5,5} },
			{{1,2}, {2,2}, {5,5}, {1,1}, {1,3}, {5,5}, {5,5}, {5,5} },
			{{2,2}, {1,2}, {1,1}, {5,5}, {3,3}, {5,5}, {5,5}, {5,0} },
			{{2,3}, {1,3}, {1,3}, {3,3}, {5,5}, {3,3}, {5,5}, {5,0} },
			{{5,2}, {5,2}, {5,5}, {5,5}, {3,3}, {5,5}, {4,5}, {5,5} },
			{{5,5}, {5,5}, {5,5}, {5,5}, {5,5}, {4,5}, {5,5}, {5,5} },
			{{5,0}, {5,5}, {5,5}, {5,0}, {5,0}, {5,5}, {5,5}, {5,5} }
	};
	 */
	//BOOST_CHECK( leftRight == aArg.leftRightSharedIndexMatrixForLiveNodes);
	/*

	cout << "     ";
	for ( size_t i = 0 ; i < aArg.positionOfAliveNodes.size() ; ++i){
		cout << aArg.positionOfAliveNodes.at(i) << "     ";
	}
	cout << endl;

	for ( size_t i = 0 ; i < aArg.leftRightSharedIndexMatrixForLiveNodes.size() ; ++i){
		cout << aArg.positionOfAliveNodes.at(i) << " {";
		for ( size_t j = 0 ; j < aArg.leftRightSharedIndexMatrixForLiveNodes.at(i).size() ; ++j){
			cout << "{" << aArg.leftRightSharedIndexMatrixForLiveNodes.at(i).at(j).first << "," << aArg.leftRightSharedIndexMatrixForLiveNodes.at(i).at(j).second <<"}, ";
		}
		cout << "},"<< endl;
	}

	 */

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(3,13);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  3);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  4);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {4,0,4,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {3};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 17) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 17) == leafReachTemp );

	leafReachTemp = {2,3};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 17) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 17) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 17) == leafReachTemp );

	// {16,0}, {9,17}, {10,11}, {10,8}, {12,0}, {12,16}, {12,17},
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  7 );
	coal = {{0,1,0,0,0,0,0,1},
			{1,0,0,0,0,0,0,1},
			{0,0,0,0,1,0,0,1},
			{0,0,0,0,0,0,1,0},
			{0,0,1,0,0,0,0,0},
			{0,0,0,0,0,0,1,0},
			{0,0,0,1,0,1,0,0},
			{1,1,1,0,0,0,0,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {0,16,17,11,9,8,10,12};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(12,17);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  4);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  4);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {3,0,4,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {3,4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 18) == leafReachTemp );

	leafReachTemp = {3};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 18) == leafReachTemp );

	leafReachTemp = {2,3};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 18) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 18) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 18) == leafReachTemp );

	// {16,0}, {9,18},{10,11},{10,8}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  4 );
	coal = {{0,1,0,0,0,0,0},
			{1,0,0,0,0,0,0},
			{0,0,0,0,1,0,0},
			{0,0,0,0,0,0,1},
			{0,0,1,0,0,0,0},
			{0,0,0,0,0,0,1},
			{0,0,0,1,0,1,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {0,16,18,11,9,8,10};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(10,11);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  1);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  5);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);
	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);

	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  4);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {3,0,4,1,1};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {5};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 19) == leafReachTemp );

	leafReachTemp = {4,5};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 19) == leafReachTemp );

	leafReachTemp = {4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 19) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 19) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 19) == leafReachTemp );

	// {16,0}, {9,18}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  2 );
	coal = {{0,1,0,0,0,0},
			{1,0,0,0,0,0},
			{0,0,0,0,1,0},
			{0,0,0,0,0,0},
			{0,0,1,0,0,0},
			{0,0,0,0,0,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {0,16,18,19,9,8};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	/*******************************************************************************************/
	aArg.generetaAMutationEvent(4,8);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  2);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  5);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  1);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  3);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {3,0,4,1,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 20) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 20) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 20) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 20) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 20) == leafReachTemp );

	// {16,0}, {9,18}, {20,9}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  3 );
	coal = {{0,1,0,0,0,0},
			{1,0,0,0,0,0},
			{0,0,0,0,1,0},
			{0,0,0,0,0,0},
			{0,0,1,0,0,1},
			{0,0,0,0,1,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {0,16,18,19,9,20};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(9,20);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  2);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  6);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  3);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  1);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  3);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {3,0,3,1,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {6,7};
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(0, 21) == leafReachTemp );
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(1, 21) == leafReachTemp );
	leafReachTemp = {5,6,7};
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(2, 21) == leafReachTemp );
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(3, 21) == leafReachTemp );
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(4, 21) == leafReachTemp );

	// {16,0}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  1 );
	coal = {{0,1,0,0,0},
			{1,0,0,0,0},
			{0,0,0,0,0},
			{0,0,0,0,0},
			{0,0,0,0,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {0,16,18,19,21};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	/*******************************************************************************************/
	aArg.generateARecombinationEvent(1,18);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  2);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  6);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  1);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  3);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {3,0,3,1,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {};
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(0,22) == leafReachTemp );
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(1, 23) == leafReachTemp );
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(2, 23) == leafReachTemp );
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(3, 23) == leafReachTemp );
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(4, 23) == leafReachTemp );

	leafReachTemp = {3,4};
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(0, 23) == leafReachTemp );

	leafReachTemp = {3};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1,22) == leafReachTemp );

	leafReachTemp = {2,3};
	BOOST_CHECK( aArg.getLeafReachabilityforALocus(2,22) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3,22) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4,22) == leafReachTemp );

	// {16,0}, {21,22}, {23,0}, {23,16},
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  4 );
	coal = {{0,1,0,0,0,1},
			{1,0,0,0,0,1},
			{0,0,0,0,1,0},
			{0,0,0,0,0,0},
			{0,0,1,0,0,0},
			{1,1,0,0,0,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {0,16,22,19,21,23};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(23,16);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  2);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  7);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  1);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  3);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {2,0,3,1,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {1,2,3,4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 24) == leafReachTemp );

	leafReachTemp = {1,2};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 24) == leafReachTemp );

	leafReachTemp = {1};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 24) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 24) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 24) == leafReachTemp );

	// {24,0}, {21,22}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  2 );
	coal = {{0,1,0,0,0},
			{1,0,0,0,0},
			{0,0,0,0,1},
			{0,0,0,0,0},
			{0,0,1,0,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {0,24,22,19,21};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(0,24);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  2);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  8);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  2);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  3);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {1,0,3,1,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {0,1,2,3,4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0,25) == leafReachTemp );

	leafReachTemp = {0,1,2};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1,25) == leafReachTemp );

	leafReachTemp = {0,1};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2,25) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3,25) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4,25) == leafReachTemp );

	//  {21,22}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  1 );
	coal = {{0,0,0,0},
			{0,0,0,1},
			{0,0,0,0},
			{0,1,0,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {25,22,19,21};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	/*******************************************************************************************/
	aArg.generetaAMutationEvent(0, 25);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  3);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  8);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  1);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  2);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {0,0,3,1,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {0,1,2,3,4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 26) == leafReachTemp );

	leafReachTemp = {0,1,2};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 26) == leafReachTemp );

	leafReachTemp = {0,1};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 26) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 26) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 26) == leafReachTemp );

	//  {21,22}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  1 );
	coal = {{0,0,0,0},
			{0,0,0,1},
			{0,0,0,0},
			{0,1,0,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {26,22,19,21};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	/*******************************************************************************************/
	aArg.generetaAMutationEvent(3, 19);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  4);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  8);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  0);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  1);
	BOOST_CHECK(!aArg.isAMutationEventPossible);

	numberOfMutantLocus = {0,0,3,0,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {5};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 27) == leafReachTemp );

	leafReachTemp = {4,5};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 27) == leafReachTemp );

	leafReachTemp = {4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 27) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 27) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 27) == leafReachTemp );

	//  {27,22}, {21,22}, {21,27}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  3);
	coal = {{0,0,0,0},
			{0,0,1,1},
			{0,1,0,1},
			{0,1,1,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {26,22,27,21};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(22,27);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  4);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  9);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  0);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  1);
	BOOST_CHECK(!aArg.isAMutationEventPossible);

	numberOfMutantLocus = {0,0,2,0,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {5};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 28) == leafReachTemp );

	leafReachTemp = {3,4,5};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 28) == leafReachTemp );

	leafReachTemp = {2,3,4};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 28) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 28) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 28) == leafReachTemp );

	//  {28,21}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  1);
	coal = {{0,0,0},
			{0,0,1},
			{0,1,0}};
	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {26,28,21};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(28,21);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  4);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  10);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  1);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  1);
	BOOST_CHECK(aArg.isAMutationEventPossible);

	numberOfMutantLocus = {0,0,1,0,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {5,6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 29) == leafReachTemp );

	leafReachTemp = {3,4,5,6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 29) == leafReachTemp );

	leafReachTemp = {2,3,4,5,6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 29) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 29) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 29) == leafReachTemp );

	//
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  0);
	coal = {{0,0},
			{0,0}};

	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {26,29};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );


	/*******************************************************************************************/
	aArg.generetaAMutationEvent(2, 29);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  5);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  10);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  0);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  0);
	BOOST_CHECK(!aArg.isAMutationEventPossible);

	numberOfMutantLocus = {0,0,0,0,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {5,6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0, 30) == leafReachTemp );

	leafReachTemp = {3,4,5,6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1, 30) == leafReachTemp );

	leafReachTemp = {2,3,4,5,6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2, 30) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3, 30) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4, 30) == leafReachTemp );

	//	{29,30}
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  1);
	coal = {{0,1},
			{1,0}};

	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {26,30};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

	/*******************************************************************************************/
	aArg.generateACoalescentEvent(26,30);

	BOOST_CHECK(aArg.totalNumberOfMutationEvent ==  5);
	BOOST_CHECK(aArg.totalNumberOfCoalescenceEvent ==  11);
	BOOST_CHECK(aArg.totalNumberOfRecombinationEvent ==  4);

	BOOST_CHECK(aArg.numberOfMutationPossible ==  0);
	BOOST_CHECK(aArg.numberOfPositionToBeMuted ==  0);
	BOOST_CHECK(!aArg.isAMutationEventPossible);

	numberOfMutantLocus = {0,0,0,0,0};
	BOOST_CHECK(aArg.numberOfMutantSNPsByPosition == numberOfMutantLocus );

	leafReachTemp = {0,1,2,3,4,5,6,7};
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(0,31) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(1,31) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(2,31) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(3,31) == leafReachTemp );
	BOOST_CHECK(aArg.getLeafReachabilityforALocus(4,31) == leafReachTemp );

	//
	BOOST_CHECK(aArg.numberOfCoalescencePossible ==  0);
	coal = {{0}};

	// BOOST_CHECK(aArg.coalescencePosibilitiesForLivesNodes == coal);

	aliveNodes = {31};
	BOOST_CHECK(aArg.positionOfAliveNodes == aliveNodes );

}
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE (ARG4WGzerosFinder)
BOOST_AUTO_TEST_CASE (smallSequences){
	// Test #1
	SnipSequence aFirstSnipSequence = SnipSequence( "10100","11000"); // 99100
	SnipSequence aSecondSnipSequence = SnipSequence("10000","00000"); // 10000
	size_t rightMost = Manhattan::getRightMostIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(rightMost == 0);

	// Test #2
	aFirstSnipSequence = SnipSequence( "10100","11000"); // 99100
	aSecondSnipSequence = SnipSequence("11000","00000"); // 11000
	rightMost = Manhattan::getRightMostIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(rightMost == 0);

	// Test #3
	aFirstSnipSequence = SnipSequence( "10100","11000"); // 99100
	aSecondSnipSequence = SnipSequence("10100","00000"); // 10100
	rightMost = Manhattan::getRightMostIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(rightMost == 5);

	// Test #4
	aFirstSnipSequence = SnipSequence( "10100","11000"); // 99100
	aSecondSnipSequence = SnipSequence("10101","00000"); // 10101
	rightMost = Manhattan::getRightMostIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(rightMost == 4);

	// Test #5
	aFirstSnipSequence = SnipSequence( "10100","01000"); // 19100
	aSecondSnipSequence = SnipSequence("00101","00000"); // 00101
	rightMost = Manhattan::getRightMostIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(rightMost == 0);

	// Test #6
	aFirstSnipSequence = SnipSequence( "10100","00000"); // 10100
	aSecondSnipSequence = SnipSequence("11101","00000"); // 11101
	rightMost = Manhattan::getRightMostIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(rightMost == 1);

	// Test #7
	aFirstSnipSequence = SnipSequence( "00000","11011"); // 99099
	aSecondSnipSequence = SnipSequence("00100","11000"); // 99100
	rightMost = Manhattan::getRightMostIndex(aFirstSnipSequence,aSecondSnipSequence);
	BOOST_CHECK(rightMost == 0);



	//cout << "rightMost = " << rightMost << std::endl;
}
BOOST_AUTO_TEST_SUITE_END()

