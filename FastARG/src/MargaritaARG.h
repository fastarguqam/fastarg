/*
 * MargaritaARG.h
 *
 *  Created on: 2016-09-22
 *      Author: �ric Marcotte
 */

#ifndef MARGARITAARG_H_
#define MARGARITAARG_H_

#include "ARG.h"
#include "ARGnode.h"
#include "SnipSequence.h"

//#include "Enums.h"

class MargaritaARG: public ARG {
public:
	double MaximumRecombinationRate;
	unsigned int numberOfRandomRecombinaisonEvent;
	MargaritaARG( Parameters params, vector<ARGnode> Leaves, const vector<bool> &phenotypes,const  vector<size_t> & distances);
	bool generateARandomMargaritaRecombinationEvent();
	bool generateARandomOptimalMargaritaRecombinationEvent();
	static bool verifyParameters(const Parameters & parameters);
	std::pair<size_t,size_t> bestAlphaBeta(size_t & i,size_t & j);
	static std::pair<size_t,size_t> margaritaCommonTractFinder(const SnipSequence &, const SnipSequence & );
	virtual void run(void) override;
	void printARGSummary();
	virtual ~MargaritaARG() = default;
};

#endif /* MARGARITAARG_H_ */
