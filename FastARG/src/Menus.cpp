/*
 * Menu.cpp
 *
 *  Created on: 2015-12-05
 *      Author: �ric Marcotte
 */

#include <cassert>

#include "Menus.h"

using std::cout;
using std::cin;

int mainMenu(){
	std::string choices = "PRTCADGQ";
	char choice;


	cout << "                                                               \n";
	cout << "                    ***   Main menu ***                        \n";
	cout << " Please choose of the following:                               \n";
	cout << " (P)henotypes retrieval from old format                        \n";
	cout << " (R)emove all loci with no mutant locus                        \n";
	cout << " (T)ransform ARG4WG to FastARG data file format.               \n";
	cout << " (C)onvert the old format to new format.                       \n";
	cout << " (A)RG data and parameters from files test.                    \n";
	cout << " (D)ebug.                                                      \n";
	cout << " (G)et system  information.                                    \n";
	cout << " (Q)uit and close the program.                                 " << std::endl;

	choice = validateUserChoice(choices);

	switch (choice){
	case 'D': return 8;                                    break; //(D)ebug.
	case 'T': return 9;                                    break; // ARG4WG to FastARG format.
	case 'P': return 7;                                    break; //(P)henotypes retreival from old format
	case 'R': return 6;                                    break; //(P)arallel snips data structure test.
	case 'C': return 5;                                    break; //(C)onvert the old format to new format.
	case 'A': return 4;                                    break; //(A)RG data and parameters from files test.
	case 'G': return 2;                                    break; //(G)et systeme information.
	case 'Q': return 0;                                    break;
	}
	return -1;

}

char validateUserChoice(std::string choices){
	char choice;
	assert(choices.size() >= 2);

	cout << "Enter your choice:";
	cin >> choice;

	while ( choices.find(::tolower(choice)) == std::string::npos && choices.find(::toupper(choice)) == std::string::npos){
		cout << "The choice '" <<  choice << "' is invalid!\n";
		cout << "Please choose between: [";
		for (unsigned int i = 0 ; i < choices.size()-1 ; ++i){
			cout << choices.at(i) << ',';
		}
		cout << choices.at(choices.size()-1) << "]!\n";
		cout << "Enter your choice:" << std::endl;
		cin >> choice;
	}

	return ::toupper(choice);
}

