/*
 * Manhattan.cpp
 *
 *  Created on: 2017-02-23
 *      Author: �ric Marcotte
 */

#include<boost/progress.hpp>

#include "Manhattan.h"

using std::cout;

Manhattan::Manhattan( Parameters params, vector<ARGnode> Leaves, const vector<bool>& phenotypes, const vector<size_t>& distances )
// Call to super !!
:ARG( params, Leaves,  phenotypes,  distances){
}

void Manhattan::run(void){

	boost::progress_display show_progress( numberOfPositionToBeMuted );
	while (this->positionOfAliveNodes.size() > 1 ){ // i.e. While not OMRCA

		show_progress += this->totalNumberOfMutationEvent - show_progress.count();
		// If an coalescence and a mutation is possible.
		if (this->numberOfCoalescencePossible != 0 && this->numberOfMutationPossible !=0 ){

			// We choose proportionally between possible events.
			std::uniform_int_distribution<size_t> randomLongestSharedEnd(1 ,this->numberOfCoalescencePossible + this->numberOfMutationPossible );
			size_t winningTransition = randomLongestSharedEnd(this->eng);

			// Then we make the "winning" event.
			if (winningTransition <= this->numberOfCoalescencePossible ){
				generateARandomCoalescentEvent();
			}else{
				generateARandomMutationEvent();
			}
		}else if (this->numberOfCoalescencePossible == 0 && this->numberOfMutationPossible !=0 ){ // Case if only a mutation is possible.
			generateARandomMutationEvent();
		}else if (this->numberOfCoalescencePossible != 0 && this->numberOfMutationPossible ==0 ){ // Case if only a coalescence is possible.
			generateARandomCoalescentEvent();
		}else{ // Case when we need to recombine.
			generatesAnManhattanRecombinationEvents();
		}
	}
}

void Manhattan::generatesAnManhattanRecombinationEvents(){
	// First we need to compute the total number of mutant loci in each sequences alive.
	// Second we need to find at which index(es) the maximum is reach.
	size_t maximum = 0;

	vector<size_t> indexOfMaximalMutantSequences;

	// We iterate trough the alive nodes.
	for ( size_t i = 0 ; i < this->positionOfAliveNodes.size() ; ++i){
		size_t tempTotal4aSequence = this->ARGnodes.at(this->positionOfAliveNodes.at(i)).numberOfMutantLoci;

		if(maximum <= tempTotal4aSequence ){
			// Then we check if the current maximum is bested.
			if (maximum < tempTotal4aSequence ){
				indexOfMaximalMutantSequences.clear();
				maximum = tempTotal4aSequence;
			}
			// We add the alive node index of the best candidate sequences.
			indexOfMaximalMutantSequences.push_back(this->positionOfAliveNodes.at(i));
		}
	}
	// Now we must choose most mutant sequence randomly.
	std::uniform_int_distribution<size_t> maxMutantSequences(0,indexOfMaximalMutantSequences.size()-1);
	size_t theChosenSequence = maxMutantSequences(this->eng);

	// Then we generate the event for a chosen node.
	generatesAnManhattanRecombinationEvents(indexOfMaximalMutantSequences.at(theChosenSequence));
}

void Manhattan::generatesAnManhattanRecombinationEvents(size_t theliveNode){
	size_t indexOfTreatedLoci = 0;
	size_t theCurrentNode = theliveNode;
	size_t nextTobeCoalNode = this->ARGnodes.size(); // Invalid so it makes sure we will process the next step.


	while (indexOfTreatedLoci < lenghtOfSequences){
		std::pair<size_t,size_t> nextEvent = nextRecombinationEvent(theCurrentNode);
		nextTobeCoalNode = nextEvent.second;

		if ( nextEvent.first == lenghtOfSequences){
			indexOfTreatedLoci = nextEvent.first;
			generateACoalescentEvent( theCurrentNode, nextTobeCoalNode); // Left material with the node to coalesce.
			/*                                                                             011010110010010
			 * This case is weird imagine we are in a state like :                         ????00110010110.
			 * And all other sequences are in a mutant state for the first                 101010001119100
			 * locus after the unknowns in the current node.                               101010101010010
			 * Then we can't recombine the sequence and coalesce the
			 * left material. The solution is to recombine one locus
			 * after and continue with the right material.
			 *
			 */
		}else if (nextEvent.first == 0){
			if (indexOfTreatedLoci == lenghtOfSequences -1){
				++indexOfTreatedLoci;
			}else{
				generateARecombinationEvent( indexOfTreatedLoci+1, theCurrentNode );
				theCurrentNode = this->ARGnodes.size()-2; // Right Material.
				++indexOfTreatedLoci;
			}
		}else{
			indexOfTreatedLoci = nextEvent.first;
			generateARecombinationEvent(indexOfTreatedLoci, theCurrentNode );
			theCurrentNode = this->ARGnodes.size()-2; // Right Material.
			generateACoalescentEvent(this->ARGnodes.size()-1,nextTobeCoalNode); // Left material with the node to coalesce.
		}
	}
}

/*
 *  The first element of the pair need to correspond to the first right most index that is
 *  not compatible.
 *
 *  The second element is the alive node which the best match is found. If there is more
 *  than one index we choose uniformly.
 */
std::pair<size_t,size_t> Manhattan::nextRecombinationEvent (size_t theliveNode){
	vector<std::pair<size_t,size_t>> bestPairs;
	size_t tempRightMostIndex = 0;
	size_t maximalIndex = 0;

	for ( size_t i = 0 ; i < this->positionOfAliveNodes.size()  ; ++i){
		if (this->positionOfAliveNodes.at(i) != theliveNode){
			tempRightMostIndex = getRightMostIndex(this->ARGnodes.at(this->positionOfAliveNodes.at(i)).sequence, this->ARGnodes.at(theliveNode).sequence  );
			if (maximalIndex <= tempRightMostIndex ){
				if ( maximalIndex < tempRightMostIndex ){
					bestPairs.clear();
					maximalIndex = tempRightMostIndex;
				}
				bestPairs.push_back(std::make_pair(tempRightMostIndex,this->positionOfAliveNodes.at(i)));
			}
		}
	}

	// Now we must choose a pair randomly between all best pair.
	std::uniform_int_distribution<size_t> indexOfChosenPair(0, bestPairs.size()-1);
	size_t temp = indexOfChosenPair(this->eng);

	return bestPairs.at(temp);
}

size_t Manhattan::getRightMostIndex (const SnipSequence & firstSequence, const SnipSequence & secondSequence ){

	size_t leftSideLimit = 0;
	std::valarray<DATA_TYPE> A = firstSequence.AncestralDataMask;
	std::valarray<DATA_TYPE> B = firstSequence.snipsData;
	std::valarray<DATA_TYPE> C = secondSequence.AncestralDataMask;
	std::valarray<DATA_TYPE> D = secondSequence.snipsData;

	// First we must found the common ancestral material. Mark with a one when true.
	//	A	B	Y
	//	0	0	1
	//	0	1	0
	//	1	0	0
	//	1	1	0
	std::valarray<DATA_TYPE> knownAncestralMaterial = ((~A)&(~C));

	// 	        M1  D1 	M2	D2	Result
	// 0	    0	0	0	0	  1
	// 1	    0	0	0	1	  0  <- 0 - 1 incompatible
	// 2	    0	0	1	0	  1
	// 3	    0	0	1	1	  1
	// 4	    0	1	0	0	  0  <- 0 - 1 incompatible
	// 5	    0	1	0	1	  1
	// 6	    0	1	1	0	  1
	// 7	    0	1	1	1	  1
	// 8	    1	0	0	0	  1
	// 9	    1	0	0	1	  1
	// 10   	1	0	1	0	  1
	// 11	    1	0	1	1	  1
	// 12	    1	1	0	0	  1
	// 13	    1	1	0	1	  1
	// 14	    1	1	1	0	  1
	// 15	    1	1	1	1	  1

	// This check for 0-1 or 1-0 incompatibilities.
	std::valarray<DATA_TYPE> result = ( C | A | ((~B)&(~D)) | ((B)&(D)) );

	DATA_TYPE binaryIndicator = BIT_INDICATOR;
	DATA_TYPE resultBlock = 0;
	bool leftSideLimitFound = false;
	size_t maxJ;

	// First we find the first index of the zero !
	for (size_t i = 0 ; i < firstSequence.getnumberOfBlock() && !leftSideLimitFound ;++i){
		resultBlock = result[i];
		// Check if we are on the last block.

		// If we are we only go trough until the end of the last locus.
		if ( i == firstSequence.getnumberOfBlock()-1 ){
			maxJ = firstSequence.getSizeOfSequence() % BIT_SIZE_OF_DATA_TYPE;
		}else{ // Else the block is full so we can go trough completely the block.
			maxJ = BIT_SIZE_OF_DATA_TYPE;
		}

		// With the right block and the right limit we can go trough the sequences for a block.
		for (size_t j = 0 ; j < maxJ ; ++j){
			if ( !(resultBlock & binaryIndicator) ){ // True when bit is 0
				leftSideLimit =  i*BIT_SIZE_OF_DATA_TYPE+j;
				leftSideLimitFound = true;
				j = maxJ; // to get out!
			}
			binaryIndicator = binaryIndicator >> 1;
		}
		binaryIndicator = BIT_INDICATOR;
	}
	if (!leftSideLimitFound){
		leftSideLimit = firstSequence.getSizeOfSequence();
	}

	bool foundOne = false;
	// We need to make there is a common ancestral position before the first zero!
	for ( size_t i = 0 ; i < leftSideLimit ; ++i){
		if ( SnipSequence::isPositionAncestral(i,firstSequence) && SnipSequence::isPositionAncestral(i,secondSequence) ){
			foundOne = true;
			i = leftSideLimit; // to break out !
		}
	}
	if (!foundOne){	leftSideLimit = 0; }

	return leftSideLimit;
}

void Manhattan::printARGSummary() {
	cout << "             ***  Manhattan ARG summary *** \n";
	ARG::printARGSummary();
	cout << "Manhattan specific attributes :\n";
	cout << " - The seed is                        : " << this->seed << "\n";
}

Manhattan::~Manhattan() { }

