
/*
 * SnipSequence.h
 *
 *  Created on: 2016-02-28
 *  Author: �ric Marcotte
 *  www.32x8.com
 *
 */

// TODO make a 64 bit version
#include <string>
#include <vector>
#include <valarray>
#include "Enums.h"

// 32 bits !
/*
#define BIT_INDICATOR 0x80000000
#define ALLONES 0xffffffff
#define BIT_SIZE_OF_DATA_TYPE 32
#define DATA_TYPE uint32_t
*/

// 64 bits !
#define BIT_INDICATOR 0x8000000000000000
#define ALLONES 0xffffffffffffffff
#define BIT_SIZE_OF_DATA_TYPE 64
#define DATA_TYPE uint64_t


#ifndef SNIPSEQUENCE_H_
#define SNIPSEQUENCE_H_

using std::string;
using std::valarray;
using std::ostream;
using std::vector;


class SnipSequence {
private:
	// Private members set in constructors with public getters.
	size_t sizeOfSequence;
	size_t numberOfBlock;
public:

	// Flag for coalescence constraints.
	static coalescenceMode coalescenceModeFlag;

	// Public members
	valarray<DATA_TYPE> snipsData;
	valarray<DATA_TYPE> AncestralDataMask;

	// Constructor
	SnipSequence( );
	SnipSequence( string );
	SnipSequence( string, string );
	SnipSequence( valarray<DATA_TYPE>, valarray<DATA_TYPE>, size_t );

	// TODO MOVE COPY ... constructors
	// Destructor
	virtual ~SnipSequence() =default;

	// Overloading of + operator
	// Will coalesce two sequences, regardless of compatibility.
	// If there is an coalescence between a primitive and a mutant marker
	// The result will be an mutant marker.
	inline friend SnipSequence operator+ ( const SnipSequence & S1, const SnipSequence & S2) {
		return  SnipSequence( ((~S2.AncestralDataMask) & S2.snipsData) | ((~S1.AncestralDataMask)&S1.snipsData) , S1.AncestralDataMask & S2.AncestralDataMask, S1.getSizeOfSequence() );
	}

	// Checks if a certain position if a mutant snip for a sequence.
	inline static bool isPositionMutant (size_t thePosition, const SnipSequence & aSnipSequence){
		return (aSnipSequence.snipsData[(thePosition / BIT_SIZE_OF_DATA_TYPE)] & (BIT_INDICATOR >> (thePosition % BIT_SIZE_OF_DATA_TYPE))) &&
					!(aSnipSequence.AncestralDataMask[(thePosition / BIT_SIZE_OF_DATA_TYPE)] & (BIT_INDICATOR >> (thePosition % BIT_SIZE_OF_DATA_TYPE))) ;
	}

	inline static bool isPositionAncestral (size_t position, const SnipSequence & aSnipSequence){
		return (!(aSnipSequence.AncestralDataMask[position / BIT_SIZE_OF_DATA_TYPE] & BIT_INDICATOR >> (position % BIT_SIZE_OF_DATA_TYPE)));
	}

	// Mutation function for a non-ancestral valid position.
	static SnipSequence mutateAPosition( const SnipSequence &, size_t );

    // Recombination function
	static std::pair<SnipSequence,SnipSequence> recombinationOnAposition( const SnipSequence &, size_t );

	//Overloading of << operator
	friend ostream& operator << (ostream &, const SnipSequence &);

	// Overloading of == operator
	// This operator will assert if two sequences can coalesce!
	bool operator == (const SnipSequence &) const;

	// Each function test if 2 sequences if they can coalesce with different specifications.
	static bool noConstraintCoalescence ( const SnipSequence &, const SnipSequence & );
	static bool commonMaterialCoalescenceTest (const SnipSequence &, const SnipSequence & );


	// This operator will assert if two sequences can't coalesce!
	inline bool operator != (const SnipSequence &aSnipSequence) const {return !(this->operator ==(aSnipSequence));}

	// generates a random SnipSequence of a specified size
	static SnipSequence generateARandomSnipSequence(SnipSequence);

	// Converter
	static valarray<DATA_TYPE> convertStringToFastSequence( string );

	// toString on 0,1 and 9 alphabet. Use in the OStream operator.
	static string toString(const SnipSequence &);

	// Setters and getters for the private static data members.
	//size_t getSizeOfSequence() const;
	//size_t getnumberOfBlock() const;
	inline size_t getSizeOfSequence() const { return this->sizeOfSequence; }
	inline size_t getnumberOfBlock()  const { return this->numberOfBlock;  }
};

#endif /* SNIPSEQUENCE_H_ */
