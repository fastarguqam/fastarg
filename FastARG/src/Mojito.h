/*
 * Mojito.h
 *
 *  Created on: 2017-01-31
 *      Author: �ric Marcotte
 */

#ifndef MOJITO_H_
#define MOJITO_H_

#include "ARG.h"
#include "ARGnode.h"
#include "SnipSequence.h"

class Mojito: public ARG {
public:


	Mojito( Parameters params, vector<ARGnode> Leaves, const vector<bool> & phenotypes,const  vector<size_t> &distances);
	bool generateARandomMojitoRecombinationEvent();
	bool generateARandomOptimalMojitoRecombinationEvent();
	//size_t getnumberMutantLocusForBestCoalescence();
	//size_t getnumberMutantLocusForBestRecombination();
	//std::pair<size_t,size_t> bestMojitoCoalescence();
	std::pair<size_t,size_t> bestAlphaBeta(size_t & i,size_t & j);
	static std::pair<size_t,size_t> mojitoLongestCommonTractFinder(const SnipSequence &, const SnipSequence & );
	void printARGSummary();
	virtual void run(void) override;
	virtual ~Mojito() = default;
};

#endif /* MOJITO_H_ */
