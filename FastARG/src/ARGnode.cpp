/*
 * ARGnode.cpp
 *
 *  Created on: 2015-09-04
 *      Author: �ric Marcotte
 */

#include <cassert>
#include <iostream>

#include "ARGnode.h"

/*********************************************************************************************/
/**                                   NODE CONSTRUCTOR                                      **/
/*********************************************************************************************/

ARGnode::ARGnode( ARGNodeType aType, SnipSequence sequence, uint16_t positionOfTheNodeInData, size_t secondDataPoint, vector<vector<uint16_t>> leafReach ) {
	this->typeOfNode = aType;
	this->sequence = sequence;
	this->positionOfDataSource = positionOfTheNodeInData;
	this->secondDataPoint = secondDataPoint;
	this->leafReachabilityForALocus = leafReach;
	this->numberOfMutantLoci = 0;
	this->numberOfPrimitiveLoci = 0;
	this->numberOfNonAncestralLoci = 0;

	for ( size_t i = 0 ; i < sequence.getSizeOfSequence() ; ++i){
		if ( SnipSequence::isPositionMutant(i,sequence)){
			++numberOfMutantLoci;
		}else if (SnipSequence::isPositionAncestral(i,sequence) ){
			++numberOfPrimitiveLoci;
		}else{
			++numberOfNonAncestralLoci;
		}
	}
	assert ((numberOfMutantLoci + numberOfPrimitiveLoci + numberOfNonAncestralLoci) == sequence.getSizeOfSequence());

}

/*********************************************************************************************/
/**                                REDIFINITION OF OUTPUT STREAM                            **/
/*********************************************************************************************/
std::ostream& operator << (std::ostream & out, const ARGnode & aNode){

	if ( aNode.typeOfNode == ARGNodeType::LEAFSAMPLEMODE){
		out << "Leaf node\n";
		out << " - Source index        : " <<  aNode.positionOfDataSource+1 << "\n";
		out << " - Second data point   : NULL \n";
	}else if ( aNode.typeOfNode == ARGNodeType::COALESCENSENODE){
		out << "Coalescense Node\n";
		out << " - Source index        : " << aNode.positionOfDataSource+1 << "\n";
		out << " - Second source       : " << aNode.secondDataPoint+1 << "\n";
	}else if (aNode.typeOfNode == ARGNodeType::RECOMBINATIONODE){
		out << "Recombination node" << "\n";
		out << " - Source index        : "<<  aNode.positionOfDataSource+1 << "\n";
		out << " - Recombination point : " << aNode.secondDataPoint << "\n";
	}else if ( aNode.typeOfNode == ARGNodeType::MUTATIONODE){
		out << "Mutation node\n";
		out << " - Source index        : " <<  aNode.positionOfDataSource+1 << "\n";
		out << " - mutation position   : " << aNode.secondDataPoint+1 << "\n";

	}else{
		throw (string(__FILE__) + ":" + std::to_string(__LINE__)+ ": Unknown node type!");
	}
	out << " - Sequence            : " << aNode.sequence << "\n";

	// Prints the vector of paths ...
	/*
	out << " - Leaf node paths     : ";
	for ( size_t i = 0 ; i < aNode.listOfLeafReachabilityForATIM.size() ;++i){
		out << i+1 << "={";
		for( size_t j = 0 ; j < aNode.listOfLeafReachabilityForATIM.at(i).size() ; ++j){
			out << aNode.listOfLeafReachabilityForATIM.at(i).at(j)+1;
			if (j < aNode.listOfLeafReachabilityForATIM.at(i).size() -1 ){
				out << '-';
			}
		}
		out << "}  ";
	}
	 */
	return out;
}
