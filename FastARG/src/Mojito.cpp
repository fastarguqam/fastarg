/*
 * Mojito.cpp
 *
 *  Created on: 2017-01-31
 *      Author: �ric Marcotte
 */

#include<boost/progress.hpp>

#include "Mojito.h"

using std::cout;
using std::endl;
using std::uniform_int_distribution;
using std::uniform_real_distribution;
using std::invalid_argument;
using std::to_string;

#define PROGRESS_DISPLAY_FREQUENCY 10

void Mojito::run(void) {
	uniform_int_distribution<size_t> randomCoalMutDist(0,1);
	boost::progress_display show_progress( numberOfPositionToBeMuted );

	while (this->positionOfAliveNodes.size() > 1 ){

		show_progress += totalNumberOfMutationEvent - show_progress.count();
		// We need to make all the coalescence an mutation in a random order.
		bool flag = true;
		while (flag){
			try{
				if (randomCoalMutDist(this->eng)){ // 50% true 50% false
					flag = generateARandomCoalescentEvent();
					if (!flag){ // if no coalescence is possible make try to make an mutation
						flag = generateARandomMutationEvent(); // if it the function returns false the we exit the while loop.
					}
				}else{
					flag = generateARandomMutationEvent(); // Same as above but inverted.
					if (!flag){
						flag = generateARandomCoalescentEvent();
					}
				}
			}catch(const std::exception & excetion){
				throw std::runtime_error (string(__FILE__) +':'+ to_string(__LINE__) + ": An runtime exception occurred while generating all possible mutation and coalescent event.\n" + excetion.what());
			}

		} // End while

		// Then we need to generate a "Mojito" recombination.
		if(this->positionOfAliveNodes.size() > 1){

			try{
				// Initialize two variables that will get the right alive nodes.
				size_t firstNodeRecomb= 0, secondNodeRecomb =  0;
				// And then we extract the longest common sequence.
				std::pair<size_t,size_t> alphaBeta = bestAlphaBeta(firstNodeRecomb,secondNodeRecomb);

				// If there is common material we need to check if one break point or two break points are required.
				if (alphaBeta.first != 0 && alphaBeta.second < this->ARGnodes.at(0).sequence.getSizeOfSequence()){
					// We need to make two recombination.
					generateARecombinationEvent(alphaBeta.first,this->positionOfAliveNodes.at(firstNodeRecomb));
					generateARecombinationEvent(alphaBeta.second,this->ARGnodes.size()-2);
					generateACoalescentEvent(this->ARGnodes.size()-1,this->positionOfAliveNodes.at(secondNodeRecomb));

				}else if (alphaBeta.first == 0 && alphaBeta.second < this->ARGnodes.at(0).sequence.getSizeOfSequence()){
					// Then we know that the one before the last node inserted will be a the end of the lives nodes vector and will have a mask for the left side.
					generateARecombinationEvent(alphaBeta.second,this->positionOfAliveNodes.at(firstNodeRecomb));
					generateACoalescentEvent(this->ARGnodes.size()-1,this->positionOfAliveNodes.at(secondNodeRecomb));

				}else if (alphaBeta.first != 0 && alphaBeta.second == this->ARGnodes.at(0).sequence.getSizeOfSequence() ){
					// Then we know that the last live inserted will be a the end of the lives nodes vector and will have a mask for the left side.
					generateARecombinationEvent(alphaBeta.first,this->positionOfAliveNodes.at(firstNodeRecomb));
					generateACoalescentEvent(this->ARGnodes.size()-2,this->positionOfAliveNodes.at(secondNodeRecomb));
				}else{
					throw std::logic_error( string(__FILE__) +':'+ to_string(__LINE__) + ": Bad optimum margarita recombination placement! i.e. it should be a coalescence,");
				}
			}catch(const std::exception & excetion){
				throw std::runtime_error (string(__FILE__) +':'+ to_string(__LINE__) + ": An runtime exception occurred while generation a random Margarita recombination event.\n" +excetion.what());
			}
		}

	} // End while loop


}


//void Mojito::run(void) {
//
//	size_t totalNumberOfMutantLocus = 0;
//	for (size_t i = 0;  i < this->numberOfMutantSNPsByPosition.size() ; ++i ){
//		totalNumberOfMutantLocus += this->numberOfMutantSNPsByPosition.at(i);
//	}
//
//	boost::progress_display show_progress( totalNumberOfMutantLocus );
//
//	size_t tour = 0;
//
//	while (this->positionOfAliveNodes.size() > 1 ){
//		cout << "\n\nTour #" << ++tour << std::endl;
//		cout << "Alive nodes size:" << this->positionOfAliveNodes.size() << endl;
//
//
//		//size_t iterationObserver = 0;
//		//if ( ! (iterationObserver % PROGRESS_DISPLAY_FREQUENCY) ){
//			size_t totalNumberOfMutantLocusTemp = 0;
//			for (size_t i = 0;  i < this->numberOfMutantSNPsByPosition.size() ; ++i ){
//				totalNumberOfMutantLocusTemp += this->numberOfMutantSNPsByPosition.at(i);
//			}
//			//show_progress += totalNumberOfMutantLocus - totalNumberOfMutantLocusTemp - show_progress.count();
//		//}
//			cout << "mutant locus : " << totalNumberOfMutantLocusTemp << endl;
//
//		// First we generate all the mutaion possible.
//		while(generateARandomMutationEvent());
//
//		// Initialize two variables that will get the right alive nodes.
//		size_t firstNodeRecomb= 0, secondNodeRecomb =  0;
//		// And then we extract the longest common sequence.
//		std::pair<size_t,size_t> alphaBeta = bestAlphaBeta(firstNodeRecomb,secondNodeRecomb);
//		// We need to count the number of common mutant position.
//		size_t numberForRecombination = 0;
//		for ( size_t i = alphaBeta.first ; i < alphaBeta.second ; ++i){
//			SnipSequence A = this->ARGnodes.at(this->positionOfAliveNodes.at(firstNodeRecomb)).sequence;
//			SnipSequence B = this->ARGnodes.at(this->positionOfAliveNodes.at(secondNodeRecomb)).sequence;
//			if (SnipSequence::isPositionMutant(i,A) && SnipSequence::isPositionMutant(i,B)){
//				++numberForRecombination;
//			}
//		}
///*
//		cout << "Recomb :\n" << "  first node : " << firstNodeRecomb << " alive : " << this->positionOfAliveNodes.at(firstNodeRecomb) << std::endl;
//		cout << "  second : " << secondNodeRecomb  << " alive : " << this->positionOfAliveNodes.at(secondNodeRecomb) << std::endl;
//		cout << "  alphabeta = " << alphaBeta.first << "," << alphaBeta.second << std::endl;
//		cout << "   number Recomb " << numberForRecombination << std::endl;
//*/
//		// Initialize two variables that will get the right alive nodes.
//		size_t firstNodeCoal= 0, secondNodeCoal =  0;
//		// Then for each possible coalescence for compute the number of common mutant locus.
//		// Will keep the best coalescence node.
//		size_t numberForACoalescence = 0, bestNumberCoal = 0;
//		// We iterate on the coalescence matrix
//		for ( size_t i = 0 ; i < this->coalescencePosibilitiesForLivesNodes.size() ; ++i){
//			for ( size_t j = i+1 ; j < this->coalescencePosibilitiesForLivesNodes.at(i).size() ; ++j){
//				// If we can coalescense theses two.
//				if (this->coalescencePosibilitiesForLivesNodes.at(i).at(j)){
//					// We compute the number for the pair
//					SnipSequence A = this->ARGnodes.at(this->positionOfAliveNodes.at(i)).sequence;
//					SnipSequence B = this->ARGnodes.at(this->positionOfAliveNodes.at(j)).sequence;
//					numberForACoalescence = 0;
//					for ( size_t k = 0 ; k < A.getSizeOfSequence() ; ++k){
//						if (SnipSequence::isPositionMutant(k,A) && SnipSequence::isPositionMutant(k,B)){
//							++numberForACoalescence;
//						}
//					}
//					// And we update the result if needed.
//					if ( bestNumberCoal <= numberForACoalescence ){
//						firstNodeCoal = i;
//						secondNodeCoal = j;
//						bestNumberCoal = numberForACoalescence;
//					}
//				}
//			}
//		}
///*
//		cout << "Coal :\n" << "  first node : " << firstNodeCoal << " alive : " << this->positionOfAliveNodes.at(firstNodeCoal) << std::endl;
//		cout << "  second : " << secondNodeCoal  << " alive : " << this->positionOfAliveNodes.at(secondNodeCoal) << std::endl;
//		cout << "   number coal " << bestNumberCoal << std::endl << std::endl;
//*/
//		// If it is better to generate a coalescence we do it.
//		if (bestNumberCoal >= numberForRecombination || numberForRecombination == 0){
//			cout << "Coal!" << std::endl;
//			generateACoalescentEvent(this->positionOfAliveNodes.at(firstNodeCoal), this->positionOfAliveNodes.at(secondNodeCoal) ) ;
//		}else{
//			cout << "Recomb!" << std::endl;
//			try{
//				// If there is common material we need to check if one break point or two break points are required.
//				if (alphaBeta.first != 0 && alphaBeta.second < this->ARGnodes.at(0).sequence.getSizeOfSequence()){
//					// We need to make two recombination.
//					generateARecombinationEvent(alphaBeta.first,this->positionOfAliveNodes.at(firstNodeRecomb));
//					generateARecombinationEvent(alphaBeta.second,this->ARGnodes.size()-2);
//					generateACoalescentEvent(this->ARGnodes.size()-1,this->positionOfAliveNodes.at(secondNodeRecomb));
//
//				}else if (alphaBeta.first == 0 && alphaBeta.second < this->ARGnodes.at(0).sequence.getSizeOfSequence()){
//					// Then we know that the one before the last node inserted will be a the end of the lives nodes vector and will have a mask for the left side.
//					generateARecombinationEvent(alphaBeta.second,this->positionOfAliveNodes.at(firstNodeRecomb));
//					generateACoalescentEvent(this->ARGnodes.size()-1,this->positionOfAliveNodes.at(secondNodeRecomb));
//
//				}else if (alphaBeta.first != 0 && alphaBeta.second == this->ARGnodes.at(0).sequence.getSizeOfSequence() ){
//					// Then we know that the last live inserted will be a the end of the lives nodes vector and will have a mask for the left side.
//					generateARecombinationEvent(alphaBeta.first,this->positionOfAliveNodes.at(firstNodeRecomb));
//					generateACoalescentEvent(this->ARGnodes.size()-2,this->positionOfAliveNodes.at(secondNodeRecomb));
//
//				}else{
//					throw std::logic_error( string(__FILE__) +':'+ to_string(__LINE__) + ": Bad optimum margarita recombination placement! i.e. it should be a coalescence,");
//				}
//
//			}catch(const std::exception & exception){
//				throw std::logic_error( string(__FILE__) +':'+ to_string(__LINE__) + ": An error has occured while generating an recombination event.\n" + exception.what());
//			}
//		}
//
//
//
//	} // while on live node.
//
//}



Mojito::Mojito( Parameters params, vector<ARGnode> Leaves, const vector<bool> &phenotypes,const  vector<size_t> &distances)
:ARG( params, Leaves,  phenotypes,  distances)
{

}

bool Mojito::generateARandomMojitoRecombinationEvent() {

	// First we choose two random nodes between all on the possibles ones.
	uniform_int_distribution<size_t> intDistribution(0,this->positionOfAliveNodes.size()-1);
	size_t firstNode =  intDistribution(this->eng);
	size_t secondNode =  intDistribution(this->eng);

	// We makes sure they are not the same !
	while (firstNode == secondNode){ secondNode =  intDistribution(this->eng);}

	// Now we get the snipsSequence for those to nodes.
	SnipSequence firstSeq = this->ARGnodes.at(this->positionOfAliveNodes.at(firstNode)).sequence;
	SnipSequence secondSeq = this->ARGnodes.at(this->positionOfAliveNodes.at(secondNode)).sequence;

	// And then we extract the longest common sequence between thoses 2 sequences only.
	std::pair<size_t,size_t> alphaBeta;

	alphaBeta = mojitoLongestCommonTractFinder(firstSeq,secondSeq);

	try{
		// If the is no common material we return false.
		if (alphaBeta.first == 0 && alphaBeta.second == 0){
			return false;
		}else{
			// If there is common material we need to check if one break point or two break points are required.
			if (alphaBeta.first != 0 && alphaBeta.second < this->ARGnodes.at(0).sequence.getSizeOfSequence()){
				//
				generateARecombinationEvent(alphaBeta.first,this->positionOfAliveNodes.at(firstNode));
				generateARecombinationEvent(alphaBeta.second,this->ARGnodes.size()-2);
				generateACoalescentEvent(this->ARGnodes.size()-1,this->positionOfAliveNodes.at(secondNode));

			}else if (alphaBeta.first == 0 && alphaBeta.second < this->ARGnodes.at(0).sequence.getSizeOfSequence()){
				// Then we know that the one before the last node inserted will be a the end of the lives nodes vector and will have a mask for the left side.
				generateARecombinationEvent(alphaBeta.second,this->positionOfAliveNodes.at(firstNode));
				generateACoalescentEvent(this->ARGnodes.size()-1,this->positionOfAliveNodes.at(secondNode));

			}else if (alphaBeta.first != 0 && alphaBeta.second == this->ARGnodes.at(0).sequence.getSizeOfSequence() ){
				// Then we know that the last live inserted will be a the end of the lives nodes vector and will have a mask for the left side.
				generateARecombinationEvent(alphaBeta.first,this->positionOfAliveNodes.at(firstNode));
				generateACoalescentEvent(this->ARGnodes.size()-2,this->positionOfAliveNodes.at(secondNode));
			}else{
				throw std::logic_error( string(__FILE__) +':'+ to_string(__LINE__) + ": Bad margarita recombination placement! i.e. it should be a coalescence,");
			}
		}
	}catch(const std::exception & exception){
		throw std::logic_error( string(__FILE__) +':'+ to_string(__LINE__) + ": An error has occured while generating an recombination event.\n" + exception.what());
	}
	return true;

}

bool Mojito::generateARandomOptimalMojitoRecombinationEvent() {

	// Initialize two variables that will get the right alive nodes.
	size_t firstNode= 0, secondNode =  0;

	// And then we extract the longest common sequence.
	std::pair<size_t,size_t> alphaBeta = bestAlphaBeta(firstNode,secondNode);
	try{
		// If the is no common material we return false.
		if (alphaBeta.first == 0 && alphaBeta.second == 0){
			return false;
		}else{
			// If there is common material we need to check if one break point or two break points are required.
			if (alphaBeta.first != 0 && alphaBeta.second < this->ARGnodes.at(0).sequence.getSizeOfSequence()){
				// We need to make two recombination.
				generateARecombinationEvent(alphaBeta.first,this->positionOfAliveNodes.at(firstNode));
				generateARecombinationEvent(alphaBeta.second,this->ARGnodes.size()-2);
				generateACoalescentEvent(this->ARGnodes.size()-1,this->positionOfAliveNodes.at(secondNode));

			}else if (alphaBeta.first == 0 && alphaBeta.second < this->ARGnodes.at(0).sequence.getSizeOfSequence()){
				// Then we know that the one before the last node inserted will be a the end of the lives nodes vector and will have a mask for the left side.
				generateARecombinationEvent(alphaBeta.second,this->positionOfAliveNodes.at(firstNode));
				generateACoalescentEvent(this->ARGnodes.size()-1,this->positionOfAliveNodes.at(secondNode));

			}else if (alphaBeta.first != 0 && alphaBeta.second == this->ARGnodes.at(0).sequence.getSizeOfSequence() ){
				// Then we know that the last live inserted will be a the end of the lives nodes vector and will have a mask for the left side.
				generateARecombinationEvent(alphaBeta.first,this->positionOfAliveNodes.at(firstNode));
				generateACoalescentEvent(this->ARGnodes.size()-2,this->positionOfAliveNodes.at(secondNode));

			}else{
				throw std::logic_error( string(__FILE__) +':'+ to_string(__LINE__) + ": Bad optimum margarita recombination placement! i.e. it should be a coalescence,");
			}
		}
	}catch(const std::exception & exception){
		throw std::logic_error( string(__FILE__) +':'+ to_string(__LINE__) + ": An error has occured while generating an recombination event.\n" + exception.what());
	}
	return true;

}


std::pair<size_t, size_t> Mojito::bestAlphaBeta(size_t & firstNode,size_t & secondNode) {
	std::pair<size_t,size_t> alphaBeta = std::make_pair(0,0);
	std::pair<size_t,size_t> bestAlphaBeta = std::make_pair(0,0);
	size_t bestCommonMutantLocusCount = 0;

	for (size_t i = 0 ;i < this->positionOfAliveNodes.size();++i){
		for (size_t j = i+1 ; j < this->positionOfAliveNodes.size();++j){
			SnipSequence A = this->ARGnodes.at(this->positionOfAliveNodes.at(i)).sequence;
			SnipSequence B = this->ARGnodes.at(this->positionOfAliveNodes.at(j)).sequence;

			alphaBeta = mojitoLongestCommonTractFinder(A,B);
			size_t currentCommonMutantLocusCount = 0;
			for ( size_t k = alphaBeta.first ; k < alphaBeta.second ; ++k){
				if (SnipSequence::isPositionMutant(k,A) && SnipSequence::isPositionMutant(k,B)){
					++currentCommonMutantLocusCount;
				}
			}
			if( bestCommonMutantLocusCount <= currentCommonMutantLocusCount ){
				bestAlphaBeta = alphaBeta;
				firstNode = i;
				secondNode = j;
				bestCommonMutantLocusCount = currentCommonMutantLocusCount;
			}
		}
	}
	return bestAlphaBeta;

}

std::pair<size_t,size_t> Mojito::mojitoLongestCommonTractFinder(const SnipSequence& firstSequence, const SnipSequence& secondSequence){
	assert (firstSequence.getSizeOfSequence() > 0);
	assert (firstSequence.getSizeOfSequence() ==  secondSequence.getSizeOfSequence());

	std::valarray<DATA_TYPE> M1 = firstSequence.AncestralDataMask;
	std::valarray<DATA_TYPE> D1 = firstSequence.snipsData;
	std::valarray<DATA_TYPE> M2 = secondSequence.AncestralDataMask;
	std::valarray<DATA_TYPE> D2 = secondSequence.snipsData;

	// First we must found the common ancestral material. Mark with a one when true.
	//	A	B	Y
	//	0	0	1
	//	0	1	0
	//	1	0	0
	//	1	1	0
	std::valarray<DATA_TYPE> commonKnownAncestralMaterial = ((~M1)&(~M2));

	// This check for 0-1 or 1-0 incompatibilities.
	// 	    M1  D1 	M2	D2	Result
	// 0    0	0	0	0   1
	// 1    0	0	0	1   0  <- 0 - 1 incompatible
	// 2    0	0	1	0   1
	// 3    0	0	1	1   1
	// 4	0	1	0	0   0  <- 0 - 1 incompatible
	// 5	0	1	0	1   1
	// 6    0	1	1	0   1
	// 7    0	1	1	1   1
	// 8    1	0	0	0   1
	// 9    1	0	0	1   1
	// 10 	1	0	1	0   1
	// 11   1	0	1	1   1
	// 12   1	1	0	0   1
	// 13   1	1	0	1   1
	// 14   1	1	1	0   1
	// 15   1	1	1	1   1
	std::valarray<DATA_TYPE> result = ( M1 | M2 | ((~D1)&(~D2)) | ((D1)&(D2)) );

	//  	A	B	C	D   Result
	// 0	0	0	0	0	0
	// 1	0	0	0	1	0
	// 2	0	0	1	0	0
	// 3	0	0	1	1	0
	// 4	0	1	0	0	0
	// 5	0	1	0	1	1
	// 6	0	1	1	0	0
	// 7	0	1	1	1	0
	// 8	1	0	0	0	0
	// 9	1	0	0	1	0
	// 10	1	0	1	0	0
	// 11	1	0	1	1	0
	// 12	1	1	0	0	0
	// 13	1	1	0	1	0
	// 14	1	1	1	0	0
	// 15	1	1	1	1	0
	std::valarray<DATA_TYPE> commonMutantMaterial = ( (~M1)&(D1)&(~M2)&(D2) );

	std::pair<size_t,size_t> bestPair = std::make_pair(0,0);
	std::pair<size_t,size_t> curentPair = std::make_pair(0,0);
	DATA_TYPE binaryIndicator = BIT_INDICATOR;
	DATA_TYPE resultBlock = 0;
	DATA_TYPE commonMutantMaterialBlock = 0;
	DATA_TYPE commonKnownAncestralMaterialBlock = 0;
	bool flagCounterOn = false;
	bool flagHaveAtLeastOneCommonAncestorPosition = false;
	size_t maxJ, currentNumberOfCommonMutantLocus = 0 ,bestNumberOfCommonMutantLocus = 0;


	for (size_t i = 0 ; i < firstSequence.getnumberOfBlock();++i){
		resultBlock = result[i];
		commonMutantMaterialBlock =  commonMutantMaterial[i];
		commonKnownAncestralMaterialBlock = commonKnownAncestralMaterial[i];
		// Check if we are on the last block.

		// If we are we only go trough until the end of the last locus.
		if ( i == firstSequence.getnumberOfBlock()-1 ){
			maxJ = firstSequence.getSizeOfSequence() % BIT_SIZE_OF_DATA_TYPE;
		}else{ // Else the block is full so we can go trough completely the block.
			maxJ = BIT_SIZE_OF_DATA_TYPE;
		}

		// With the right block and the right limit we can go trough the sequences for a block.
		for (size_t j = 0 ; j < maxJ ; ++j){

			// Then we check if at the current position in the block we can count.
			if (resultBlock & binaryIndicator){
				if (commonKnownAncestralMaterialBlock & binaryIndicator){
					flagHaveAtLeastOneCommonAncestorPosition = true;
				}
				if(flagCounterOn){ // if we started to count that is it ok just to move right indicator
					curentPair.second = i*BIT_SIZE_OF_DATA_TYPE+j+1;
					// And we increment by one if a mutant locus is common.
					// We need to check if this position is both known on both sequences.
				}else{ // In this case we start to count.
					curentPair.first = i*BIT_SIZE_OF_DATA_TYPE+j;
					curentPair.second = i*BIT_SIZE_OF_DATA_TYPE+j+1;
					flagCounterOn = true;
					currentNumberOfCommonMutantLocus = 0;
				}
				if (commonMutantMaterialBlock & binaryIndicator){
					++currentNumberOfCommonMutantLocus;
				}
			}else{ // Other we don't count and put the the counter flag.
				if(flagCounterOn){

					flagCounterOn = false;
					flagHaveAtLeastOneCommonAncestorPosition = false;
				}
			}
			// Each time we update if we have a better pair result.
			if (bestNumberOfCommonMutantLocus <= currentNumberOfCommonMutantLocus &&  flagHaveAtLeastOneCommonAncestorPosition ){
				// if there is the same number of common mutant locus.
				if (bestNumberOfCommonMutantLocus == currentNumberOfCommonMutantLocus){
					// We update only if there the common track is longer
					if ((curentPair.second - curentPair.first) > (bestPair.second - bestPair.first)){
						bestPair.first = curentPair.first;
						bestPair.second = curentPair.second;
					}
				}else{
					bestPair.first = curentPair.first;
					bestPair.second = curentPair.second;
					bestNumberOfCommonMutantLocus = currentNumberOfCommonMutantLocus;
				}
			}

			binaryIndicator = binaryIndicator >> 1;
		}

		binaryIndicator = BIT_INDICATOR;
	}
	return bestPair;
}

void Mojito::printARGSummary() {
	cout << "             ***  Mojito ARG summary *** \n";
	ARG::printARGSummary();
}


