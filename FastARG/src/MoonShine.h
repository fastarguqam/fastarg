/*
 * MoonShine.h
 *
 *  Created on: 2017-03-29
 *      Author: �ric Marcotte
 */

#ifndef MOONSHINE_H_
#define MOONSHINE_H_

#include "ARG.h"
#include "ARGnode.h"
#include "SnipSequence.h"

class MoonShine : public ARG {
public:
	MoonShine( Parameters params, vector<ARGnode> Leaves, const vector<bool> & phenotypes,const  vector<size_t> &distances);

	virtual ~MoonShine();
	virtual void run(void);
};

#endif /* MOONSHINE_H_ */
