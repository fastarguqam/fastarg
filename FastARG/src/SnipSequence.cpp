/*
 * SnipSequence.cpp
 *
 *  Created on: 2016-02-28
 *      Author: �ric Marcotte
 *      TP1 INF7235
 *
 *      http://www.32x8.com/circuits4 to confirm logic operations!*
 *
 */


#include <sstream>
#include <random>
#include <cassert>

#include "SnipSequence.h"

using std::string;
using std::valarray;

// Do not delete, static member needs outside class initialization.
enum coalescenceMode SnipSequence::coalescenceModeFlag = coalescenceMode::UNSPECIFIED; // Default mode!

/*********************************************************************************************/
/**                            Constructors and destructors                                 **/
/*********************************************************************************************/
SnipSequence::SnipSequence() {
	this->numberOfBlock = 0;
	this->sizeOfSequence = 0;
}

// Constructor with just snip data.
SnipSequence::SnipSequence( string aSnipDataSequence ) {
	assert ( aSnipDataSequence.size() > 0 );

	// Initialize the size and the number of block for the sequence.
	this->sizeOfSequence = aSnipDataSequence.size() ;
	this->numberOfBlock = sizeOfSequence / BIT_SIZE_OF_DATA_TYPE;
	if( sizeOfSequence % BIT_SIZE_OF_DATA_TYPE != 0 ){ ++this->numberOfBlock; }

	// Converts the string of 1 and 0 into the binary encoding.
	this->snipsData = SnipSequence::convertStringToFastSequence(aSnipDataSequence);

	// Puts 0 in all the masks locus because it is a leaf in the ARG (no non-ancestral material).
	this->AncestralDataMask = std::valarray<DATA_TYPE>(SnipSequence::getnumberOfBlock());
	for ( size_t i = 0 ; i < SnipSequence::getnumberOfBlock(); ++i ){
		this->AncestralDataMask[i] = 0;
	}
}

// Constructor with some data and non-ancestral material.
SnipSequence::SnipSequence( string aSnipDataSequence, string aAncestralDataMaskSequence) {
	assert ( aSnipDataSequence.size() > 0 );
	assert ( aSnipDataSequence.size() == aAncestralDataMaskSequence.size());

	// Initialize the size and the number of block for the sequence.
	this->sizeOfSequence = aSnipDataSequence.size() ;
	this->numberOfBlock = sizeOfSequence / BIT_SIZE_OF_DATA_TYPE;
	if( this->sizeOfSequence % BIT_SIZE_OF_DATA_TYPE != 0 ){ ++this->numberOfBlock; }

	// Converts the string of 1 and 0 into the binary encoding for the data and the mask.
	this->snipsData = SnipSequence::convertStringToFastSequence(aSnipDataSequence);
	this->AncestralDataMask = SnipSequence::convertStringToFastSequence(aAncestralDataMaskSequence);
}


SnipSequence::SnipSequence( valarray<DATA_TYPE> snipsDataSequence, valarray<DATA_TYPE> ancestralDataMask, size_t sizeOfSequence) {
	assert (snipsDataSequence.size() > 0 );
	assert (snipsDataSequence.size() ==  ancestralDataMask.size());
	assert (sizeOfSequence > 0 );

	this->sizeOfSequence = sizeOfSequence;
	this->numberOfBlock = sizeOfSequence/BIT_SIZE_OF_DATA_TYPE;
	if( sizeOfSequence%BIT_SIZE_OF_DATA_TYPE != 0 ){ ++this->numberOfBlock; }

	assert (this->numberOfBlock == snipsDataSequence.size() );

	this->snipsData= snipsDataSequence;
	this->AncestralDataMask = ancestralDataMask;
}
/*********************************************************************************************/
/**                           Operators overloading: == !=  << +                            **/
/*********************************************************************************************/
bool SnipSequence::operator == (const SnipSequence& otherSnipSequence) const {
	switch (this->coalescenceModeFlag){
	case coalescenceMode::NO_CONSTRAINT:{
		return SnipSequence::noConstraintCoalescence(*this,otherSnipSequence);
		break;
	}case coalescenceMode::HAVE_ANCESTRAL_MATERIAL_IN_COMMON:{
		return SnipSequence::commonMaterialCoalescenceTest(*this,otherSnipSequence);
		break;
	}case (coalescenceMode::UNSPECIFIED) :{
		throw std::invalid_argument ( string(__FILE__) +':'+ std::to_string(__LINE__)+ " Unspecified coalescence test mode!");
	}default :{
		throw std::invalid_argument ( string(__FILE__) +':'+ std::to_string(__LINE__)+ " Unimplemented coalescence test mode!");
	}
	}
	return false;
}
/*
bool SnipSequence::operator !=(const SnipSequence& aSnipSequence) const {
	return !(this->operator ==(aSnipSequence));
}
 */
bool SnipSequence::noConstraintCoalescence( const SnipSequence &firstSequence, const SnipSequence& secondSequence) {
	// TODO Check if alias is better !
	const std::valarray<DATA_TYPE> & A = firstSequence.AncestralDataMask;
	std::valarray<DATA_TYPE> B = firstSequence.snipsData;
	std::valarray<DATA_TYPE> C = secondSequence.AncestralDataMask;
	std::valarray<DATA_TYPE> D = secondSequence.snipsData;

	// TODO Put the truth table for this boolean function.
	size_t i = 0;
	while (i < firstSequence.getnumberOfBlock() ){
		if ( (~C[i])&(~A[i])&(((~D[i])&B[i])|(D[i] &(~B[i])))  ){
			return false;
		}
		++i;
	}
	return true;
}

bool SnipSequence::commonMaterialCoalescenceTest(const SnipSequence& firstSequence, const SnipSequence& secondSequence) {
	std::valarray<DATA_TYPE> A = firstSequence.AncestralDataMask;
	std::valarray<DATA_TYPE> B = firstSequence.snipsData;
	std::valarray<DATA_TYPE> C = secondSequence.AncestralDataMask;
	std::valarray<DATA_TYPE> D = secondSequence.snipsData;
	/*
	// We need to make sure there's at least one common ancestral position to allow an recombination.
	//  	A	B	Y
	// 0	0	0	1
	// 1	0	1	1
	// 2	1	0	1
	// 3	1	1	0
	 */
	size_t i = 0;
	while (i < firstSequence.getnumberOfBlock() ){
		// The last block shouldn't pose problem even if not complete.
		// Since there are all primitive position i.e. 0.
		if ( (~C[i])&(~A[i])&(((~D[i])&B[i])|(D[i] &(~B[i])))  ){
			return false;
		}
		++i;
	}

	// First we iterate trough the vector and check if there is at least one position in common.
	//bool flagHaveAPositionInCommon = false;
	for (size_t i = 0 ; i < firstSequence.getSizeOfSequence();++i){
		if ( isPositionAncestral(i,firstSequence) && isPositionAncestral(i,secondSequence)){
			return true;
		}
	}
	// We return false only if there is none.
	return false;
}

std::ostream& operator << (std::ostream & out, const SnipSequence & aSeq){
	return out << SnipSequence::toString(aSeq);
}


/*
bool SnipSequence::SnipSequence::isPositionMutant(size_t thePosition, const SnipSequence & aSnipSequence){

	if( thePosition >=  aSnipSequence.getSizeOfSequence() ){
		throw std::invalid_argument (  ": The the position : " +  std::to_string(thePosition) +
				" is invalid. It must be between 0 and "+ std::to_string(aSnipSequence.getSizeOfSequence()-1) +".\n");
	}
	size_t blockNumber = thePosition / BIT_SIZE_OF_DATA_TYPE;
	size_t positionInTheBlock = thePosition % BIT_SIZE_OF_DATA_TYPE;
	DATA_TYPE bitIndicator= BIT_INDICATOR >> positionInTheBlock;

	// Check if the material is non-ancestral and return false if it is.
	// If it is ancestral material we return the right result
	return (aSnipSequence.snipsData[(thePosition / BIT_SIZE_OF_DATA_TYPE)] & (BIT_INDICATOR >> (thePosition % BIT_SIZE_OF_DATA_TYPE))) &&
			!(aSnipSequence.AncestralDataMask[(thePosition / BIT_SIZE_OF_DATA_TYPE)] & (BIT_INDICATOR >> (thePosition % BIT_SIZE_OF_DATA_TYPE))) ;
}

*/
SnipSequence SnipSequence::mutateAPosition( const SnipSequence & aSnipSequence, size_t thePosition){
	if( thePosition >=  aSnipSequence.getSizeOfSequence() ){
		throw std::logic_error(string(__FILE__) +':'+ std::to_string(__LINE__)+ ": Invalid position, must be between 0 and size of sequence-1.");
	}
	size_t blockNumber = thePosition / BIT_SIZE_OF_DATA_TYPE;
	size_t positionInTheBlock = thePosition % BIT_SIZE_OF_DATA_TYPE;
	DATA_TYPE bit1OnTheRightPosition= BIT_INDICATOR >> positionInTheBlock;
	if( aSnipSequence.AncestralDataMask[blockNumber] & bit1OnTheRightPosition  ){
		throw std::logic_error(string(__FILE__) +':'+ std::to_string(__LINE__)+ ": Can't mutate a ancestral position!");
	}
	if( aSnipSequence.AncestralDataMask[blockNumber] & bit1OnTheRightPosition  ){
		throw std::logic_error(string(__FILE__) +':'+ std::to_string(__LINE__)+ ": Can't mutate a ancestral position!");
	}
	if ( !(aSnipSequence.snipsData[blockNumber] & bit1OnTheRightPosition) ){
		throw std::logic_error(string(__FILE__) +':'+ std::to_string(__LINE__)+ ": Can't mutate an primitive position!");
	}

	// Creates a nes Snip sequence
	std::valarray<DATA_TYPE> newSnipData = aSnipSequence.snipsData;
	std::valarray<DATA_TYPE> newMaskData = aSnipSequence.AncestralDataMask;
	newSnipData[blockNumber] ^= bit1OnTheRightPosition; // Flips the right bit!
	return SnipSequence( newSnipData , newMaskData, aSnipSequence.sizeOfSequence );
}

std::pair<SnipSequence,SnipSequence> SnipSequence::recombinationOnAposition(const SnipSequence& sourceSequence ,	size_t positionOfTheRecombination ) {

	//std::valarray<SnipSequence> twoResultSequences(2);
	std::pair<SnipSequence,SnipSequence> resultingSeuqences;
	DATA_TYPE bitIndicator = BIT_INDICATOR;
	size_t fullBlock = positionOfTheRecombination / BIT_SIZE_OF_DATA_TYPE;
	size_t splitBlock = positionOfTheRecombination % BIT_SIZE_OF_DATA_TYPE;

	if( positionOfTheRecombination == 0 || positionOfTheRecombination >= sourceSequence.getSizeOfSequence()){
		throw std::logic_error( string(__FILE__) +':'+ std::to_string(__LINE__)+ " : Wrong recombination point : "+std::to_string(positionOfTheRecombination) +", must be between ]0,size[" );
	}
	std::valarray<DATA_TYPE> newDataMaskForLeftSequence = sourceSequence.AncestralDataMask;
	std::valarray<DATA_TYPE> newDataMaskForRightSequence = sourceSequence.AncestralDataMask;
	std::valarray<DATA_TYPE> newDataForBothSequences = sourceSequence.snipsData;

	for( size_t i = 0 ; i < sourceSequence.getnumberOfBlock() ; i++ ){
		if ( i < fullBlock ){
			newDataMaskForLeftSequence[i] |= ALLONES;
		}else if(i > fullBlock){
			newDataMaskForRightSequence[i] |= ALLONES;
		}
	}

	for( size_t i = 0 ; i < BIT_SIZE_OF_DATA_TYPE ; i++ ){
		if (i < splitBlock ){
			newDataMaskForLeftSequence[fullBlock] |= bitIndicator;
		}else{
			newDataMaskForRightSequence[fullBlock] |= bitIndicator;
		}
		bitIndicator = bitIndicator >> 1;
	}
	resultingSeuqences.first = SnipSequence( newDataForBothSequences , newDataMaskForLeftSequence, sourceSequence.sizeOfSequence );
	resultingSeuqences.second = SnipSequence( newDataForBothSequences , newDataMaskForRightSequence, sourceSequence.sizeOfSequence );
	/*
	twoResultSequences[0] =  SnipSequence( newDataForBothSequences , newDataMaskForLeftSequence, sourceSequence.sizeOfSequence );
	twoResultSequences[1] =  SnipSequence( newDataForBothSequences , newDataMaskForRightSequence, sourceSequence.sizeOfSequence );

	return twoResultSequences;
	 */
	return resultingSeuqences;
}

/*********************************************************************************************/
/**          Static method providing various services related to the data structure         **/
/*********************************************************************************************/
std::valarray<DATA_TYPE> SnipSequence::convertStringToFastSequence( string aString ){
	assert (aString.size() > 0);

	// Computes the number of blocks needed.
	size_t numberOfBlock = aString.size() / BIT_SIZE_OF_DATA_TYPE;
	if( aString.size() % BIT_SIZE_OF_DATA_TYPE != 0 ){ ++numberOfBlock; }

	// Initialize the array with the right size.
	std::valarray<DATA_TYPE> resultVector (numberOfBlock);

	// Temporary variable for each block.
	DATA_TYPE number = 0;
	DATA_TYPE bitIndicator = BIT_INDICATOR;
	string tempString = "";

	for ( size_t i = 0 ; i < numberOfBlock ; ++i ){
		// We extract the right segment of the sequence
		tempString = aString.substr( i*BIT_SIZE_OF_DATA_TYPE , BIT_SIZE_OF_DATA_TYPE );
		// If it is the last block we maybe need to fill it with zeroes.
		if ( i ==  numberOfBlock-1 ){
			tempString.append( BIT_SIZE_OF_DATA_TYPE-tempString.size(), '0');
		}
		for ( size_t j = 0 ; j < BIT_SIZE_OF_DATA_TYPE ; ++j ){
			// If we have a 1 we switch the right bit to 1. |= is the bitwise or.
			if( tempString.at( j ) == '1' ){
				number |= bitIndicator ;
			}else if ( tempString.at(j) != '0' ){
				throw std::runtime_error(string(__FILE__) + ":" + std::to_string(__LINE__) + ": "+ "Error invalid character :'" + tempString.at(j) + "' in string!\n" );
			}
			// >> is the bitwise right shift.
			bitIndicator = bitIndicator >> 1;
		}
		resultVector[i] = number;
		bitIndicator = BIT_INDICATOR;
		number = 0;
	}
	return resultVector;
}


string SnipSequence::toString(const SnipSequence & aSeq){
	size_t residualSequenceSize = aSeq.getSizeOfSequence() % BIT_SIZE_OF_DATA_TYPE;
	string result = "";
	DATA_TYPE bitIndicator = BIT_INDICATOR;
	DATA_TYPE aSnipsBlock = 0;
	DATA_TYPE aMaskBlock = 0;

	for ( size_t i = 0 ; i < aSeq.snipsData.size() ; ++i ){
		aSnipsBlock = aSeq.snipsData[i];
		aMaskBlock =  aSeq.AncestralDataMask[i];
		for ( size_t j = 0 ; j < BIT_SIZE_OF_DATA_TYPE ; ++j ){
			if( aMaskBlock & bitIndicator ){                  //  Check if the ancestral mask is set to 1.
				result += '?';                                //  then add 9.                                Mask   0011
			}else if (aSnipsBlock & bitIndicator ){           //  Check the snip state on the data         + Data   0101
				result += '1';                                //  if mutant add 1.                         = Result 0199
			} else {                                          //
				result += '0';                                //  if primitive add 0.
			}                                                 //
			bitIndicator = bitIndicator >> 1;
		}
		bitIndicator = BIT_INDICATOR;
	}
	if (residualSequenceSize == 0 ){
		return result;
	}else{
		return result.substr(0,result.size()-(BIT_SIZE_OF_DATA_TYPE-residualSequenceSize));
	}
}


/*********************************************************************************************/
/**                       Various getters of the data structure                             **/
/*********************************************************************************************/
SnipSequence SnipSequence::generateARandomSnipSequence(SnipSequence aSequence) {

	// Random generator
	static std::random_device rd;
	static std::mt19937 gen(rd());
	static std::uniform_int_distribution<DATA_TYPE> dis(0, std::numeric_limits<DATA_TYPE>::max());

	// Initialization of all the arrays
	std::valarray<DATA_TYPE> randomMasKData(aSequence.getnumberOfBlock());
	std::valarray<DATA_TYPE> randomSnipData(aSequence.getnumberOfBlock());

	// Generation of random sequence for the length of the sequence
	for (size_t i = 0 ; i < aSequence.getnumberOfBlock() ; ++i){
		randomMasKData[i] = dis(gen);
		randomSnipData[i] = dis(gen);

	}
	return SnipSequence(randomSnipData,randomMasKData,aSequence.getSizeOfSequence());
}


