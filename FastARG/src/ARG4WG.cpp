/*
 * ARG4WG.cpp
 *
 *  Created on: 2017-02-21
 *      Author: �ric Marcotte
 */

#include<boost/progress.hpp>
#include "ARG4WG.h"

using std::cout;
using std::uniform_int_distribution;

ARG4WG::ARG4WG( Parameters params, vector<ARGnode> Leaves, const vector<bool>& phenotypes, const vector<size_t>& distances )
// Call to super !!
:ARG( params, Leaves,  phenotypes,  distances){

	this->leftRightSharedIndexMatrixForLiveNodes = vector<vector<std::pair<size_t,size_t>>> (this->positionOfAliveNodes.size(), vector<std::pair<size_t,size_t>>(this->positionOfAliveNodes.size()));
	try {
		for ( size_t i = 0 ; i < this->positionOfAliveNodes.size() ; ++i){
			SnipSequence A = this->ARGnodes.at( this->positionOfAliveNodes.at(i)).sequence;
			this->leftRightSharedIndexMatrixForLiveNodes.at(i).at(i) = std::pair<size_t,size_t> (params.sequencesSize,params.sequencesSize);
			for ( size_t j = i+1 ; j <  this->positionOfAliveNodes.size(); ++j){
				SnipSequence B = this->ARGnodes.at(this->positionOfAliveNodes.at(j)).sequence;
				std::pair<size_t,size_t> aPair = getleftAndRightSharedIndex(A, B);
				this->leftRightSharedIndexMatrixForLiveNodes.at(i).at(j) = aPair;
				this->leftRightSharedIndexMatrixForLiveNodes.at(j).at(i) = aPair;
			}
		}
	}catch (const std::exception & exception){
		throw std::runtime_error( string(__FILE__) +':'+ std::to_string(__LINE__) +  " : Error during ARG4WG contruction :" + exception.what() );
	}
}

void ARG4WG::generateACoalescentEvent(size_t firstSourceNode, size_t secondSourceNode) {
	size_t indexOfFirstSourceInAliveNode = this->positionOfAliveNodes.size(), indexOfSecondSourceInAliveNode = this->positionOfAliveNodes.size();
	for ( size_t i = 0 ; i < this->positionOfAliveNodes.size(); ++i){
		if (this->positionOfAliveNodes.at(i) == firstSourceNode){
			indexOfFirstSourceInAliveNode = i;
		}else if (this->positionOfAliveNodes.at(i) == secondSourceNode ){
			indexOfSecondSourceInAliveNode = i;
		}
		if (indexOfFirstSourceInAliveNode != this->positionOfAliveNodes.size() && indexOfSecondSourceInAliveNode != this->positionOfAliveNodes.size() ){
			break;
		}
	}
	if (indexOfFirstSourceInAliveNode > indexOfSecondSourceInAliveNode ){
		std::swap(indexOfFirstSourceInAliveNode,indexOfSecondSourceInAliveNode);
	}
	ARG::generateACoalescentEvent(firstSourceNode, secondSourceNode);

	// Deletes the line corresponding to the second source node.
	this->leftRightSharedIndexMatrixForLiveNodes.erase(this->leftRightSharedIndexMatrixForLiveNodes.begin() + indexOfSecondSourceInAliveNode);

	// An then modify the left right matrix.
	for ( size_t i = 0 ; i < this->leftRightSharedIndexMatrixForLiveNodes.size() ; ++i){
		// Then we remove the element for each line of the second source.
		this->leftRightSharedIndexMatrixForLiveNodes.at(i).erase(this->leftRightSharedIndexMatrixForLiveNodes.at(i).begin()+indexOfSecondSourceInAliveNode ) ;
		// We don't do the test for the diagonal term.
		if (i != indexOfFirstSourceInAliveNode){
			// Then we refresh with the added node.
			std::pair <size_t,size_t> aPair = getleftAndRightSharedIndex( this->ARGnodes.back().sequence , this->ARGnodes.at(this->positionOfAliveNodes.at(i)).sequence);
			this->leftRightSharedIndexMatrixForLiveNodes.at(i).at(indexOfFirstSourceInAliveNode) = aPair;
			this->leftRightSharedIndexMatrixForLiveNodes.at(indexOfFirstSourceInAliveNode).at(i) = aPair;
		}
	}
}

void ARG4WG::generateARecombinationEvent(size_t indexOfTheRecombination, size_t sourceNode){
	vector<size_t>::iterator it;
	it = find(this->positionOfAliveNodes.begin(), this->positionOfAliveNodes.end(), sourceNode  );
	size_t indexOfFirstSourceInAliveNode = it - this->positionOfAliveNodes.begin();
	ARG::generateARecombinationEvent(indexOfTheRecombination,sourceNode);

	// We find and iterator on the node that will recombine.
	SnipSequence A = this->ARGnodes.at(this->ARGnodes.size()-2).sequence;
	SnipSequence B = this->ARGnodes.back().sequence;

	std::pair <size_t,size_t> aPair;
	vector<std::pair <size_t,size_t>> newLine = {};

	for ( size_t i = 0 ; i < this->leftRightSharedIndexMatrixForLiveNodes.size() ; ++i){
		// Then we refresh with the left node.
		if (i != indexOfFirstSourceInAliveNode){
			aPair = getleftAndRightSharedIndex( A , this->ARGnodes.at(this->positionOfAliveNodes.at(i)).sequence);
			this->leftRightSharedIndexMatrixForLiveNodes.at(i).at(indexOfFirstSourceInAliveNode) = aPair;
			this->leftRightSharedIndexMatrixForLiveNodes.at(indexOfFirstSourceInAliveNode).at(i) = aPair;
		}
		// This is the part for the added node.
		aPair = getleftAndRightSharedIndex( B , this->ARGnodes.at(this->positionOfAliveNodes.at(i)).sequence);
		this->leftRightSharedIndexMatrixForLiveNodes.at(i).push_back(aPair);
		newLine.push_back(aPair);
	}
	newLine.push_back(std::pair <size_t,size_t> (A.getSizeOfSequence(),A.getSizeOfSequence()));
	this->leftRightSharedIndexMatrixForLiveNodes.push_back(newLine);

}

void ARG4WG::generetaAMutationEvent(size_t positionOfTheMutation , size_t sourceNode){
	ARG::generetaAMutationEvent(positionOfTheMutation,sourceNode);
	vector<size_t>::iterator it;
	it = find(this->positionOfAliveNodes.begin(), this->positionOfAliveNodes.end(), this->ARGnodes.size()-1  );
	SnipSequence A = this->ARGnodes.back().sequence;
	size_t index = std::distance(this->positionOfAliveNodes.begin(), it);
	std::pair <size_t,size_t> aPair;

	// Then we update the matrix of possible coalescence.
	for (size_t i = 0 ; i < this->leftRightSharedIndexMatrixForLiveNodes.at(index).size(); ++i){
		if (index != i){ // Diagonal term doesn't need to be computed.
			aPair = getleftAndRightSharedIndex( A , this->ARGnodes.at(this->positionOfAliveNodes.at(i)).sequence);
			this->leftRightSharedIndexMatrixForLiveNodes.at(i).at(index) = aPair;
			this->leftRightSharedIndexMatrixForLiveNodes.at(index).at(i) = aPair;

		}
	}
}


void ARG4WG::run(void){
	// To make the progress bar.
	boost::progress_display show_progress( numberOfPositionToBeMuted );

	while (this->positionOfAliveNodes.size() > 1 ){ // i.e. While not OMRCA
		show_progress += this->totalNumberOfMutationEvent - show_progress.count();
		if (this->numberOfCoalescencePossible != 0){
			generateARandomCoalescentEvent();
		}else if (this->numberOfMutationPossible != 0){
			generateARandomMutationEvent();
		}else{
			try{
				generateAnARG4WGRecombinationEvent();
			}catch (const std::exception & exception) {
				throw std::runtime_error ( string(__FILE__) +':'+ std::to_string(__LINE__) + ": An runtime exception occurred while generating an \"ARGWG\" recombination event.\n" + exception.what() );
			}
		}
	}// End while OMRCA
}// End run.

void ARG4WG::generateAnARG4WGRecombinationEvent(){
	// Creates a vector with all the bests recombination possible.
	std::vector<std::tuple<bool,size_t,size_t,size_t>> longestSharedEnds = findLongestSharedEnds();

	// Gets one of the possible recombination randomly.
	std::uniform_int_distribution<size_t> randomLongestSharedEnd(0,longestSharedEnds.size()-1);
	size_t winningIndex = randomLongestSharedEnd(this->eng);
	std::tuple<bool,size_t,size_t,size_t> winningTuple = longestSharedEnds.at(winningIndex);

	// Gets tuple info and other variables initialization.
	size_t sharedEnd = std::get<1>(winningTuple);
	size_t node1Index =  std::get<2>(winningTuple) ;
	size_t node2Index =  std::get<3>(winningTuple) ;
	assert(sharedEnd < lenghtOfSequences);

	if ( std::get<0>(winningTuple) ){ // lhs
		if (this->ARGnodes.at(node1Index).numberOfNonAncestralLoci > this->ARGnodes.at(node2Index).numberOfNonAncestralLoci ){
			generateARecombinationEvent(sharedEnd+1,node1Index);
			generateACoalescentEvent(this->ARGnodes.size()-1,node2Index);
		}else{
			generateARecombinationEvent(sharedEnd+1,node2Index);
			generateACoalescentEvent(this->ARGnodes.size()-1,node1Index);
		}
	}else { // rhs

		if (this->ARGnodes.at(node1Index).numberOfNonAncestralLoci > this->ARGnodes.at(node2Index).numberOfNonAncestralLoci ){
			generateARecombinationEvent(sharedEnd,node1Index);
			generateACoalescentEvent(this->ARGnodes.size()-2,node2Index);
		}else{
			generateARecombinationEvent(sharedEnd,node2Index);
			generateACoalescentEvent(this->ARGnodes.size()-2,node1Index);
		}
	}
}


std::vector<std::tuple<bool,size_t,size_t,size_t>> ARG4WG::findLongestSharedEnds(void){ // tuple< LEFT RIGHT bool, index of end, source node1, source node 2>

	std::vector<std::tuple<bool,size_t,size_t,size_t>> results;
	std::pair <size_t,size_t> leftRightSharedEnds;

	size_t longestTrackLength = 0;
	size_t leftTrackLength = 0;
	size_t rightTrackLength = 0;

	for ( size_t i = 0 ; i < this->positionOfAliveNodes.size() ; ++i){
		for ( size_t j = i+1 ; j < this->positionOfAliveNodes.size() ; ++j){
			leftRightSharedEnds = this->leftRightSharedIndexMatrixForLiveNodes.at(i).at(j);
			//leftRightSharedEnds = getleftAndRightSharedIndex(this->ARGnodes.at(this->positionOfAliveNodes.at(i)).sequence,this->ARGnodes.at(this->positionOfAliveNodes.at(j)).sequence );

			(std::get<0>(leftRightSharedEnds) != lenghtOfSequences ) ? (leftTrackLength  = std::get<0>(leftRightSharedEnds) )                    : (leftTrackLength  = 0);
			(std::get<1>(leftRightSharedEnds) != lenghtOfSequences ) ? (rightTrackLength = lenghtOfSequences-std::get<1>(leftRightSharedEnds)-1) : (rightTrackLength = 0);

			if (longestTrackLength <= leftTrackLength ){
				if (longestTrackLength < leftTrackLength ){
					results.clear();
					longestTrackLength = leftTrackLength;
				}
				results.push_back( std::make_tuple(true, std::get<0>(leftRightSharedEnds)-1, this->positionOfAliveNodes.at(i), this->positionOfAliveNodes.at(j) ));
			}

			if ( longestTrackLength <= rightTrackLength ){
				if ( longestTrackLength < rightTrackLength ){
					results.clear();
					longestTrackLength = rightTrackLength;
				}
				results.push_back( std::make_tuple(false, std::get<1>(leftRightSharedEnds)+1, this->positionOfAliveNodes.at(i), this->positionOfAliveNodes.at(j) ));
			}
		}
	}

	return results;
}
// 0123456789
// 01??10?110
// 01??11?110 -> returns 5,6
/*
 * If there is no left track it returns the size of the sequences for the first item.
 * Idem for the second item.
 * It will also return size of sequences for both if the can coalesce!
 * If the lhs of the pair is equal to the size of the sequence then there is not a valid shared ends for the left.
 * If the rhs of the pair is equal to zero then there is not a valid shared ends for the left.
 *
 */

int _DEBUG_ARG4WG = 0;
#define DEBUG_ARG4WG( x )  if (_DEBUG_ARG4WG > 0) { x }

std::pair<size_t,size_t> ARG4WG::getleftAndRightSharedIndex (const SnipSequence & firstSequence, const SnipSequence & secondSequence){
	size_t leftSideLimit = firstSequence.getSizeOfSequence();
	size_t rightSideLimit = firstSequence.getSizeOfSequence();
	std::valarray<DATA_TYPE> A = firstSequence.AncestralDataMask;
	std::valarray<DATA_TYPE> B = firstSequence.snipsData;
	std::valarray<DATA_TYPE> C = secondSequence.AncestralDataMask;
	std::valarray<DATA_TYPE> D = secondSequence.snipsData;

	// First we must found the common ancestral material. Mark with a one when true.
	//	A	B	Y
	//	0	0	1
	//	0	1	0
	//	1	0	0
	//	1	1	0
	std::valarray<DATA_TYPE> knownAncestralMaterial = ((~A)&(~C));

	// 	        M1  D1 	M2	D2	Result
	// 0	    0	0	0	0	  1
	// 1	    0	0	0	1	  0  <- 0 - 1 incompatible
	// 2	    0	0	1	0	  1
	// 3	    0	0	1	1	  1
	// 4	    0	1	0	0	  0  <- 0 - 1 incompatible
	// 5	    0	1	0	1	  1
	// 6	    0	1	1	0	  1
	// 7	    0	1	1	1	  1
	// 8	    1	0	0	0	  1
	// 9	    1	0	0	1	  1
	// 10   	1	0	1	0	  1
	// 11	    1	0	1	1	  1
	// 12	    1	1	0	0	  1
	// 13	    1	1	0	1	  1
	// 14	    1	1	1	0	  1
	// 15	    1	1	1	1	  1

	// This check for 0-1 or 1-0 incompatibilities.
	std::valarray<DATA_TYPE> result = ( C | A | ((~B)&(~D)) | ((B)&(D)) );

	DATA_TYPE binaryIndicator = BIT_INDICATOR;
	DATA_TYPE resultBlock = 0;
	bool leftSideLimitFound = false;
	size_t maxJ;

	// First we find the first and last index of the zeroes !
	for (size_t i = 0 ; i < firstSequence.getnumberOfBlock();++i){
		resultBlock = result[i];
		// Check if we are on the last block.

		// If we are we only go trough until the end of the last locus.
		if ( i == firstSequence.getnumberOfBlock()-1 ){
			maxJ = firstSequence.getSizeOfSequence() % BIT_SIZE_OF_DATA_TYPE;
		}else{ // Else the block is full so we can go trough completely the block.
			maxJ = BIT_SIZE_OF_DATA_TYPE;
		}

		// With the right block and the right limit we can go trough the sequences for a block.
		for (size_t j = 0 ; j < maxJ ; ++j){
			DEBUG_ARG4WG( std::cerr << " j = " << j << std::endl;);

			if ( !(resultBlock & binaryIndicator) ){ // True when bit is 0
				DEBUG_ARG4WG( std::cerr << " Hey3 !" << std::endl; );
				if (!leftSideLimitFound){
					DEBUG_ARG4WG( std::cerr << " Hey 3.5 " << std::endl;);
					leftSideLimit =  i*BIT_SIZE_OF_DATA_TYPE+j;
					leftSideLimitFound = true;
				}
				DEBUG_ARG4WG( std::cerr << "Hey4 !" << std::endl;);
				rightSideLimit =  i*BIT_SIZE_OF_DATA_TYPE+j;
			}
			binaryIndicator = binaryIndicator >> 1;
		}
		binaryIndicator = BIT_INDICATOR;
	}
	bool foundOne = false;
	// We need to make there is a common ancestral position before the first zero!
	for ( size_t i = 0 ; i < leftSideLimit ; ++i){
		if ( SnipSequence::isPositionAncestral(i,firstSequence) && SnipSequence::isPositionAncestral(i,secondSequence) ){
			foundOne = true;
			i = leftSideLimit; // to break out !
		}
	}
	if (!foundOne){	leftSideLimit = firstSequence.getSizeOfSequence(); }

	foundOne = false;
	// We need to make there is a common ancestral position after the last zero!
	for ( size_t i = rightSideLimit+1 ; i < firstSequence.getSizeOfSequence() ; ++i){
		if ( SnipSequence::isPositionAncestral(i,firstSequence) && SnipSequence::isPositionAncestral(i,secondSequence) ){
			foundOne = true;
			i = firstSequence.getSizeOfSequence(); // to break out !

		}
	}
	if (!foundOne){	rightSideLimit = firstSequence.getSizeOfSequence();	}

	return std::make_pair(leftSideLimit, rightSideLimit);
}

void ARG4WG::printARGSummary() {
	cout << "             ***  ARG4GW ARG summary *** \n";
	ARG::printARGSummary();
}

