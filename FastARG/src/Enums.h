/*
 * Enums.h
 *
 *  Created on: 2016-10-17
 *      Author: �ric Marcotte
 */

#ifndef ENUMS_H_
#define ENUMS_H_

// This flag is used in the main to call the right algorithm.
enum class algorithms         : int8_t {UNSPECIFIED = 0, MARGARITA = 1, MOJITO = 2, FEARNHEAD = 3, ARG4WG = 4, MANHATTAN = 5, MOONSHINE = 6};

// This flag resides in the SnipSequence data structure since it changes low level functions.
enum class coalescenceMode    : int8_t {UNSPECIFIED = 0, NO_CONSTRAINT = 1, HAVE_ANCESTRAL_MATERIAL_IN_COMMON = 2};

// This flag resides in the ARG data structure since it changes high level algorithm.
enum class recombinationMode  : int8_t {UNSPECIFIED = 0, NO_CONSTRAINT = 1, HAVE_ANCESTRAL_MATERIAL_ONLY = 2};


#endif /* ENUMS_H_ */
