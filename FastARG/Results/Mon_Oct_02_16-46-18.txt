FastARG parameters are : 
 -The data file is                     : 8_5.dat
 -The specified number of sequences is : 8
 -The specified size of sequences is   : 5
 -The algorithm is                     : Margarita
 -The coalescence mode is              : HAVE_ANCESTRAL_MATERIAL_IN_COMMON
 -The recombination mode is            : HAVE_ANCESTRAL_MATERIAL_ONLY
 -The random seed flag is              : true
 -The seed is                          : 1
 -The Rplot settings                   : false
Margarita specialized parameters are : 
 - Recombination specific proportion : 0.9


             ***  Margarita ARG summary *** 
Basic attributes.
 - Length of sequences   : 5
 - Number of sequences   : 8
 - Total number of nodes : 32
 - Total of lives nodes  : 1
 - Sum of mutant loci    : 0
 - Maximum generation    : 8

Quantity of each type of event in the current state of the ARG.
 - Mutation event      : 5
 - Coalescence event   : 11
 - Recombination event : 4

Quantity of possible events for each type.
 - Possible mutation event        : 0
 - Possible coalescence event     : 0
 - Number of position to be muted : 0

Margarita specific attributes :
 - The seed is                                 : 1506977178372868500
 - The optimal recombination  rate is          : 0.9
 - The number of random recombination event is : 0
Constructed in 0 seconds.
