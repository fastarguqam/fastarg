FastARG parameters are : 
 -The data file is                     : 8_5.dat
 -The specified number of sequences is : 8
 -The specified size of sequences is   : 5
 -The algorithm is                     : ARG4WG
 -The coalescence mode is              : HAVE_ANCESTRAL_MATERIAL_IN_COMMON
 -The recombination mode is            : HAVE_ANCESTRAL_MATERIAL_ONLY
 -The random seed flag is              : false
 -The seed is                          : 1
 -The Rplot settings                   : false


             ***  ARG4GW ARG summary *** 
Basic attributes.
 - Length of sequences   : 5
 - Number of sequences   : 8
 - Total number of nodes : 26
 - Total of lives nodes  : 1
 - Sum of mutant loci    : 0
 - Maximum generation    : 8

Quantity of each type of event in the current state of the ARG.
 - Mutation event      : 5
 - Coalescence event   : 9
 - Recombination event : 2

Quantity of possible events for each type.
 - Possible mutation event        : 0
 - Possible coalescence event     : 0
 - Number of position to be muted : 0

Constructed in 0 seconds.
